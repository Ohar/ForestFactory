import EMPTY_TILE_INDEX from '@/constants/empty_tile_index'

function clearTile (tile, tileMap, tileMapLayer) {
	tileMap.replace(tile.index, EMPTY_TILE_INDEX, tile.x, tile.y, 1, 1, tileMapLayer)
}

export default clearTile
