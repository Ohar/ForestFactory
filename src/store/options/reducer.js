import * as actionTypes from '@/store/options/action-types'
import DEFAULT_STATE from '@/store/options/default_state'

export default function optionsReducer (state = DEFAULT_STATE, action) {
  switch (action.type) {
    case actionTypes.LOCALE_SET: {
      console.info('optionsReducer LOCALE_SET', action.locale) // eslint-disable-line no-console
      return {
        ...state,
        l10n: action.l10n,
        locale: action.locale,
      }
    }

    default:
      return state
  }
}
