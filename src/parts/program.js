import compileProgramToIR from '@/compiler/compileProgramToIR'
import compiler from '@/compiler/compiler'
import { EDITOR_HIGHLIGHT_COMMAND, PROGRAM_ADD } from '@/store/editor/action-types'
import store from '@/store'
import getRobotById from '@/utils/getRobotById'
import uid from 'uid'

export default class Program {
	constructor (
		{
      _stage = null,
      commands = [],
      functions = {},
      id = uid(),
      inProgress = false,
			js = '',
      labels = {},
      listingIR = '',
      name = 'Default name',
      originId = null,
      showAtList = true,
      returnToLabelsStack = [],
      robotId = null,
			xml = '',
		},
		addToStore = true
	) {
    this._js                 = js
    this._stage              = _stage
    this.commands            = commands
    this.functions           = functions
    this.id                  = id
    this.inProgress          = inProgress
    this.labels              = labels
    this.listingIR           = listingIR
    this.name                = name
    this.originId            = originId
    this.showAtList          = showAtList
    this.returnToLabelsStack = returnToLabelsStack
    this.robotId             = robotId
    this.xml                 = xml

		if (addToStore) {
      store.dispatch({
        type   : PROGRAM_ADD,
        program: this,
      })
		}
	}

	get stage () {
		return this._stage
	}

	set stage (val) {
		if (val < this.commands.length) {
			this._stage = val
		} else {
			this.inProgress = false
			this._stage     = null
		}
	}

	get js () {
		return this._js
	}

	set js (js) {
		this._js = js
    this.listingIR = compileProgramToIR(this)
		this.compile()
	}

	addLabel (name, val) {
		this.labels[name] = val
	}

	compile () {
    this.commands = compiler(this)
	}

	run () {
		if (this.inProgress) {
			if (this.stage === null) {
				this.stage = 0
			}

			let finished = this.commands
				&& this.commands[this.stage]
				&& this.commands[this.stage]()

			if (finished) {
				this.stage++
			}
		}
	}

	start () {
		this.inProgress = true
	}

	stop () {
		const robot = getRobotById(this.robotId)

		this.inProgress = false
		this._stage     = null

		if (robot) {
      if (robot.modules.chassis) {
        robot.modules.chassis.stop()
      }
      if (robot.modules.memory) {
        robot.modules.memory.clear()
      }
		}

		store.dispatch({
			type   : EDITOR_HIGHLIGHT_COMMAND,
			blockId: null,
		})
	}

	pause () {
		this.inProgress = false

    const robot = getRobotById(this.robotId)

    if (robot && robot.modules.chassis) {
      robot.modules.chassis.stop()
    }
	}

	init (robot) {
		this.robotId = robot.ffId
		this.compile()
	}

	createCopy (name) {
		return new Program({
			name: name || this.name,
			js  : this.js,
			xml : this.xml,
		})
	}

	createChild () {
		return new Program({
      name      : this.name,
      js        : this.js,
      xml       : this.xml,
      originId  : this.id,
      showAtList: false,
    })
	}

	toJson () {
    return {
	    ...this,
	    commands: [],
	    robot: null,
    }
	}
}
