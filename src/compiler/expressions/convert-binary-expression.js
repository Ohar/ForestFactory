import IRNode from '@/classes/ir-node'
import convertAstNodeToIR from '@/compiler/converters/convert-ast-node-to-ir'
import uid from 'uid'

export default function convertBinaryExpression (node, program, loopLabels) {
	const left  = convertAstNodeToIR(node.left, program, loopLabels),
		  right = convertAstNodeToIR(node.right, program, loopLabels),
		  id    = uid(20)

	return new IRNode({
		id,
		code: `
${left.code}
${right.code}
memory.save ${id} ${left.id} ${node.operator} ${right.id}
`,
	})
}
