import Phaser from 'phaser'

function inCircle (center, radius, point) {
	const circle = new Phaser.Circle(center.x, center.y, radius * 2)
	return circle.contains(point.x, point.y)
}

export default inCircle
