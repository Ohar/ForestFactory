import { LOAD_MODULES } from '@/store/editor/action-types'
import store from '@/store'
import moduleClassByType from '@/constants/moduleClassByType'
import moduleTypes from '@/constants/moduleTypes'

export default function loadModules (levelModules) {
  const moduleList = levelModules
    .filter(
      ({moduleType}) => moduleTypes.includes(moduleType)
    )
    .map(
      e => new (moduleClassByType[e.moduleType])(e)
    )

  return new Promise(
    resolve => {
      setTimeout(
        () => {
          store.dispatch({
            type: LOAD_MODULES,
            moduleList,
          })

          resolve()
        },
        200,
      )
    },
  )
}
