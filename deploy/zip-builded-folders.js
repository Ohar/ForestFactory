const zipFolder                                   = require('zip-folder')
const foreachTimeout                              = require('foreach-timeout')
const {name, version}                             = require('./../package.json')
const {arch, platform: platformList, buildFolder} = require('./../config.build.json')

console.log('Zip builded folders')
console.time('Zip')

foreachTimeout(
  platformList,
  platform => new Promise(
    resolve => {
      zipFolder(
        `./${buildFolder}/${name}-${platform}-${arch}`,
        `./${buildFolder}/${name}-${version}-${platform}-${arch}.zip`,
        err => {
          if (err) {
            console.error('Zip failed', err)
          } else {
            console.log(`Zipped: ${platform}`)
          }
          resolve()
        },
      )
    },
  ),
  0,
)
  .then(() => {
    console.timeEnd('Zip')
    console.log('All platforms are zipped.')
  })
