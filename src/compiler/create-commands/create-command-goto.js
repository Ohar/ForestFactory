import getLabelName from '@/utils/get-label-name'

export default function createCommandGoto (program, params) {
	const labelName = getLabelName(params[0])

	return () => {
		const nextStage = program.labels[labelName]

		program.stage = nextStage

		return program.run()
	}
}
