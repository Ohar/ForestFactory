const {name, description, version} = require('./../package.json')
const {platform: platformList}     = require('./../config.build.json')
const {releasePageFileName}        = require('./../config.deploy.json')
const generateFileName             = require('./generate-file-name')
const platformNames                = require('./platformNames')
const ejs                          = require('ejs')
const fs                           = require('fs')

function generateReleasePage () {
  return ejs.renderFile(
    'release_page_template.ejs',
    {
      description,
      title    : name,
      version,
      buildList: platformList.map(platform => ({
        platform: platformNames[platform],
        fileName: generateFileName(platform),
      })),
    },
    {},
    (err, html) => {
      if (err) {
        console.error('HTML page generation failed', err)
        throw err
      } else {
        fs.writeFile(
          releasePageFileName,
          html,
          err => {
            if (err) {
              console.error(`Saving ${releasePageFileName} failed`, err)
              throw err
            } else {
              console.log('HTML page is generated')
            }
          }
        );
      }
    },
  )
}

module.exports = generateReleasePage
