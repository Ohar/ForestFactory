import convertIRToCommands from '@/compiler/converters/convert-ir-to-commands'
import store from '@/store'
import { SET_EDITOR_LISTING_IR } from '@/store/editor/action-types'
import { COMPILER_ERROR_PARSING } from '@/l10n/keys'

export default function compiler (program) {
  try {
    const commands = convertIRToCommands(program)

    store.dispatch({
      type: SET_EDITOR_LISTING_IR,
      ir  : program.listingIR,
    })

    return commands
  } catch (err) {
    const {optionsState: {l10n}} = store.getState()

    console.error('err', err)
    console.error(l10n[COMPILER_ERROR_PARSING], program && program.listingIR)
    return []
  }
}
