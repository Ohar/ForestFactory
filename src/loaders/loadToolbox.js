import { SET_TOOLBOX } from '@/store/editor/action-types'
import store from '@/store'

export default function loadToolbox (toolbox) {
  return new Promise(
    resolve => {
      store.dispatch({
        type: SET_TOOLBOX,
        toolbox,
      })

      resolve()
    },
  )
}
