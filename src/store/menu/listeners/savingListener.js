import store from '@/store/index'
import generateLevelFromCurrentTilemap from '@/utils/generateLevelFromCurrentTilemap'
import saveLevel from '@/utils/saveLevel'

export default function savingListener (levelMap, {isGameSaving}) {
  return () => {
    // TODO: getwhy “let” is here
    let nextIsGameSaving = store.getState().menuState.isGameSaving

    if (isGameSaving !== nextIsGameSaving) {
      // TODO: Get why is there such string
      isGameSaving = nextIsGameSaving

      if (isGameSaving) {
        const level = generateLevelFromCurrentTilemap(levelMap)

        saveLevel(level)
      }
    }
  }
}
