import Phaser from 'phaser'

function isTile (subject) {
	return subject instanceof Phaser.Tile
}

export default isTile
