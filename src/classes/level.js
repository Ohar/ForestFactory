import store from '@/store'
import { LEVEL_NAME_DEFAULT } from '@/l10n/keys'
import uid from 'uid'

export default class Level {
  constructor (
    {
      id = uid(20),
      levelMap = null,
      modules = [],
      name,
      options = {},
      plot = [],
      programs = [],
      toolbox = '',
      date = Date.now(),
      nextLvlId = null,
    },
  ) {
    const {optionsState: {l10n}} = store.getState()

    this.id        = id
    this.levelMap  = levelMap
    this.date      = date
    this.modules   = modules
    this.name      = name || l10n[LEVEL_NAME_DEFAULT]
    this.nextLvlId = nextLvlId
    this.options   = options
    this.plot      = plot
    this.programs  = programs
    this.toolbox   = toolbox
  }
}
