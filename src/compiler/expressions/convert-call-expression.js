import IRNode from '@/classes/ir-node'
import convertAstNodeToIR from '@/compiler/converters/convert-ast-node-to-ir'
import uid from 'uid'
import map from 'lodash/map'

export default function convertCallExpression (node, program, loopLabels) {
	const id     = uid(20),
		  callee = convertAstNodeToIR(node.callee, program, loopLabels).code,
		  args   = node.arguments
			.map(arg => convertAstNodeToIR(arg, program, loopLabels))

	const argsCode = map(args, 'code').join('\n'),
		  argsIds  = map(args, 'id').join(' ')

	return new IRNode({
		id,
		code: `${argsCode}
${callee} ${id} ${argsIds}`,
	})
}
