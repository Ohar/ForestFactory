import Blockly from '@/blockly/blockly'
import { CHASSIS_MODULE_TYPE } from '@/constants/moduleTypes'
import store from '@/store'
import { BLOCKLY_TO_XY_LABEL_X, BLOCKLY_TO_XY_LABEL_Y, BLOCKLY_TO_XY_TEXT } from '@/l10n/keys'

const BJS = Blockly.JavaScript

const BlockMoveToXY = {
	name      : 'FF_MoveToXY',
	generator : block => {
		const X = BJS.valueToCode(block, 'X', BJS.ORDER_ATOMIC),
		      Y = BJS.valueToCode(block, 'Y', BJS.ORDER_ATOMIC)

		return `${CHASSIS_MODULE_TYPE}.moveToXY(${X}, ${Y})\n`
	},
	definition: {
		init () {
      const {optionsState: {l10n}} = store.getState()

      this.appendDummyInput()
				.appendField(l10n[BLOCKLY_TO_XY_TEXT]);
			this.appendValueInput('X')
				.setCheck('Number')
				.appendField(l10n[BLOCKLY_TO_XY_LABEL_X]);
			this.appendValueInput('Y')
				.setCheck('Number')
				.appendField(l10n[BLOCKLY_TO_XY_LABEL_Y]);
			this.setInputsInline(true);
			this.setPreviousStatement(true, null);
			this.setNextStatement(true, null);
			this.setColour(0);
		}
	}
}

export default BlockMoveToXY
