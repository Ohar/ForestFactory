import groupGidDataList from '@/utils/groupGidDataList'

export default function getObjGid (obj) {
  let gid = null

  groupGidDataList.forEach(
    ({constructor, data}) => {
      if (obj instanceof constructor) {
        const type = Object
          .keys(data)
          .find(key => data[key].frame === obj._frame.index)

        if (type) {
          gid = data[type].gid
        }
      }
    },
  )

  return gid
}
