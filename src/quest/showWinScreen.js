import actionGameplayUIHide from '@/store/gui/actions/gameplay-ui-hide'
import actionWinScreenShow from '@/store/gui/actions/winscreen-show'
import store from '@/store'

export default function showWinScreen(game) {
  game.paused = true

  store.dispatch(actionGameplayUIHide())
  store.dispatch(actionWinScreenShow())
}
