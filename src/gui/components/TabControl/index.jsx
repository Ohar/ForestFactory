import signalSetActiveTabId from '@/store/tab/actions/set-active-tab-id'
import signalTabListHide from '@/store/tab/actions/tab-list-hide'
import signalTabListShow from '@/store/tab/actions/tab-list-show'
import classNames from 'classnames'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import './styles.less'

class TabControl extends Component {
  constructor (...args) {
    super(...args)

    this.activate = this.activate.bind(this)
  }

  activate () {
    const {isTabListVisible, setActiveTabId, showTabList, tabId} = this.props

    if (!isTabListVisible) {
      showTabList()
    }

    setActiveTabId(tabId)
  }

  render () {
    const {activeTabId, children, tabId} = this.props

    const isActive = tabId === activeTabId

    return (
      <button
        className={classNames('TabControl', {
          'TabControl-active': isActive,
        })}
        onClick={this.activate}
      >
        {children}
      </button>
    )
  }
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators(
    {
      setActiveTabId: signalSetActiveTabId,
      hideTabList   : signalTabListHide,
      showTabList   : signalTabListShow,
    },
    dispatch,
  )
}

function mapStateToProps (state, ownProps) {
  return {
    ...ownProps,
    activeTabId     : state.tabState.activeTabId,
    isTabListVisible: state.tabState.tabListVisibility,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TabControl)
