import { SET_QUEST_BY_INDEX } from '@/store/plot/action-types'

export default function signalSetQuestByIndex (questIndex) {
  return dispatch => {
    dispatch(
      {
        type: SET_QUEST_BY_INDEX,
        questIndex,
      },
    )
  }
}
