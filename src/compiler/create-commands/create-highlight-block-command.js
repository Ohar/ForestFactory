import store from '@/store'
import {EDITOR_HIGHLIGHT_COMMAND} from '@/store/editor/action-types'
import getRobotById from '@/utils/getRobotById'

function createHighlightBlockCommand (program, [resultName, blockIdMemoryName]) {
	return () => {
    const robot = getRobotById(program.robotId)
		const blockId = robot.modules.memory.load(blockIdMemoryName)

		store.dispatch({
			type   : EDITOR_HIGHLIGHT_COMMAND,
			blockId,
		})

		program.stage++
		return program.run()
	}
}

export default createHighlightBlockCommand
