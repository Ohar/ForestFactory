import Blockly from 'node-blockly/browser'
import USER_LANGUAGE from '@/constants/user_language'
import loadLocalFile from '@/utils/load-local-file'

Blockly.JavaScript.STATEMENT_PREFIX = 'highlightBlock(%1)\n';
Blockly.JavaScript.addReservedWords('highlightBlock');

//TODO: Придумать способ определять язык на лету
loadLocalFile(`./l10n/blockly/${USER_LANGUAGE}.json`)
	.then(
		data => {
			Blockly.Msg = {
				...Blockly.Msg,
				...data,
			}
		}
	)

export default Blockly
