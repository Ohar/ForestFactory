import { HIDE_START_LEVEL_SCREEN } from '@/store/gui/action-types'

export default function actionStartLevelScreenHide () {
  return dispatch => {
    dispatch({
      type: HIDE_START_LEVEL_SCREEN,
    })
  }
}
