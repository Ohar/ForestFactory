import Gui from '@/gui'
import store from '@/store'
import React from 'react'
import ReactDOM from 'react-dom'
import Provider from 'react-redux/es/components/Provider'
import { ruOnDev } from 'root/config.l10n'
import './main.less'

ReactDOM.render(
  <Provider store={store}>
    <Gui/>
  </Provider>,
  document.getElementById('gui'),
)
