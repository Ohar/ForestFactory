import convertAstNodeToIR from '@/compiler/converters/convert-ast-node-to-ir'
import getFunctionName from '@/utils/get-function-name'
import uid from 'uid'
import IRNode from '@/classes/ir-node'

export default function convertFunctionDeclaration (node, program, loopLabels) {
	const labelStart  = uid(20),
		  labelFinish = uid(20),
		  body        = convertAstNodeToIR(node.body, program, loopLabels).code,
		  name        = getFunctionName(node.id.name)

	// TODO: Сайд-эффект, надо бы убрать. Но как?
	program.functions[name] = labelStart

	return new IRNode({
		id  : labelStart,
		code: `
goto ${labelFinish}
label ${labelStart}
${body}
functionEnd
label ${labelFinish}
`,
	})
}
