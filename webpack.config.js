const path               = require('path')
const webpack            = require('webpack')
const BrowserSyncPlugin  = require('browser-sync-webpack-plugin')
const ExtractTextPlugin  = require('extract-text-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const copyWebpackPlugin  = require('copy-webpack-plugin')
const electronPackager   = require('webpack-electron-packager')
const GenerateJsonPlugin = require('generate-json-webpack-plugin')

const {author: appCopyright}        = require('./package.json')
const {arch, platform, buildFolder} = require('./config.build.json')
const levelList                     = require('./src/levels')

const __DEV__     = Boolean(JSON.parse(process.env.DEV || 'true'))
const __WEB__     = Boolean(JSON.parse(process.env.WEB || 'true'))
const __PACKAGE__ = Boolean(JSON.parse(process.env.PACKAGE || 'true'))

const plugins = {
  common: [
    new webpack.DefinePlugin({__DEV__, __WEB__, __PACKAGE__}),
    new ExtractTextPlugin('[name].css'),
    new copyWebpackPlugin([{
      from: path.join(__dirname, '/node_modules/blockly/msg/json/'),
      to  : path.join(__dirname, '/l10n/blockly/'),
    }]),
    ...levelList.map(
      level => new GenerateJsonPlugin(
        `./levels/${level.id}.json`,
        level
      )
    ),
  ],

  dev: [],

  web: [
    new BrowserSyncPlugin(
      {
        host  : process.env.IP || 'localhost',
        port  : process.env.PORT || 3000,
        open  : false,
        server: {
          baseDir: ['./', `./${buildFolder}`],
        },
      },
    ),
  ],

  prod: [
    new CleanWebpackPlugin(['dist', buildFolder]),
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
    new webpack.optimize.UglifyJsPlugin(
      {
        drop_console: true,
        minimize    : true,
        output      : {
          comments: false,
        },
        compress    : {
          warnings: false,
        },
      },
    ),
    new webpack.optimize.OccurrenceOrderPlugin(),
  ],

  electron: [
    new electronPackager({
      appCopyright,
      arch,
      platform,
      dir           : '.',
      out           : `./${buildFolder}`,
      packageManager: 'yarn',
      icon          : './favicon.ico',
      ignore        : [
        '.idea',
        `${buildFolder}`,
        'node_modules',
        'deploy',
        'src',
        'test',
        '.babelrc',
        '.gitignore',
        'npm-debug.log',
        'postcss.config.js',
        'webpack.config.js',
        'config.build.json',
        'config.deploy.json',
        'README.md',
        'yarn.lock',
      ],
    }),
  ],
}

module.exports = {
  entry: {
    app: [
      'babel-polyfill',
      path.resolve(__dirname, 'src/main.js'),
    ],
  },

  output: {
    pathinfo  : true,
    path      : path.resolve(__dirname, 'dist'),
    publicPath: './dist/',
    filename  : 'bundle.js',
  },

  watch: __DEV__,

  plugins: plugins.common.concat(
    __DEV__
    ? plugins.dev
    : plugins.prod,
    __WEB__
    ? plugins.web
    : plugins.electron,
  ),

  module: {
    rules: [
      {
        test  : /\.json$/,
        loader: 'json-loader',
      },
      {
        test   : /\.jsx?$/,
        loader : 'babel-loader',
        include: path.join(__dirname, 'src'),
      },
      {
        test  : /pixi\.js/,
        loader: 'expose-loader?PIXI',
      },
      {
        test  : /phaser-split\.js$/,
        loader: 'expose-loader?Phaser',
      },
      {
        test  : /p2\.js/,
        loader: 'expose-loader?p2',
      },
      {
        test: /\.(less|css)$/,
        use : ExtractTextPlugin.extract({
          use: [
            'css-loader',
            'postcss-loader',
            {
              loader : 'less-loader',
              options: {
                compress: !__DEV__,
              },
            },
          ],
        }),
      },
      {
        test  : /\.xml$/,
        loader: 'raw-loader',
      },
    ],
  },

  node: {
    fs: 'empty',
  },

  resolve: {
    extensions: ['.js', '.jsx', '.json'],
    alias     : {
      phaser: path.join(__dirname, '/node_modules/phaser/build/custom/phaser-split.js'),
      pixi  : path.join(__dirname, '/node_modules/phaser/build/custom/pixi.js'),
      p2    : path.join(__dirname, '/node_modules/phaser/build/custom/p2.js'),
      '@'   : path.join(__dirname, '/src'),
      root  : path.join(__dirname, '/'),
    },
  },

  target: __WEB__
          ? 'web'
          : 'electron-renderer',
}
