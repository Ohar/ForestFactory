import {
  BATTERY_MODULE_TYPE,
  CHAINSAW_MODULE_TYPE,
  CHASSIS_MODULE_TYPE,
  DEFAULT_MODULE_TYPE,
  GRIPPER_MODULE_TYPE,
  MEMORY_MODULE_TYPE,
  STORAGE_MODULE_TYPE,
  TOOL_MODULE_TYPE,
  VISION_MODULE_TYPE,
} from '@/constants/moduleTypes'
import {
  MODULE_NAME_BATTERY,
  MODULE_NAME_CHAINSAW,
  MODULE_NAME_CHASSIS,
  MODULE_NAME_DEFAULT,
  MODULE_NAME_GRIPPER,
  MODULE_NAME_MEMORY,
  MODULE_NAME_STORAGE,
  MODULE_NAME_TOOL,
  MODULE_NAME_VISION,
} from '@/l10n/keys'

const moduleNameByType = {
  [BATTERY_MODULE_TYPE] : MODULE_NAME_BATTERY,
  [CHAINSAW_MODULE_TYPE]: MODULE_NAME_CHAINSAW,
  [CHASSIS_MODULE_TYPE] : MODULE_NAME_CHASSIS,
  [GRIPPER_MODULE_TYPE] : MODULE_NAME_GRIPPER,
  [MEMORY_MODULE_TYPE]  : MODULE_NAME_MEMORY,
  [DEFAULT_MODULE_TYPE] : MODULE_NAME_DEFAULT,
  [STORAGE_MODULE_TYPE] : MODULE_NAME_STORAGE,
  [TOOL_MODULE_TYPE]    : MODULE_NAME_TOOL,
  [VISION_MODULE_TYPE]  : MODULE_NAME_VISION,
}

export default moduleNameByType
