import dictionary from '@/l10n/dictionary'
import { UNKNOWN_KEY } from '@/l10n/keys'
import { defaultLocale } from '@/l10n/locales'

export default function translateToLocale (locale) {
  return key => {
    const text = dictionary[key] || dictionary[UNKNOWN_KEY]

    return text[locale] || text[defaultLocale]
  }
}
