import store from '@/store'

export default function getProgramById (programId) {
  const {editorState: {programs}} = store.getState()

  return programs.find(({id}) => id === programId)
}
