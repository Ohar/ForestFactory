import classNames from 'classnames'
import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import getLocalizedString from '@/l10n/getLocalizedString'
import { QUEST_HEADER_TEXT } from '@/l10n/keys'
import './styles.less'

class QuestInfo extends Component {
  renderBlock () {
    const {l10n, plot: {questIndex, questList}} = this.props
    const {text, counter, showCounter, goal, description, progressType} = questList[questIndex]

    const isFinished = goal <= counter
    const localizedText = getLocalizedString(text)
    const localizedDescription = getLocalizedString(description)
    const textToShow = showCounter
                       ? `${localizedText}: ${counter}/${goal}`
                       : localizedText

    return (
      <section className='QuestInfo'>
        <header className='QuestInfo_header'>
          {l10n[QUEST_HEADER_TEXT](questIndex + 1)}
        </header>
        <p className='QuestInfo_description'>
          {localizedDescription}
        </p>
        <section className='QuestInfo_list'>
          {
            progressType === 'checkbox'
            ? this.renderCheckbox(isFinished)
            : null
          }
          <span className={classNames('QuestInfo_text', isFinished ? 'QuestInfo_text-finished' : '')}>
            {textToShow}
          </span>
          {
            progressType === 'meter'
            ? this.renderMeter()
            : null
          }
        </section>
      </section>
    )
  }

  renderCheckbox (isFinished) {
    return (
      <span className='QuestInfo_checkbox'>
        {isFinished ? '☑' : '☐'}
      </span>
    )
  }

  renderMeter () {
    const {questIndex, questList} = this.props.plot
    const {counter, showCounter, goal} = questList[questIndex]

    return showCounter
           ? (
             <meter
               className='QuestInfo_meter'
               value={counter}
               max={goal}
               min='0'
             />
           )
           : null
  }

  render () {
    const {questIndex, questList} = this.props.plot

    return questList[questIndex]
           ? this.renderBlock()
           : null
  }
}

QuestInfo.propTypes = {
  plot: PropTypes.object,
}

QuestInfo.defaultProps = {
  plot: null,
}

function mapStateToProps (state, ownProps) {
  return {
    ...ownProps,
    plot: state.plotState,
    l10n: state.optionsState.l10n,
  }
}

export default connect(mapStateToProps)(QuestInfo)
