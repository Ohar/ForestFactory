import Phaser from 'phaser'

const COLOR = 0x0078D7
const RANGE_KOEF = .75

export default class ActiveMobCircle extends Phaser.Graphics {
	constructor (robot) {
		const game = robot.game

		super(game)

		this.game = game
		this.robot = robot
		this.range = Math.max(this.robot.width, this.robot.height) * RANGE_KOEF
		this.circle = null
		this.center = {
			x: this.robot.width / 2,
			y: this.robot.height / 2,
		}

		this.show = this.show.bind(this)
		this.hide = this.hide.bind(this)
	}

	show () {
		this.circle = this.game.add.graphics()

		this.circle.lineStyle(3, COLOR, 1)
		this.circle.drawCircle(this.center.x, this.center.y, this.range * 2)

		this.robot.addChild(this.circle)
	}

	hide () {
	  if (this.circle) {
      this.robot.removeChild(this.circle)

      this.circle.destroy()
    }
	}

	get diameter () {
		return this.range * 2
	}
}
