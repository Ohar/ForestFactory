const MENU_DEFAULT_STATE = {
  isGameSaving   : false,
  isGameDeleting : false,
  isGameLoading  : false,
  isMenuOpen     : false,
  deletingLevelId: null,
  deletingTimeout: null,
}

export default MENU_DEFAULT_STATE
