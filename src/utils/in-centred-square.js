function inCentredSquare (center, halfSideSize, point) {
	return (
		   center.x + halfSideSize > point.x
		&& center.x - halfSideSize < point.x
		&& center.y + halfSideSize > point.y
		&& center.y - halfSideSize < point.y
	)
}

export default inCentredSquare
