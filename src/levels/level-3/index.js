const levelMap = require('./levelMap.json')
const modules  = require('./modules.json')
const options  = require('./options.json')
const plot     = require('./plot.json')
const programs = require('./programs.json')
const toolbox  = require('./toolbox.xml')

module.exports = {
  levelMap,
  modules,
  options,
  plot,
  programs,
  toolbox,
  name: {
    en_GB: 'Level 3',
    ru_RU: 'Уровень 3',
  },
  id  : 'level-3',
}
