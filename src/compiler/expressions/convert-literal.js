import IRNode from '@/classes/ir-node'
import uid from 'uid'

export default function convertLiteral (node, program, loopLabels) {
	const id = uid(20)

	return new IRNode({
		id,
		code: `memory.save ${id} ${node.value}`,
	})
}
