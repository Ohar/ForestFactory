import { SET_GAMEPLAYUI_VISIBILITY } from '@/store/gui/action-types'

export default function actionGameplayUIShow () {
  return dispatch => {
    dispatch({
      type             : SET_GAMEPLAYUI_VISIBILITY,
      gameplayUIVisible: true,
    })
  }
}
