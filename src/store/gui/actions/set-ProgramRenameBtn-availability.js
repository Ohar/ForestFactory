import { SET_PROGRAM_RENAME_BTN_AVAILABILITY } from '@/store/gui/action-types'

export default function actionSetProgramRenameBtnAvailability (isProgramRenameBtnEnabled) {
  return dispatch => {
    dispatch({
      type: SET_PROGRAM_RENAME_BTN_AVAILABILITY,
      isProgramRenameBtnEnabled,
    })
  }
}
