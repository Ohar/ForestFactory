const TABS_DEFAULT_STATE = {
  activeTabId  : null,
  fullScreen   : false,
  isTabListOpen: false,
}

export default TABS_DEFAULT_STATE
