import IRNode from '@/classes/ir-node'
import convertAstNodeToIR from '@/compiler/converters/convert-ast-node-to-ir'

export default function convertAssignmentExpression (node, program, loopLabels) {
	const left  = convertAstNodeToIR(node.left, program, loopLabels),
		  right = convertAstNodeToIR(node.right, program, loopLabels)

	return new IRNode({
		id  : left.id,
		code: `
${left.code}
${right.code}
memory.save ${left.id} ${right.id}
`,
	})
}
