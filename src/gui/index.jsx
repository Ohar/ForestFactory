import GameplayUI from '@/gui/components/GameplayUI'
import LoadGameMenu from '@/gui/components/LoadGameMenu'
import Options from '@/gui/components/Options'
import MainMenu from '@/gui/components/MainMenu'
import StartLevelScreen from '@/gui/components/StartLevelScreen'
import WinScreen from '@/gui/components/WinScreen'
import signalLocaleSet from '@/store/options/signals/locale-set'
import React from 'react'
import connect from 'react-redux/es/connect/connect'
import { bindActionCreators } from 'redux'
import { ruOnDev } from 'root/config.l10n'
import { ru_RU } from '@/l10n/locales'

let started = false

function Gui ({localeSet}) {
  if (__DEV__ && !started && ruOnDev) {
    localeSet(ru_RU)
    started = true
  }

  return (
    <section className='Gui'>
      <MainMenu/>
      <GameplayUI/>
      <WinScreen/>
      <StartLevelScreen/>
      <LoadGameMenu/>
      <Options/>
    </section>
  )
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators(
    {
      localeSet: signalLocaleSet,
    },
    dispatch,
  )
}

export default connect(null, mapDispatchToProps)(Gui)
