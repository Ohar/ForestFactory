// IndexedDB: добавить множество записей

import isArray from 'lodash/isArray'
import addSingleItem from '@/singletons/db/add-single-item'

export default function addItemList (storeName) {
	return items => {
    const itemsList = isArray(items)
      ? items
      : [items]

    return Promise.all(
      itemsList.map(
        item => addSingleItem(item, storeName)
      )
    )
	}
}
