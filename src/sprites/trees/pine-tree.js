import BaseTree from '@/sprites/trees/base-tree';

export default class PineTree extends BaseTree {
	constructor ({game, x, y, key, frame, ffId}) {
		const treetopFrame = Math.random() > .5 ? 2 : 3

		super({game, x, y, key, frame, ffId}, treetopFrame)
	}
}
