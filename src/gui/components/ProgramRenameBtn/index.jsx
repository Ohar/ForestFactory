import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import actionProgramRename from '@/store/editor/actions/program-rename'
import generateEditorBtnTitle from '@/utils/generateEditorBtnTitle'
import getProgramById from '@/utils/getProgramById'
import './styles.less'
import {
  PROGRAM_RENAME_BTN_TEXT,
  PROGRAM_RENAME_REQUEST_TEXT,
  PROGRAM_RENAME_TEMPLATE_COMMON,
  PROGRAM_NOT_SELECTED_DISABLED,
} from '@/l10n/keys'

class ProgramRenameBtn extends Component {
  constructor (...args) {
    super(...args)

    this.onClick = this.onClick.bind(this)
  }

  onClick () {
    const {l10n, selectedProgramId, rename} = this.props
    const program                     = getProgramById(selectedProgramId)
    const name                        = prompt(l10n[PROGRAM_RENAME_REQUEST_TEXT], program.name)

    if (name) {
      rename(selectedProgramId, name)
    }
  }

  render () {
    const {l10n, selectedProgramId} = this.props
    const program             = getProgramById(selectedProgramId)

    const title = generateEditorBtnTitle({
      program,
      isDisabled      : !program,
      templateDisabled: l10n[PROGRAM_NOT_SELECTED_DISABLED],
      templateCommon  : l10n[PROGRAM_RENAME_TEMPLATE_COMMON],
    })

    return (
      <button
        className='ProgramRenameBtn'
        disabled={!selectedProgramId}
        onClick={this.onClick}
        title={title}
      >
        {l10n[PROGRAM_RENAME_BTN_TEXT]}
      </button>
    )
  }
}

function mapStateToProps (state, ownProps) {
  return {
    ...ownProps,
    selectedProgramId: state.editorState.selectedProgramId,
    l10n             : state.optionsState.l10n,
  }
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators({rename: actionProgramRename}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ProgramRenameBtn)
