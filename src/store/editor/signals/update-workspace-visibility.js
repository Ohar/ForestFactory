import { UPDATE_WORKSPACE_VISIBILITY } from '@/store/editor/action-types'
import store from '@/store/index'
import getProgramById from '@/utils/getProgramById'
import getProgramId from '@/utils/getProgramId'

export default function signalUpdateWorkSpaceVisibility () {
  return dispatch => {
    const {robotsPrograms, robot} = store.getState().editorState
    const programId               = getProgramId(robotsPrograms, robot)
    const program                 = getProgramById(programId)
    const isWorkspaceVisible      = Boolean(program)

    dispatch({
      type: UPDATE_WORKSPACE_VISIBILITY,
      isWorkspaceVisible,
    })
  }
}
