import L10n from '@/l10n'
import { defaultLocale } from '@/l10n/locales'

const OPTIONS_DEFAULT_STATE = {
  locale: defaultLocale,
  l10n: new L10n(defaultLocale),
}

export default OPTIONS_DEFAULT_STATE
