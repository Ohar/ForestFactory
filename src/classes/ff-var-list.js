import uid from 'uid'

class FFVarList {
	constructor () {
		this.list = {}
	}
	
	get (ffId) {
		return this.list[ffId]
	}
	
	add (item, ffId = uid(20)) {
		item.ffId = ffId
		
		this.list[ffId] = item
		
		return item
	}
}

export default FFVarList
