import moduleNameByType from '@/constants/moduleNameByType'
import getModuleById from '@/utils/getModuleById'
import getModuleOwner from '@/utils/getModuleOwner'
import classNames from 'classnames'
import React from 'react'
import connect from 'react-redux/es/connect/connect'
import './styles.less'
import { MODULE_EMPTY_NAME, MODULE_OWNER_EMPTY_NAME } from '@/l10n/keys'

function ModuleInfo ({l10n, selectedModuleId}) {
  const module     = getModuleById(selectedModuleId)
  const owner      = getModuleOwner(selectedModuleId)
  const moduleName = module
                     ? l10n[moduleNameByType[module.moduleType]]
                     : l10n[MODULE_EMPTY_NAME]
  const ownerName  = owner
                     ? owner.name
                     : l10n[MODULE_OWNER_EMPTY_NAME]

  return (
    <section className='ModuleInfo'>
      <div className={classNames(
        'ModuleInfo_img',
        module
        ? `ModuleInfo_img-${module.moduleType}`
        : 'ModuleInfo_img-empty',
      )}/>
      <p className={classNames(
        'ModuleInfo_name',
        module
        ? `ModuleInfo_name-${module.moduleType}`
        : '',
      )}>
        {moduleName}
      </p>
      <p className='ModuleInfo_owner'>
        {ownerName}
      </p>
    </section>
  )
}

function mapStateToProps (state, ownProps) {
  return {
    ...ownProps,
    selectedModuleId: state.editorState.selectedModuleId,
    l10n           : state.optionsState.l10n,
  }
}

export default connect(mapStateToProps)(ModuleInfo)
