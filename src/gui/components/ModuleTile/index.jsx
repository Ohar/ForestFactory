import moduleNameByType from '@/constants/moduleNameByType'
import actionModuleSetSelected from '@/store/editor/actions/module-set-selected'
import getModuleOwner from '@/utils/getModuleOwner'
import classNames from 'classnames'
import React from 'react'
import connect from 'react-redux/es/connect/connect'
import { bindActionCreators } from 'redux'
import './styles.less'
import { MODULE_OWNER_EMPTY_NAME } from '@/l10n/keys'

function ModuleTile ({l10n, selectedModuleId, moduleSetSelected, module: {id, moduleType}}) {
  const owner     = getModuleOwner(id)
  const ownerName = owner
                    ? owner.name
                    : l10n[MODULE_OWNER_EMPTY_NAME]
  const name = l10n[moduleNameByType[moduleType]]

  return (
    <section className='ModuleTile'>
      <input
        type='radio'
        name='module_selected'
        checked={selectedModuleId === id}
        id={id}
        value={id}
        className='ModuleTile_input'
        onChange={() => moduleSetSelected(id)}
      />
      <label
        htmlFor={id}
        title={`${name}\n${ownerName}`}
        className={classNames(
          'ModuleTile_label',
          moduleType
          ? `ModuleTile_label-${moduleType}`
          : 'ModuleTile_label-empty',
        )}
      >
        {name}
        <br/>
        {ownerName}
      </label>
    </section>
  )
}

function mapStateToProps (state, ownProps) {
  return {
    ...ownProps,
    selectedModuleId: state.editorState.selectedModuleId,
    l10n: state.optionsState.l10n,
  }
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators(
    {
      moduleSetSelected: actionModuleSetSelected,
    },
    dispatch,
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(ModuleTile)
