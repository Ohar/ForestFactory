import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import programDelete from '@/store/editor/actions/program-delete'
import generateEditorBtnTitle from '@/utils/generateEditorBtnTitle'
import getProgramById from '@/utils/getProgramById'
import './styles.less'
import {
  PROGRAM_DELETE_BTN_TEMPLATE_COMMON,
  PROGRAM_NOT_SELECTED_DISABLED,
  PROGRAM_DELETE_BTN_TEXT,
} from '@/l10n/keys'

function ProgramDeleteBtn ({l10n, programDelete, selectedProgramId}) {
  const program = getProgramById(selectedProgramId)

  const title = generateEditorBtnTitle({
    program,
    isDisabled      : !selectedProgramId,
    templateDisabled: l10n[PROGRAM_NOT_SELECTED_DISABLED],
    templateCommon  : l10n[PROGRAM_DELETE_BTN_TEMPLATE_COMMON],
  })

  return (
    <button
      className='ProgramDeleteBtn'
      onClick={() => programDelete(selectedProgramId)}
      disabled={!selectedProgramId}
      title={title}
    >
      {l10n[PROGRAM_DELETE_BTN_TEXT]}
    </button>
  )
}

function mapStateToProps (state, ownProps) {
  return {
    ...ownProps,
    selectedProgramId: state.editorState.selectedProgramId,
    l10n: state.optionsState.l10n,
  }
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators({programDelete}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ProgramDeleteBtn)
