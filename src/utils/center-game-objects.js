export default function centerGameObjects (objects) {
	return objects.forEach(object => {
		object.anchor.setTo(0.5)
	})
}
