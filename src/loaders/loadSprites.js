import Phaser from 'phaser'

export default function loadSprites (levelMap, load) {
  return new Promise(resolve => {
    load.tilemap('levelMap', null, levelMap, Phaser.Tilemap.TILED_JSON)

    // Есть в карте
    levelMap.tilesets.forEach(
      ({name, image, tilewidth, tileheight}) => {
        const imagePathFixed = image.replace('..\/..\/..\/resources', '.\/resources')
        load.spritesheet(name, imagePathFixed, tilewidth, tileheight)
      }
    )

    // Нет в карте
    load
      .spritesheet('trunk', './resources/images/LPC Base Assets/tiles/trunk.png', 96, 96)
      .spritesheet('treetop', './resources/images/trees/treetop.png', 96, 112)

    load.onLoadComplete.add(resolve)
    load.start()
  })
}
