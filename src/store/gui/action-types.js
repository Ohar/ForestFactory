export const HIDE_START_LEVEL_SCREEN             = 'HIDE_START_LEVEL_SCREEN'
export const SET_GAMEPLAYUI_VISIBILITY           = 'SET_GAMEPLAYUI_VISIBILITY'
export const SET_LOAD_GAME_MENU_VISIBILITY       = 'SET_LOAD_GAME_MENU_VISIBILITY'
export const SET_MAIN_MENU_VISIBILITY            = 'SET_MAIN_MENU_VISIBILITY'
export const SET_OPTIONS_VISIBILITY              = 'SET_OPTIONS_VISIBILITY'
export const SET_REPOSITORY_AVAILABILITY         = 'SET_REPOSITORY_AVAILABILITY'
export const SET_EDITOR_AVAILABILITY             = 'SET_EDITOR_AVAILABILITY'
export const SET_MODULES_AVAILABILITY            = 'SET_MODULES_AVAILABILITY'
export const SET_PROGRAM_DELETE_BTN_AVAILABILITY = 'SET_PROGRAM_DELETE_BTN_AVAILABILITY'
export const SET_PROGRAM_RENAME_BTN_AVAILABILITY = 'SET_PROGRAM_RENAME_BTN_AVAILABILITY'
export const SET_PROGRAM_COPY_BTN_AVAILABILITY   = 'SET_PROGRAM_COPY_BTN_AVAILABILITY'
export const SET_WINSCREEN_VISIBILITY            = 'SET_WINSCREEN_VISIBILITY'
export const SHOW_START_LEVEL_SCREEN             = 'SHOW_START_LEVEL_SCREEN'
