import Phaser from 'phaser'

class FFPoint extends Phaser.Point {
	constructor (obj) {
		const x = 'worldX' in obj
			      ? obj.worldX
			      : obj.x,
		      y = 'worldY' in obj
			      ? obj.worldY
			      : obj.y
		
		super(x, y)
		
		this.obj = obj
	}
}

export default FFPoint
