const nameSizeList = [0, 6, 9, 12, 15, 18, 21, 24, Infinity]

export default function nameSizeSetter (name) {
  return nameSizeList.findIndex(
    (size, i, arr) => {
      const nextSize = arr[i + 1]
      const len = name.length

      return nextSize
             ? len > size && len <= nextSize
             : len > size
    }
  )
}
