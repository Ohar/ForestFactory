export const en_GB = 'en_GB'
export const ru_RU = 'ru_RU'

export const defaultLocale = en_GB

const localesList = [en_GB, ru_RU]

export default localesList
