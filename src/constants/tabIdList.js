const tabIdList = {
  editor    : 'EDITOR_TAB_ID',
  repository: 'REPOSITORY_TAB_ID',
  modules   : 'MODULES_TAB_ID',
}

export default tabIdList
