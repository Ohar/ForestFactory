import Level from '@/classes/level'
import levelMap from '@/levels/level-test/levelMap.json'
import modules from '@/levels/level-test/modules.json'
import programs from '@/levels/level-test/programs.json'
import toolbox from '@/levels/level-test/toolbox.xml'

export default new Level(
  {
    programs,
    levelMap,
    modules,
    toolbox,
    name: {
      en_GB: 'Test Level',
      ru_RU: 'Тестовый уровень',
    },
    id  : 'level-0',
  },
)
