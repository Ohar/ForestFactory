export const fontSize  = 12
export const fontStyle = {
  fontSize,
  stroke         : 'white',
  strokeThickness: 4,
}
