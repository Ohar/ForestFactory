import store from '@/store'
import { PROGRAM_CONTROL_BTN_PAUSE, PROGRAM_CONTROL_BTN_START, PROGRAM_CONTROL_BTN_STOP } from '@/l10n/keys'

const {optionsState: {l10n}} = store.getState()

const PROGRAM_CONTROLS_LIST = [
	{
		icon       : '▶',
		text       : l10n[PROGRAM_CONTROL_BTN_START],
		commandName: 'start',
	},
	{
		icon       : '⏸',
		text       : l10n[PROGRAM_CONTROL_BTN_PAUSE],
		commandName: 'pause',
	},
	{
		icon       : '⏹',
		text       : l10n[PROGRAM_CONTROL_BTN_STOP],
		commandName: 'stop',
	},
]

export default PROGRAM_CONTROLS_LIST
