import * as actionTypes from '@/store/gui/action-types'
import DEFAULT_STATE from '@/store/gui/default_state'

export default function guiReducer (state = DEFAULT_STATE, action) {
	switch (action.type) {
		case actionTypes.SET_GAMEPLAYUI_VISIBILITY:
			return {
				...state,
				gameplayUIVisible: action.gameplayUIVisible,
			}

		case actionTypes.SET_WINSCREEN_VISIBILITY:
			return {
				...state,
				winScreenVisible: action.winScreenVisible,
			}

		case actionTypes.SET_MAIN_MENU_VISIBILITY:
			return {
				...state,
				mainMenuVisible: action.mainMenuVisible,
			}

		case actionTypes.SET_LOAD_GAME_MENU_VISIBILITY:
			return {
				...state,
        loadGameMenuVisible: action.loadGameMenuVisible,
			}

    case actionTypes.SET_EDITOR_AVAILABILITY:
      return {
        ...state,
        isEditorEnabled: action.isEditorEnabled,
      }

    case actionTypes.SET_MODULES_AVAILABILITY:
    	return {
        ...state,
        isModulesEnabled: action.isModulesEnabled,
      }

		case actionTypes.SET_REPOSITORY_AVAILABILITY:
			return {
				...state,
        isRepositoryEnabled: action.isRepositoryEnabled,
			}

		case actionTypes.SET_PROGRAM_COPY_BTN_AVAILABILITY:
			return {
				...state,
        isProgramCopyBtnEnabled: action.isProgramCopyBtnEnabled,
			}

		case actionTypes.SET_PROGRAM_DELETE_BTN_AVAILABILITY:
			return {
				...state,
        isProgramDeleteBtnEnabled: action.isProgramDeleteBtnEnabled,
			}

		case actionTypes.SET_PROGRAM_RENAME_BTN_AVAILABILITY:
			return {
				...state,
        isProgramRenameBtnEnabled: action.isProgramRenameBtnEnabled,
			}

		case actionTypes.SHOW_START_LEVEL_SCREEN:
			return {
				...state,
        startLevelScreenVisible: true,
        levelName: action.levelName,
        nextLvlId: action.nextLvlId,
			}

		case actionTypes.HIDE_START_LEVEL_SCREEN:
			return {
				...state,
        startLevelScreenVisible: false,
			}

		case actionTypes.SET_OPTIONS_VISIBILITY:
			return {
				...state,
        optionsVisible: action.optionsVisible,
			}

		default:
			return state
	}
}
