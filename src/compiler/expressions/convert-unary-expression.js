import IRNode from '@/classes/ir-node'
import uid from 'uid'

export default function convertUnaryExpression (node, program, loopLabels) {
	const id  = uid(20),
		  val = node.prefix
			? `${node.operator}${node.argument.value}`
			: `${node.argument.value}${node.operator}`

	return new IRNode({
		id,
		code: `memory.save ${id} ${val}`,
	})
}
