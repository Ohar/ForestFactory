// IndexedDB: Удалить одну запись

import dbConnectionPromise from '@/singletons/db/db-connection-promise'

export default function deleteSingleItem (storeName) {
	return itemKey => dbConnectionPromise
    .then(
      dbConnection => {
        const store = dbConnection.transaction(storeName, 'readwrite').objectStore(storeName)

        return new Promise(
          (resolve, reject) => {
            const request = store.delete(itemKey)

            request.onsuccess = () => {
              resolve(true)
            }

            request.onerror = event => {
              reject(event.target.errorCode)
            }
          }
        )
      }
    )
}
