const level_1 = require('./level-1')
const level_2 = require('./level-2')
const level_3 = require('./level-3')

const levelList = [
  level_1,
  level_2,
  level_3,
]

module.exports = levelList
