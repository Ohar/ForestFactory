import Blockly from '@/blockly/blockly'
import { CHASSIS_MODULE_TYPE } from '@/constants/moduleTypes'
import store from '@/store'
import { BLOCKLY_MOVE_BY_TEXT, BLOCKLY_MOVE_BY_X_ARROW_TEXT, BLOCKLY_MOVE_BY_Y_ARROW_TEXT } from '@/l10n/keys'

const BJS = Blockly.JavaScript

const BlockMoveByXYBlock = {
	name      : 'FF_MoveByXY',
	generator : block => {
		const X = BJS.valueToCode(block, 'X', BJS.ORDER_ATOMIC),
		      Y = BJS.valueToCode(block, 'Y', BJS.ORDER_ATOMIC);

		return `${CHASSIS_MODULE_TYPE}.moveByXY(${X}, ${Y})\n`
	},
	definition: {
		init () {
      const {optionsState: {l10n}} = store.getState()

			this.appendDummyInput()
				.appendField(l10n[BLOCKLY_MOVE_BY_TEXT]);
			this.appendValueInput('X')
				.setCheck('Number')
				.appendField(l10n[BLOCKLY_MOVE_BY_X_ARROW_TEXT]);
			this.appendValueInput('Y')
				.setCheck('Number')
				.appendField(l10n[BLOCKLY_MOVE_BY_Y_ARROW_TEXT]);
			this.setInputsInline(true);
			this.setPreviousStatement(true, null);
			this.setNextStatement(true, null);
			this.setColour(0);
		}
	},
}

export default BlockMoveByXYBlock
