import dictionary from '@/l10n/dictionary'
import translateToLocale from '@/l10n/translateToLocale'

export default class L10n {
  constructor (locale) {
    const translate = translateToLocale(locale)

    Object
      .keys(dictionary)
      .forEach(
        key => {
          this[key] = translate(key)
        },
      )
  }
}
