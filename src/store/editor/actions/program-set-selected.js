import {SET_SELECTED_PROGRAM} from '@/store/editor/action-types'

export default function actionProgramSetSelected (selectedProgramId) {
    return dispatch => {
	    dispatch({
		    type: SET_SELECTED_PROGRAM,
        selectedProgramId,
	    })
    }
}
