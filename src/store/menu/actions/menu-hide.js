import {MENU_HIDE} from '@/store/menu/action-types'

export default function actionMenuHide () {
	return dispatch => {
		dispatch({type: MENU_HIDE})
	}
}
