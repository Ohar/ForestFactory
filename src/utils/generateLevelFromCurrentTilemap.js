import { isNumber } from 'lodash'
import Level from '@/classes/level'
import { wood } from '@/constants/game_objects_data'
import store from '@/store'
import Robot from '@/sprites/mobs/robot'
import Resource from '@/sprites/pobs/resource'
import BaseTree from '@/sprites/trees/base-tree'
import filterObjectGroups from '@/utils/filterObjectGroups'
import getGroupName from '@/utils/getGroupName'
import getGroupType from '@/utils/getGroupType'
import getObjGid from '@/utils/getObjGid'
import sortByName from '@/utils/sortByName'
import sortByXY from '@/utils/sortByXY'

const GAP = {
  trees: {
    x: 32,
    y: -16,
  },
}

export default function generateLevelFromCurrentTilemap (tileMap) {
  const {currentLevel, world}                                  = tileMap.game
  const {editorState: {programs, moduleList}, plotState: plot} = store.getState()

  let id = 0

  const objectGroups = world
    .children
    .filter(filterObjectGroups)
    .map(
      group => {
        switch (group.name) {
          case 'robots':
          case 'trees':
            return group

          default:
            return group.children[0]
        }
      },
    )
    .sort(sortByName)
    .map(
      group => ({
        draworder: 'topdown',
        name     : getGroupName(group),
        objects  : group.children
          .map(
            obj => {
              let gapX = 0
              let gapY = 0

              switch (group.name) {
                case 'trees':
                  gapX = GAP.trees.x
                  gapY = GAP.trees.y
                  break
              }

              const hasProgramId  = obj instanceof Robot && obj.programId

              // TODO: refactor modules saving
              const hasBatteryId  = obj instanceof Robot && obj.modules && obj.modules.battery && obj.modules.battery.id
              const hasChainsawId = obj instanceof Robot && obj.modules && obj.modules.chainsaw && obj.modules.chainsaw.id
              const hasChassisId  = obj instanceof Robot && obj.modules && obj.modules.chassis && obj.modules.chassis.id
              const hasGripperId  = obj instanceof Robot && obj.modules && obj.modules.gripper && obj.modules.gripper.id
              const hasMemoryId   = obj instanceof Robot && obj.modules && obj.modules.memory && obj.modules.memory.id
              const hasStorageId  = obj instanceof Robot && obj.modules && obj.modules.storage && obj.modules.storage.id
              const hasVisionId   = obj instanceof Robot && obj.modules && obj.modules.vision && obj.modules.vision.id

              const hasDoneQuests = obj.hasDoneQuests()
              const hasVolume     = obj instanceof Resource && isNumber(obj.volume)

              const properties    = {
                ffId: obj.ffId,
                ...(
                  hasProgramId
                  ? {programId: obj.programId}
                  : {}
                ),
                ...(
                  hasBatteryId
                  ? {batteryId: obj.modules.battery.id}
                  : {}
                ),
                ...(
                  hasChainsawId
                  ? {chainsawId: obj.modules.chainsaw.id}
                  : {}
                ),
                ...(
                  hasChassisId
                  ? {chassisId: obj.modules.chassis.id}
                  : {}
                ),
                ...(
                  hasGripperId
                  ? {gripperId: obj.modules.gripper.id}
                  : {}
                ),
                ...(
                  hasMemoryId
                  ? {memoryId: obj.modules.memory.id}
                  : {}
                ),
                ...(
                  hasStorageId
                    ? {storageId: obj.modules.storage.id}
                : {}
                ),
                ...(
                  hasVisionId
                    ? {visionId: obj.modules.vision.id}
                : {}
                ),
                ...(
                  hasVolume
                  ? {volume: obj.volume}
                  : {}
                ),
                ...(
                  hasDoneQuests
                  ? {questDoneString: obj.questDoneListToString()}
                  : {}
                ),
              }
              const propertytypes = {
                ffId: 'string',
                ...(
                  hasProgramId
                  ? {programId: 'string'}
                  : {}
                ),
                ...(
                  hasBatteryId
                  ? {memoryId: 'string'}
                  : {}
                ),
                ...(
                  hasChainsawId
                  ? {memoryId: 'string'}
                  : {}
                ),
                ...(
                  hasChassisId
                  ? {memoryId: 'string'}
                  : {}
                ),
                ...(
                  hasGripperId
                  ? {memoryId: 'string'}
                  : {}
                ),
                ...(
                  hasMemoryId
                  ? {memoryId: 'string'}
                  : {}
                ),
                ...(
                  hasStorageId
                  ? {memoryId: 'string'}
                  : {}
                ),
                ...(
                  hasVisionId
                  ? {memoryId: 'string'}
                  : {}
                ),
                ...(
                  hasVolume
                  ? {volume: 'int'}
                  : {}
                ),
                ...(
                  hasDoneQuests
                  ? {questDoneString: 'string'}
                  : {}
                ),
              }

              return {
                gid   : getObjGid(obj),
                height: obj instanceof BaseTree
                        ? 32 // Tree has complicated sprites, see width
                        : obj.body.height,
                id    : id++, // TODO: Где брать?
                name  : obj.name || '',

                properties,
                propertytypes,

                rotation: obj.body.rotation,
                // TODO: separate wood from Resource
                type    : obj instanceof Resource
                          ? 'wood'
                          : getGroupType(group),
                visible : obj.visible,
                width   : obj instanceof BaseTree
                          ? 32 // Tree has complicated sprites, see height
                          : obj.body.width,
                x       : Math.round(obj.body.x) + gapX,
                y       : obj.body.y + gapY,
              }
            },
          )
          .sort(sortByXY),
        opacity  : 1,
        type     : 'objectgroup',
        visible  : true,
        x        : 0,
        y        : 0,
      }),
    )

  const placesObjectGroup = {
    draworder: 'topdown',
    name     : 'places',
    objects  : world.places
      .sort(sortByXY)
      .map(
        place => ({
          height  : place.height,
          id      : id++,
          name    : place.name,
          rotation: 0,
          type    : '',
          visible : true,
          width   : place.width,
          x       : place.x,
          y       : place.y,
        }),
      ),
    opacity  : 1,
    type     : 'objectgroup',
    visible  : true,
    x        : 0,
    y        : 0,
  }

  const tileLayers = tileMap.layers.map(
    layer => {
      const data = []

      layer.data.forEach(
        row => {
          row.forEach(
            tile => {
              if (tile.index === -1) {
                data.push(0)
              } else {
                data.push(tile.index)
              }
            },
          )
        },
      )

      return {
        data,
        height : 100,
        name   : layer.name,
        opacity: 1,
        type   : 'tilelayer',
        visible: true,
        width  : 100,
        x      : 0,
        y      : 0,
      }
    },
  )

  console.info('moduleList', moduleList) // eslint-disable-line no-console
  moduleList.forEach(
    module => {
      console.info('module', module) // eslint-disable-line no-console
      try {
        console.info('module.toJson()', module.toJson()) // eslint-disable-line no-console
      } catch (e) {
        console.info('Err .toJson()', e) // eslint-disable-line no-console
      }
    }
  )

  return new Level({
    ...currentLevel,
    id      : undefined,
    date    : Date.now(),
    programs: programs.map(e => e.toJson()),
    modules : moduleList.map(e => e.toJson()),
    plot,
    levelMap: {
      ...currentLevel.levelMap,
      nextobjectid: 208, // TODO: Что это?
      layers      : [
        ...tileLayers,
        ...objectGroups,
        placesObjectGroup,
      ],
    },
  })
}
