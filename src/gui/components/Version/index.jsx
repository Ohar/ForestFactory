import React from 'react'
import connect from 'react-redux/es/connect/connect'
import { VERSION_TEXT } from '@/l10n/keys'
import { version } from 'root/package.json'
import './styles.less'

function Version ({l10n}) {
  return (
    <span className='Version'>
      {l10n[VERSION_TEXT](version)}
    </span>
  )
}

function mapStateToProps (state, ownProps) {
  return {
    ...ownProps,
    l10n: state.optionsState.l10n,
  }
}

export default connect(mapStateToProps)(Version)
