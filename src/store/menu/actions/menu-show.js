import {MENU_SHOW} from '@/store/menu/action-types'

export default function actionMenuShow () {
	return dispatch => {
		dispatch({type: MENU_SHOW})
	}
}
