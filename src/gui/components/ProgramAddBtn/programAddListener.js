// TODO: use Symbol
const callbackPropName = 'callbackPropName'

const programAddListener = {
  [callbackPropName]: null,
  get hasCallback () {
    return Boolean(this[callbackPropName])
  },
  addCallback (callback) {
    this[callbackPropName] = callback
  },
  removeCallback () {
    this[callbackPropName] = null
  },
  runCallback () {
    this[callbackPropName]()
    this.removeCallback()
  },
}

export default programAddListener
