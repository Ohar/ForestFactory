import 'pixi'
import 'p2'
import Phaser from 'phaser'

import BootState from '@/states/bootstate'
import GameState from '@/states/gamestate'
import FFVarList from '@/classes/ff-var-list'
import CANVAS_PARENT_ID from '@/constants/canvas_parent_id';

export default class FFGame extends Phaser.Game {
	constructor (levelId) {
		const width  = Math.max(document.documentElement.clientWidth, 800),
		      height = Math.max(document.documentElement.clientHeight, 600)

		super(width, height, Phaser.AUTO, CANVAS_PARENT_ID, null)

		this.ffVarList = new FFVarList()
		this.levelId = levelId
		this.robotIdCounter = 0

		this.state.add('bootState', BootState, false)
		this.state.add('gameState', GameState, false)

		this.state.start('bootState')
	}
}
