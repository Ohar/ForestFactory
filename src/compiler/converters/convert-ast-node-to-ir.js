import IRNode from '@/classes/ir-node'
import convertAssignmentExpression from '@/compiler/expressions/convert-assignment-expression'
import convertBinaryExpression from '@/compiler/expressions/convert-binary-expression'
import convertBlockStatement from '@/compiler/expressions/convert-block-statement'
import convertBreakStatement from '@/compiler/expressions/convert-break-statement'
import convertExpressionStatement from '@/compiler/expressions/convert-expression-statement'
import convertCallExpression from '@/compiler/expressions/convert-call-expression'
import convertContinueStatement from '@/compiler/expressions/convert-continue-statement'
import convertForStatement from '@/compiler/expressions/convert-for-statement'
import convertIdentifier from '@/compiler/expressions/convert-identifier'
import convertIfStatement from '@/compiler/expressions/convert-if-statement'
import convertLiteral from '@/compiler/expressions/convert-literal'
import convertMemberExpression from '@/compiler/expressions/convert-member-expression'
import convertUnaryExpression from '@/compiler/expressions/convert-unary-expression'
import convertWhileStatement from '@/compiler/expressions/convert-while-statement'

export default function convertAstNodeToIR (node, program, loopLabels) {
	switch (node && node.type) {
		case 'AssignmentExpression':
			return convertAssignmentExpression(node, program, loopLabels)

		case 'BinaryExpression':
			return convertBinaryExpression(node, program, loopLabels)

		case 'BlockStatement':
		case 'Program':
			return convertBlockStatement(node, program, loopLabels)

		case 'BreakStatement':
			return convertBreakStatement(node, program, loopLabels)

		case 'CallExpression':
			return convertCallExpression(node, program, loopLabels)

		case 'ContinueStatement':
			return convertContinueStatement(node, program, loopLabels)

		case 'ExpressionStatement':
			return convertExpressionStatement(node, program, loopLabels)

		case 'ForStatement':
			return convertForStatement(node, program, loopLabels)

		case 'Identifier':
			return convertIdentifier(node, program, loopLabels)

		case 'IfStatement':
			return convertIfStatement(node, program, loopLabels)

		case 'Literal':
			return convertLiteral(node, program, loopLabels)

		case 'MemberExpression':
			return convertMemberExpression(node, program, loopLabels)

		case 'UnaryExpression':
			return convertUnaryExpression(node, program, loopLabels)

		case 'WhileStatement':
			return convertWhileStatement(node, program, loopLabels)

		default:
			return new IRNode()
	}
}
