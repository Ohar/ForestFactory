import store from '@/store/index'

export default function openTabListener (callback, {activeTabId: curVal}, tabId) {
  let isDone = false

  return () => {
    let nextVal = store.getState().tabState.activeTabId

    if (!isDone && (curVal === tabId || nextVal === tabId)) {
      isDone = true
      callback()
    }

    if (curVal !== nextVal) {
      curVal = nextVal

      if (!isDone && (curVal === tabId || nextVal === tabId)) {
        isDone = true
        callback()
      }
    }
  }
}
