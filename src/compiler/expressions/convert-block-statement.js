import IRNode from '@/classes/ir-node'
import convertAstNodeToIR from '@/compiler/converters/convert-ast-node-to-ir'
import uid from 'uid'

export default function convertBlockStatement (node, program, loopLabels) {
	return new IRNode({
		id  : uid(20),
		code: node.body
		  .map(childNode => convertAstNodeToIR(childNode, program, loopLabels).code)
		  .join('\n'),
	})
}
