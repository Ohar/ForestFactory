import {LOCALE_SET} from '@/store/options/action-types'
import L10n from '@/l10n'

export default function signalLocaleSet (locale) {
  console.info('signalLocaleSet', 1) // eslint-disable-line no-console
  return dispatch => {
    console.info('signalLocaleSet', 2) // eslint-disable-line no-console
    const l10n = new L10n(locale)

    console.info('signalLocaleSet', 3) // eslint-disable-line no-console
    dispatch({
      l10n,
      locale,
      type: LOCALE_SET,
    })
  }
}
