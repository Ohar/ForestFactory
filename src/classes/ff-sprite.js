import Phaser from 'phaser'

export default class FFSprite extends Phaser.Sprite {
	constructor ({game, x, y, key, frame, questDoneString = ''}, ffId) {
		super(game, x, y, key, frame)

    this.updateQuest = this.updateQuest.bind(this)

    this._questHandlerList = []
    this._questDoneList = questDoneString
	    .split(',')
	    .filter(e => e.length)
	    .map(e => Number(e))

    this.game.ffVarList.add(this, ffId)
	}

  addToQuestDoneList (questId) {
    this._questDoneList.push(questId)
  }

  checkInQuestDoneList (questId) {
    return this._questDoneList.find(e => e === questId)
  }

  hasDoneQuests () {
    return Boolean(this._questDoneList.length)
  }

  questDoneListToString () {
    return this._questDoneList.join()
  }

  addToQuestHandlerList (element) {
    this._questHandlerList.push(element)
    this._questHandlerList.forEach(e => e())
	}

  removeFromQuestHandlerList (element) {
			this._questHandlerList = this._questHandlerList.filter(e => e !== element)
	}

  updateQuest () {
		if (this._questHandlerList.length) {
      this._questHandlerList.forEach(e => e())
		}
  }
}
