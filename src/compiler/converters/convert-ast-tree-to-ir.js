import convertAstFunctionToIR from '@/compiler/converters/convert-ast-function-to-ir'
import convertAstNodeToIR from '@/compiler/converters/convert-ast-node-to-ir'
import IRNode from '@/classes/ir-node'

export default function convertAstTreeToIR (tree, program) {
  const functionNodes = convertAstFunctionToIR(tree, program),
        otherCode     = convertAstNodeToIR(tree, program).code

  return new IRNode({
    id  : null,
    code: `${functionNodes.code}
${otherCode}`,
  })
}
