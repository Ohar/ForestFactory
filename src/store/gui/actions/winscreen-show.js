import { SET_WINSCREEN_VISIBILITY } from '@/store/gui/action-types'

export default function actionWinScreenShow () {
  return dispatch => {
    dispatch({
      type            : SET_WINSCREEN_VISIBILITY,
      winScreenVisible: true,
    })
  }
}
