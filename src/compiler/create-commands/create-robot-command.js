export default function createRobotCommand (robot, moduleName, actionName, actionParams) {

	const [resultName, ...paramNames] = actionParams

	return () => {
		const args = paramNames.map(name => robot.modules.memory.load(name)),
		      call = robot.modules[moduleName][actionName](...args)

		robot.modules.memory.save(resultName, call.resultForSave)

		return call.finished
	}
}
