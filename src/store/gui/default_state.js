const DEFAULT_STATE = {
  gameplayUIVisible        : false,
  mainMenuVisible          : true,
  winScreenVisible         : false,
  startLevelScreenVisible  : false,
  levelName                : null,
  nextLvlId                : null,
  loadGameMenuVisible      : false,
  isRepositoryEnabled      : true,
  isEditorEnabled          : true,
  isModulesEnabled         : true,
  isProgramCopyBtnEnabled  : true,
  isProgramDeleteBtnEnabled: true,
  isProgramRenameBtnEnabled: true,
  optionsVisible           : false,
}

export default DEFAULT_STATE
