const {name, version} = require('./../package.json')
const {arch}          = require('./../config.build.json')

function generateFileName (platform) {
  return `${name}-${version}-${platform}-${arch}.zip`
}

module.exports = generateFileName
