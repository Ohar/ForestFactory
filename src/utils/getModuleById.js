import store from '@/store'

export default function getModuleById (moduleId) {
  const {editorState: {moduleList}} = store.getState()

  return moduleList.find(({id}) => id === moduleId)
}
