import { SET_OPTIONS_VISIBILITY } from '@/store/gui/action-types'

export default function actionOptionsHide () {
  return dispatch => {
    dispatch({
      type          : SET_OPTIONS_VISIBILITY,
      optionsVisible: false,
    })
  }
}
