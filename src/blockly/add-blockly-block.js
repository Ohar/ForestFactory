import Blockly from '@/blockly/blockly'
import blocklyBlockList from '@/blockly/blockly-block-list'

export default function addBlocklyBlock () {
    blocklyBlockList.forEach(
		({generator, name, definition}) => {
			if (generator) {
				Blockly.JavaScript[name] = generator
			}

			if (definition) {
				Blockly.Blocks[name] = definition
			}
		}
	)
}
