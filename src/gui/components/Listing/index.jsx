import './styles.less';

import React from 'react';
import { connect } from 'react-redux'

function mapStateToProps (state, ownProps) {
	return {
		...ownProps,
		ir: state.editorState.ir
	}
}

function Listing ({js, xml, ir}) {
	return (
		<ul className="Listing">
			<li className="Listing_item">
				<header className="Listing_header">XML</header>
				<textarea className="Listing_code"
				          value={xml}
				          readOnly/>
			</li>
			<li className="Listing_item">
				<header className="Listing_header">JavaScript</header>
				<textarea className="Listing_code"
				          value={js}
				          readOnly/>
			</li>
			<li className="Listing_item">
				<header className="Listing_header">IR</header>
				<textarea className="Listing_code"
				          value={ir}
				          readOnly/>
			</li>
		</ul>
	)
}

export default connect(mapStateToProps)(Listing)
