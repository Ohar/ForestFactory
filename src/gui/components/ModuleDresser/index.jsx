import moduleNameByType from '@/constants/moduleNameByType'
import { moduleGameTypes } from '@/constants/moduleTypes'
import classNames from 'classnames'
import React from 'react'
import connect from 'react-redux/es/connect/connect'
import { MODULE_EMPTY, ROBOT_NOT_SELECTED } from '@/l10n/keys'
import './styles.less'

function ModuleDresser ({l10n, robot}) {
  return (
    <section className='ModuleDresser'>
      <p className='ModuleDresser_name'>
        { robot && robot.name || l10n[ROBOT_NOT_SELECTED] }
      </p>
      <div className='ModuleDresser_doll'>
        {
          robot && robot.modules
          ? moduleGameTypes
            .map(
              (moduleType, i) => {
                const module = robot.modules[moduleType]

                return (
                  <p
                    key={i}
                    className='ModuleDresser_module'
                  >
                    <span className={classNames(
                      'ModuleDresser_img',
                      module
                      ? `ModuleDresser_img-${moduleType}`
                      : 'ModuleDresser_img-empty',
                    )}/>
                    {
                      module
                      ? `${l10n[moduleNameByType[moduleType]]} / ${module.id}`
                      : l10n[MODULE_EMPTY]
                    }
                  </p>
                )
              }
            )
          : null
        }
      </div>
    </section>
  )
}

function mapStateToProps (state, ownProps) {
  return {
    ...ownProps,
    robot: state.editorState.robot,
    l10n : state.optionsState.l10n,
  }
}

export default connect(mapStateToProps)(ModuleDresser)
