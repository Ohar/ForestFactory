import db from '@/singletons/db/db'

export default {
  getLevel   : db.getItem('level'),
  delLevel   : db.del('level'),
  getAllLevel: db.getAll('level'),
  setLevel   : db.putItem('level'),
}
