import { LANGUAGE_TOGGLER_EN_GB, LANGUAGE_TOGGLER_RU_RU } from '@/l10n/keys'
import { en_GB, ru_RU } from '@/l10n/locales'

const langNamesList = {
  [en_GB]: LANGUAGE_TOGGLER_EN_GB,
  [ru_RU]: LANGUAGE_TOGGLER_RU_RU,
}

export default langNamesList
