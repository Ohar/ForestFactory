import Storage from '@/parts/modules/storage'
import {chests} from '@/constants/game_objects_data'
import Building from '@/sprites/buildings/building'
import getWoodCounterText from '@/utils/get-wood-counter-text'
import uid from 'uid'

function countGatheredWood (game) {
	const chests = game.world
		.getByName('buildings')
		.getByName('chests')
		.children

  // TODO: use plot here
  // game.quest.counter = chests.reduce(
		// (counter, chest) => counter + chest.modules.storage.fullness,
		// 0
  // )
  //
  // game.quest.text = getWoodCounterText(game.quest.counter)
}

export default class Chest extends Building {
	constructor ({game, x, y, key = 'chests', ffId, storageId = uid(20)}) {
		super({game, x, y, key, frame: chests.open.frame, ffId})

		this.animations.add('opening', [chests.halfOpen.frame, chests.open.frame], 2)
		this.animations.add('closing', [chests.halfOpen.frame, chests.closed.frame], 2)
		this.animations.add('halfOpening', [chests.closed.frame, chests.halfOpen.frame], 2)
		this.animations.add('halfClosing', [chests.open.frame, chests.halfOpen.frame], 2)

		this.game.world
			.getByName('buildings')
			.getByName('chests')
			.add(this)

		this.onChangeFullness = this.onChangeFullness.bind(this)

		this.modules = {
      storage: new Storage({
        id: storageId,
        owner: this,
        hasText: true,
        changeFullnessCb: this.onChangeFullness,
      }),
		}
	}

  onChangeFullness (oldFullness) {
    // countGatheredWood(this.game);

    const {volume, fullness} = this.modules.storage

    if (oldFullness === volume && volume > fullness) {
      this.animations.play('halfOpening')
    } else if (oldFullness === 0 && volume > fullness) {
      this.animations.play('halfClosing')
    } else if (fullness === 0) {
      this.animations.play('opening')
    } else if (fullness === volume) {
      this.animations.play('closing')
    }
	}
}
