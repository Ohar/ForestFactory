import store from '@/store'
import { SEEK_LIST_ROBOTS, SEEK_LIST_TREES } from '@/l10n/keys'

const {optionsState: {l10n}} = store.getState()

const SEEK_LIST = [
	// [
	// 	'Сундуки',
	// 	'group chests',
	// ],
	// [
	// 	'Брёвна',
	// 	'group resources',
	// ],
	// [
	// 	'Цветы',
	// 	'layer environment 356 357 358',
	// ],
	[
    l10n[SEEK_LIST_ROBOTS],
		'group robots',
	],
	[
    l10n[SEEK_LIST_TREES],
		'group trees',
	],
]

export default SEEK_LIST
