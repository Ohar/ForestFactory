import FFGame from '@/classes/ff-game'
import actionClearGameData from '@/store/editor/actions/clear-game-data'
import actionMainMenuHide  from '@/store/gui/actions/main-menu-hide'
import actionLoadGameMenuHide from '@/store/gui/actions/load-game-menu-hide'
import actionWinScreenHide from '@/store/gui/actions/winscreen-hide'

export default function signalLoadGame (levelId) {
	return dispatch => {
    if (window.game) {
      dispatch(actionClearGameData())
      window.game.destroy()
    }

    window.game = new FFGame(levelId)

    dispatch(actionMainMenuHide ())
    dispatch(actionLoadGameMenuHide())
    dispatch(actionWinScreenHide())
	}
}
