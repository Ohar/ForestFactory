import { SET_GAMEPLAYUI_VISIBILITY } from '@/store/gui/action-types'

export default function actionGameplayUIHide () {
  return dispatch => {
    dispatch({
      type             : SET_GAMEPLAYUI_VISIBILITY,
      gameplayUIVisible: false,
    })
  }
}
