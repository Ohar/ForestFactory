import esprima from 'esprima'
import convertAstTreeToIR from '@/compiler/converters/convert-ast-tree-to-ir'

export default function compileProgramToIR (program) {
  try {
    const ast = esprima.parse(program.js)
    return convertAstTreeToIR(ast, program).code

  } catch (err) {
    console.error('err', err)
    console.error('Code parsing problem', program.js)
    return []
  }
}
