import { SET_LEVEL_SAVED_DATA } from '@/store/editor/action-types'

export default function actionSetLevelSavedData (levelSavedData) {
  return dispatch => {
    dispatch(
      {
        type: SET_LEVEL_SAVED_DATA,
        levelSavedData,
      },
    )
  }
}
