import createCommandFunctionCall from '@/compiler/create-commands/create-command-function-call'
import createCommandFunctionEnd from '@/compiler/create-commands/create-command-function-end'
import createCommandGoto from '@/compiler/create-commands/create-command-goto'
import createCommandIf from '@/compiler/create-commands/create-command-if'
import createCommandLabel from '@/compiler/create-commands/create-command-label'
import createCommandMemorySave from '@/compiler/create-commands/create-command-memory-save'
import createHighlightBlockCommand from '@/compiler/create-commands/create-highlight-block-command'
import createRobotCommand from '@/compiler/create-commands/create-robot-command'
import getRobotById from '@/utils/getRobotById'

export default function createCommand (name, program, tokens, commandNumber) {
  switch (name) {
    case 'chassis.moveByXY':
    case 'chassis.moveToObj':
    case 'chassis.moveToXY':
    case 'gripper.grab':
    case 'gripper.putInto':
    case 'gripper.release':
    case 'chainsaw.saw':
    case 'vision.seek':
    case 'vision.seekNearest':
      const [moduleName, actionName] = name.split('.')
      const robot                    = getRobotById(program.robotId)
      return createRobotCommand(robot, moduleName, actionName, tokens)

    case 'highlightBlock':
      return createHighlightBlockCommand(program, tokens)

    case 'memory.save':
      return createCommandMemorySave(program, tokens)

    case 'label':
      return createCommandLabel(program, tokens, commandNumber)

    case 'functionCall':
      return createCommandFunctionCall(program, tokens)

    case 'functionEnd':
      return createCommandFunctionEnd(program, tokens)

    case 'goto':
      return createCommandGoto(program, tokens)

    case 'if':
      return createCommandIf(program, tokens)
  }
}
