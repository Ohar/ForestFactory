import sortByXY from '@/utils/sortByXY'

export default function fixLevelObjectsOrder (level) {
  return level && {
    ...level,
    levelMap: {
      ...level.levelMap,
      layers: level.levelMap && level.levelMap.layers && level.levelMap.layers.map(
        layer => layer.type === 'objectgroup'
          ? {
            ...layer,
            objects: layer.objects.sort(sortByXY),
          }
          : layer,
      ),
    },
  }
}
