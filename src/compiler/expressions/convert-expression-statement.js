import convertAstNodeToIR from '@/compiler/converters/convert-ast-node-to-ir'

export default function convertExpressionStatement (node, program, loopLabels) {
	return convertAstNodeToIR(node.expression, program, loopLabels)
}
