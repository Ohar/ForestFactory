import signalSetProgramToRobot from '@/store/editor/signals/set-program-to-robot'
import Program from '@/parts/program'

export default function signalSetNewProgramToRobot (robot) {
  const createdProgram = new Program({
    name      : 'Новая программа',
    showAtList: false,
  })

  return dispatch => {
    dispatch(signalSetProgramToRobot(robot, createdProgram.id))
  }
}
