import generateEditorBtnTitle from '@/utils/generateEditorBtnTitle'
import getProgramById from '@/utils/getProgramById'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  PROGRAM_COPY_BTN_TEMPLATE_COMMON,
  PROGRAM_COPY_BTN_TEXT,
  PROGRAM_COPY_TO_STORAGE_NEW_NAME,
  PROGRAM_NOT_SELECTED_DISABLED,
} from '@/l10n/keys'
import './styles.less'

class ProgramCopyBtn extends Component {
  constructor (...args) {
    super(...args)

    this.onClick = this.onClick.bind(this)
  }

  onClick () {
    const {l10n, selectedProgramId} = this.props
    const program             = getProgramById(selectedProgramId)

    program.createCopy(l10n[PROGRAM_COPY_TO_STORAGE_NEW_NAME](program.name))
  }

  render () {
    const {l10n, selectedProgramId} = this.props
    const program             = getProgramById(selectedProgramId)

    const title = generateEditorBtnTitle({
      program,
      isDisabled      : !program,
      templateDisabled: l10n[PROGRAM_NOT_SELECTED_DISABLED],
      templateCommon  : l10n[PROGRAM_COPY_BTN_TEMPLATE_COMMON],
    })

    return (
      <button
        className='ProgramCopyBtn'
        disabled={!selectedProgramId}
        onClick={this.onClick}
        title={title}
      >
        {l10n[PROGRAM_COPY_BTN_TEXT]}
      </button>
    )
  }
}

function mapStateToProps (state, ownProps) {
  return {
    ...ownProps,
    selectedProgramId: state.editorState.selectedProgramId,
    l10n             : state.optionsState.l10n,
  }
}

export default connect(mapStateToProps)(ProgramCopyBtn)
