import IRNode from '@/classes/ir-node'
import uid from 'uid'

export default function convertBreakStatement (node, program, loopLabels) {
	return new IRNode({
		id  : uid(20),
		code: `goto ${loopLabels.endLoop}`,
	})
}
