import { GRIPPER_MODULE_TYPE } from '@/constants/moduleTypes'
import store from '@/store'
import { BLOCKLY_RELEASE_TEXT } from '@/l10n/keys'

const BlockRelease = {
	name      : 'FF_Release',
	generator : () => `${GRIPPER_MODULE_TYPE}.release()\n`,
	definition: {
		init () {
      const {optionsState: {l10n}} = store.getState()

      this
				.appendDummyInput()
				.appendField(l10n[BLOCKLY_RELEASE_TEXT]);
			this.setInputsInline(true);
			this.setPreviousStatement(true, null);
			this.setNextStatement(true, null);
			this.setColour(250);
		}
	}
}

export default BlockRelease
