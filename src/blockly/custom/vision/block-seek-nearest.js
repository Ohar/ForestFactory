import Blockly from '@/blockly/blockly'
import { VISION_MODULE_TYPE } from '@/constants/moduleTypes'
import SEEK_LIST from '@/constants/seek_list'
import store from '@/store'
import { BLOCKLY_SEEK_NEAREST_TEXT } from '@/l10n/keys'

const BJS = Blockly.JavaScript

const BlockSeekNearest = {
	name      : 'FF_SeekNearest',
	generator : block => {
		const [type, name, ...tiles] = block.getFieldValue('chosen').split(' '),
		      tilesStr               = tiles && tiles.length
			      ? `,${tiles.join(',')}`
			      : '',
		      code                   = `${VISION_MODULE_TYPE}.seekNearest('${name}','${type}'${tilesStr})`;

		return [code, BJS.ORDER_ATOMIC]
	},
	definition: {
		init () {
      const {optionsState: {l10n}} = store.getState()

      this
				.appendDummyInput()
				.appendField(l10n[BLOCKLY_SEEK_NEAREST_TEXT])
				.appendField(
					new Blockly.FieldDropdown(SEEK_LIST),
					'chosen'
				);

			this.setInputsInline(true);
			this.setOutput(true, null);
			this.setColour(300);
		}
	},
}

export default BlockSeekNearest
