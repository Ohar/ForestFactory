const levelMap   = require('./levelMap.json')
const modules    = require('./modules.json')
const options    = require('./options.json')
const plot       = require('./plot.json')
const programs   = require('./programs.json')
const toolbox    = require('./toolbox.xml')
const level_2    = require('./../level-2')
const level_1_id = require('./../../constants/level_1_id')

module.exports = {
  levelMap,
  modules,
  options,
  plot,
  programs,
  toolbox,
  name     : {
    en_GB: 'Level 1',
    ru_RU: 'Уровень 1',
  },
  id       : level_1_id,
  nextLvlId: level_2.id,
}
