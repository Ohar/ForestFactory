import store from '@/store/index'

export default function hideStartLevelScreenListener (startLevel, {startLevelScreenVisible: curVal}) {
  return () => {
    let nextVal = store.getState().guiState.startLevelScreenVisible

    if (curVal !== nextVal) {
      curVal = nextVal

      if (!curVal) {
        startLevel()
      }
    }
  }
}
