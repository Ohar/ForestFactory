import programAddListener from '@/gui/components/ProgramAddBtn/programAddListener'
import signalSetNewProgramToRobot from '@/store/editor/signals/set-new-program-to-robot'
import generateEditorBtnTitle from '@/utils/generateEditorBtnTitle'
import getRobotProgram from '@/utils/getRobotProgram'
import { noop } from 'lodash'
import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  PROGRAM_ADD_BTN_CLEAR,
  PROGRAM_ADD_BTN_NEW,
  PROGRAM_UPDATE_TITLE_TEMPLATE_1,
  PROGRAM_UPDATE_TITLE_TEMPLATE_2,
  ROBOT_NOT_SELECTED_DISABLED,
} from '@/l10n/keys'

class ProgramAddBtn extends Component {
  constructor (...args) {
    super(...args)

    this.state = {
      title: '',
      text : '',
    }

    this.onClick            = this.onClick.bind(this)
    this.updateTextAndTitle = this.updateTextAndTitle.bind(this)
    this.updateText         = this.updateText.bind(this)
    this.updateTitle        = this.updateTitle.bind(this)
  }

  componentWillMount () {
    this.updateTextAndTitle()
  }

  componentWillReceiveProps ({robot, robotsPrograms}) {
    if (
      robotsPrograms !== this.props.robotsPrograms
      || robot !== this.props.robot
    ) {
      this.updateTextAndTitle(robot)
    }
  }

  updateTextAndTitle (robot = this.props.robot) {
    const program = getRobotProgram(robot)

    this.updateText(program)
    this.updateTitle(program, robot)
  }

  updateText (program) {
    const {l10n} = this.props
    const text = program
                 ? l10n[PROGRAM_ADD_BTN_CLEAR]
                 : l10n[PROGRAM_ADD_BTN_NEW]

    this.setState({text})
  }

  updateTitle (program, robot) {
    const {l10n}           = this.props
    const templatePart1    = l10n[PROGRAM_UPDATE_TITLE_TEMPLATE_1]
    const templatePart2    = l10n[PROGRAM_UPDATE_TITLE_TEMPLATE_2]
    const templateDisabled = l10n[ROBOT_NOT_SELECTED_DISABLED]
    const templateCommon   = program
                             ? `${templatePart1}.\n${templatePart2}`
                             : templatePart1

    const title = generateEditorBtnTitle({
      program,
      robot,
      templateCommon,
      templateDisabled,
      isDisabled: !robot,
    })

    this.setState({title})
  }

  onClick () {
    const {robot, setNewProgramToRobot} = this.props

    setNewProgramToRobot(robot)

    if (programAddListener.hasCallback) {
      programAddListener.runCallback()
    }
  }

  render () {
    const {text, title} = this.state
    const {robot}       = this.props

    return (
      <button
        onClick={this.onClick}
        title={title}
        disabled={!robot}
      >
        {text}
      </button>
    )
  }
}

function mapStateToProps (state, ownProps) {
  return {
    ...ownProps,
    robot         : state.editorState.robot,
    robotsPrograms: state.editorState.robotsPrograms,
    l10n          : state.optionsState.l10n,
  }
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators({
    setNewProgramToRobot: signalSetNewProgramToRobot,
  }, dispatch)
}

ProgramAddBtn.propTypes = {
  robot         : PropTypes.object,
  robotsPrograms: PropTypes.array.isRequired,
}

ProgramAddBtn.defaultProps = {
  robot         : {},
  robotsPrograms: [],
}

export default connect(mapStateToProps, mapDispatchToProps)(ProgramAddBtn)
