import {isNumber} from 'lodash'
import Pob from '@/sprites/pobs/pob'
import getResourceVolumeByFrame from '@/utils/game-resource-volume-by-frame'

export default class Resource extends Pob {
	constructor ({game, x, y, key = 'farming_fishing', frame, options = {}, ffId}) {
		super({game, x, y, key, frame}, ffId)

		this.game.world
			.getByName('pobs')
			.getByName('resources')
			.add(this)

    this.isSeparable = true
    this.options     = options
		this.volume      = isNumber(this.options.volume)
			? this.options.volume
			: getResourceVolumeByFrame(frame)
	}

	separate (volumeToSeparate) {
		if (this.volume > volumeToSeparate) {
			this.volume -= volumeToSeparate

			return new Resource({
        game: this.game,
				x: this.x,
				y: this.y,
				key: this.key,
				frame: this.frame,
				options: {
	        ...this.options,
	        volume: volumeToSeparate,
	      }
			})
		} else {
			return null
		}
	}
}
