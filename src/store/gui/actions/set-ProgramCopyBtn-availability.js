import { SET_PROGRAM_COPY_BTN_AVAILABILITY } from '@/store/gui/action-types'

export default function actionSetProgramCopyBtnAvailability (isProgramCopyBtnEnabled) {
  return dispatch => {
    dispatch({
      type: SET_PROGRAM_COPY_BTN_AVAILABILITY,
      isProgramCopyBtnEnabled,
    })
  }
}
