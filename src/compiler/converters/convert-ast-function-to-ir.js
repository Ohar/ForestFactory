import IRNode from '@/classes/ir-node'
import convertFunctionDeclaration from '@/compiler/expressions/convert-function-declaration'

// Возможно, нужно рекурсивно перебирать всё дерево вглубь на случай вложенных функций
export default function convertAstFunctionToIR (node, program) {
	switch (node.type) {
		case 'Program': {
			return new IRNode({
				id  : null,
				code: node.body
				  .filter(childNode => childNode.type === 'FunctionDeclaration')
				  .map(childNode => convertFunctionDeclaration(childNode, program).code)
				  .join('\n')
			})
		}

		default:
			return new IRNode()
	}
}
