import FFPoint from '@/classes/ff-point'
import { CHASSIS_MODULE_TYPE } from '@/constants/moduleTypes'
import Module from '@/parts/modules/module'

const SPEED        = 300
const MIN_DISTANCE = 5

export default class Chassis extends Module {
  constructor ({id, owner, ownerId}) {
    super({id, moduleType: CHASSIS_MODULE_TYPE, owner, ownerId})
  }

  moveToXY (x, y) {
    const distance = this.owner.game.physics.arcade.distanceToXY(this.owner, x, y)
    const finished = distance <= MIN_DISTANCE

    if (finished) {
      this.owner.body.x = x
      this.owner.body.y = y
      this.stop()
    } else {
      this.owner.game.physics.arcade.moveToXY(this.owner, x, y, SPEED)
    }

    return {
      finished,
      result       : true,
      resultForSave: true,
    }
  }

  moveToObj (ffId) {
    const result = Boolean(ffId)
    let finished = false

    if (ffId) {
      const {modules: {memory}, game: {ffVarList}} = this.owner
      const objFfId                                = memory.load(ffId)
      const {x, y}                                 = new FFPoint(ffVarList.get(objFfId))

      finished = this.moveToXY(x, y).finished
    } else {
      finished = true
    }

    return {
      finished,
      result,
      resultForSave: result,
    }
  }

  moveByXY (x, y) {
    const {memory} = this.owner

    if (!memory.targetToMove) {
      memory.targetToMove = new FFPoint({
        x: Number(x) + body.x,
        y: Number(y) + body.y,
      })
    }

    const {targetToMove: {x: targetX, y: targetY}} = memory
    const {finished}                               = this.moveToXY(targetX, targetY)

    if (finished) {
      delete memory.targetToMove
    }

    return {
      finished,
      result       : true,
      resultForSave: true,
    }
  }

  stop () {
    this.owner.body.velocity.set(0, 0)
    this.owner.body.acceleration.set(0, 0)

    return {
      finished     : true,
      result       : true,
      resultForSave: true,
    }
  }
}
