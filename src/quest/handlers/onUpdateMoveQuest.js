import signalIncrementQuestCounter from '@/store/plot/actions/quest-counter-increment'
import store from '@/store'
import checkQuestsDone from '@/quest/checkQuestsDone'

export default function onUpdateMoveQuest (zone, self, questId) {
  function questUpdater () {
    if (zone.intersects(self) && !self.checkInQuestDoneList(questId)) {
      store.dispatch(signalIncrementQuestCounter(questId))
      self.removeFromQuestHandlerList(questUpdater)
      self.addToQuestDoneList(questId)
      checkQuestsDone(self.game)
    }
  }

  return questUpdater
}
