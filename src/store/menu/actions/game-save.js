import {SAVE_GAME_REQUEST} from '@/store/menu/action-types'

export default function actionSaveGame () {
	return dispatch => {
		dispatch({type: SAVE_GAME_REQUEST})
	}
}
