import { INCREMENT_QUEST_COUNTER } from '@/store/plot/action-types'

export default function signalIncrementQuestCounter (questId) {
  return dispatch => {
    dispatch({
      type: INCREMENT_QUEST_COUNTER,
      questId,
    })
  }
}
