// IndexedDB: добавить одну запись

import dbConnectionPromise from '@/singletons/db/db-connection-promise'

export default function addSingleItem (item, storeName) {
	return dbConnectionPromise
		.then(
			dbConnection => {
				const store = dbConnection.transaction(storeName, 'readwrite').objectStore(storeName)

				return new Promise(
					(resolve, reject) => {
						const request = store.add(item)

						request.onsuccess = () => {
							resolve(true)
						}

						request.onerror = event => {
							reject(event.target.errorCode)
						}
					}
				)
			}
		)
}
