import { sortBy } from 'lodash'

export default function normalizeLevelObj (level) {
  return {
    ...level,
    levelMap: {
      ...level.levelMap,
      layers: sortBy(level.levelMap.layers, 'name'),
    }
  }
}
