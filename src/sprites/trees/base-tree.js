import FFSprite from '@/classes/ff-sprite'
import Phaser from 'phaser'
import Resource from '@/sprites/pobs/resource';
import getWoodType from '@/utils/get-wood-type'

// Подобраны экспериментально
const TREE_OFFSET    = 64,
      TREETOP_OFFSET = {min: 50, free: 25},
      ANCHOR_OFFSET  = {x: .33, y: .5}

export default class BaseTree extends FFSprite {
	constructor ({game, x, y, key, frame, ffId}, treetopFrame) {
		super({game, x, y: y + TREE_OFFSET, key, frame}, ffId)

		this.game = game
		this.wood = 10

		this.setHealth(100)
		this.game.world.getByName('trees').add(this)

		const treeTopMargin = TREETOP_OFFSET.min + Math.random() * TREETOP_OFFSET.free,
		      treeTop       = new Phaser.Image(game, 0, -treeTopMargin, 'treetop', treetopFrame)

		this.anchor.set(ANCHOR_OFFSET.x, ANCHOR_OFFSET.y)
		treeTop.anchor.set(ANCHOR_OFFSET.x, ANCHOR_OFFSET.y)

		this.addChild(treeTop)
		this.events.onKilled.add(this.onKilled, this);
	}

	onKilled () {
		const {frame} = getWoodType(this.wood)

		new Resource({game: this.game, x: this.x, y: this.y, key: undefined, frame})

		this.destroy()
	}
}
