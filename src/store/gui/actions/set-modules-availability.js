import { SET_MODULES_AVAILABILITY } from '@/store/gui/action-types'

export default function actionSetModulesAvailability (isModulesEnabled) {
  return dispatch => {
    dispatch({
      type: SET_MODULES_AVAILABILITY,
      isModulesEnabled,
    })
  }
}
