# ForestFactory

## Install

```bash
yarn
```

## Start

### At browser

```bash
yarn run dev:web
```

### Electron

```bash
yarn run dev:electron
```

## Deploy config example

`config.deploy.json`

```json
{
  "user": "user",
  "server": "122.456.789.012",
  "path": "/var/www/html/",
  "releasePageFileName": "index.html"
}

```

## Level config example

`config.level.json`

```json
{
  "startDevLevel": "level-1",
  "showLevelData": false
}

```
