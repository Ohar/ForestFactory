import {wood} from '@/constants/game_objects_data';

export default function getWoodType (woodAmount) {
	switch (woodAmount) {
		case wood.big.volume:
			return wood.big

		case wood.medium.volume:
			return wood.medium

		case wood.small.volume:
		default:
			return wood.small
	}
}
