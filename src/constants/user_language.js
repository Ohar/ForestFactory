import getBrowserLanguage from 'get-browser-language'
import { defaultLocale } from '@/l10n/locales'

const USER_LANGUAGE = getBrowserLanguage() || defaultLocale

export default USER_LANGUAGE
