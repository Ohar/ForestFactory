export default function getSpriteClassByTileIndex (index) {
	switch (index) {
		case 1429:
			return {
				key  : 'chests',
				frame: 1,
			}

		case 356:
		case 357:
		case 358:
			return {
				key  : 'terrain',
				frame: 356,
			}

		default:
			return {
				key  : 'NO_SPRITE',
				frame: 0,
			}
	}
}
