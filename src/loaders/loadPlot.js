import { SET_PLOT } from '@/store/plot/action-types'
import store from '@/store'

export default function loadPlot (plot) {
  return new Promise(
    resolve => {
      store.dispatch({
        type: SET_PLOT,
        plot,
      })

      resolve()
    },
  )
}
