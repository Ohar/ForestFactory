import {wood} from '@/constants/game_objects_data'

function getResourceVolumeByFrame (frame) {
	switch (frame) {
		case wood.big.frame:
			return wood.big.volume;

		case wood.medium.frame:
			return wood.medium.volume;

		case wood.small.frame:
		default:
			return wood.small.volume;
	}
}

export default getResourceVolumeByFrame
