import React, {PropTypes} from 'react'
import PROGRAM_CONTROLS_LIST from '@/gui/components/ProgramControls/consts/program_controls_list'
import ProgramControlBtn from '@/gui/components/ProgramControlBtn/index'
import './styles.less'

function ProgramControls ({short, robot}) {
  return (
    <section className='ProgramControls'>
      {
        PROGRAM_CONTROLS_LIST.map(
          ({commandName, icon, text}, i) => (
            <ProgramControlBtn
              commandName={commandName}
              robot={robot}
              key={i}
            >
              {
                short
                ? icon
                : `${icon} ${text}`
              }
            </ProgramControlBtn>
          ),
        )
      }
    </section>
  )
}

ProgramControls.propTypes = {
  short: PropTypes.bool,
  robot: PropTypes.object,
}

ProgramControls.defaultProps = {
  short: false,
  robot: {},
}

export default ProgramControls
