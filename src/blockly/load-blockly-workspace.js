import addBlocklyBlock from '@/blockly/add-blockly-block'
import Blockly from '@/blockly/blockly'

export default function loadBlocklyWorkspace (toolbox) {
  // TODO: Remove side effect; extend Blockly with it
  addBlocklyBlock()

  return new Promise(resolve => {
    return resolve(Blockly.inject('BLOCKLY_WORKSPACE', {toolbox}))
  })
}
