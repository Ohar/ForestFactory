export default function sortByModuleType (a, b) {
  return a.moduleType > b.moduleType ? 1 : -1
}
