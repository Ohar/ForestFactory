import classNames from 'classnames'
import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import actionLoadGameMenuShow from '@/store/gui/actions/load-game-menu-show'
import actionMainMenuShow from '@/store/gui/actions/main-menu-show'
import actionOptionsShow from '@/store/gui/actions/options-show'
import actionSaveGame from '@/store/menu/actions/game-save'
import GameMenuToggler from '@/gui/components/GameMenuToggler'
import Spinner from '@/gui/components/Spinner'
import './styles.less'
import { GAME_MENU_BTN_LOAD, GAME_MENU_BTN_MAIN_MENU, GAME_MENU_BTN_SAVE, MAIN_MENU_OPTIONS_TEXT } from '@/l10n/keys'

function GameMenu (
  {
    l10n,
    save,
    showLoadGameMenu,
    isGameSaving,
    isGameLoading,
    isMenuOpen,
    showMainMenu,
    showOptions,
    isMainMenuVisible,
  }
  ) {
  const items = [
    {
      text    : l10n[GAME_MENU_BTN_SAVE],
      onClick : save,
      disabled: isGameSaving,
      showSpinner: isGameSaving,
    },
    {
      text    : l10n[GAME_MENU_BTN_LOAD],
      onClick : showLoadGameMenu,
      disabled: isGameLoading,
      showSpinner: isGameLoading,
    },
    {
      text    : l10n[GAME_MENU_BTN_MAIN_MENU],
      onClick : showMainMenu,
      disabled: isMainMenuVisible,
      showSpinner: isMainMenuVisible,
    },
    {
      text    : l10n[MAIN_MENU_OPTIONS_TEXT],
      onClick : showOptions,
    },
  ]

  return (
    <section className='GameMenu'>
      <section className={classNames('GameMenu_list', {
        'GameMenu_list-visible': isMenuOpen,
      })}>
        {
          items.map(
            ({text, onClick, disabled = false, showSpinner = false}, i) => (
              <button
                className='GameMenu_button'
                disabled={disabled}
                onClick={onClick}
                key={i}
              >
                <Spinner visible={showSpinner}/>
                {text}
              </button>
            ),
          )
        }
      </section>
      <GameMenuToggler/>
    </section>
  )
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators(
    {
      save            : actionSaveGame,
      showLoadGameMenu: actionLoadGameMenuShow,
      showMainMenu    : actionMainMenuShow,
      showOptions     : actionOptionsShow,
    },
    dispatch,
  )
}

function mapStateToProps (state, ownProps) {
  return {
  ...ownProps,
    isGameSaving     : state.menuState.isGameSaving,
    isGameLoading    : state.menuState.isGameLoading,
    isMenuOpen       : state.menuState.isMenuOpen,
    isMainMenuVisible: state.guiState.mainMenuVisible,
    l10n             : state.optionsState.l10n,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(GameMenu)
