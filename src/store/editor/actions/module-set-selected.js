import {SET_SELECTED_MODULE} from '@/store/editor/action-types'

export default function actionModuleSetSelected (selectedModuleId) {
    return dispatch => {
	    dispatch({
		    type: SET_SELECTED_MODULE,
        selectedModuleId,
	    })
    }
}
