import './styles.less'

import React from 'react'
import {connect} from 'react-redux'
import RobotBtn from '@/gui/components/RobotBtn'
import RobotProgramName from '@/gui/components/RobotProgramName'
import ProgramControls from '@/gui/components/ProgramControls';

const RobotPanel = ({robotList}) => {
	return robotList.length
		? (
			<ul className='RobotPanel'>
				{
					robotList.map(
						(robot, i) => (
							<li
								className='RobotPanel_item'
								key={i}
							>
								<RobotBtn robot={robot}/>
								<RobotProgramName robot={robot}/>
								<ProgramControls
									robot={robot}
									short
								/>
							</li>
						)
					)
				}
			</ul>
		)
		: null
}


function mapStateToProps (state, ownProps) {
	return {
		...ownProps,
		robotList: state.editorState.robotList,
	}
}

export default connect(mapStateToProps)(RobotPanel)
