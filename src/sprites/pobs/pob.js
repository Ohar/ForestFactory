import FFSprite from '@/classes/ff-sprite'

// Portable OBject
export default class Pob extends FFSprite {
	constructor ({game, x, y, key, frame}, ffId) {
		super({game, x, y, key, frame}, ffId)
		
		this.game = game
		
		const pobs = this.game.world.getByName('pobs')
		pobs.add(this)
	}
}
