export const BATTERY_MODULE_TYPE = 'battery'
export const CHAINSAW_MODULE_TYPE = 'chainsaw'
export const CHASSIS_MODULE_TYPE = 'chassis'
export const GRIPPER_MODULE_TYPE = 'gripper'
export const MEMORY_MODULE_TYPE = 'memory'
export const DEFAULT_MODULE_TYPE = 'default_module'
export const STORAGE_MODULE_TYPE = 'storage'
export const TOOL_MODULE_TYPE = 'tool'
export const VISION_MODULE_TYPE = 'vision'

const moduleTypes = [
  BATTERY_MODULE_TYPE,
  CHAINSAW_MODULE_TYPE,
  CHASSIS_MODULE_TYPE,
  GRIPPER_MODULE_TYPE,
  MEMORY_MODULE_TYPE,
  DEFAULT_MODULE_TYPE,
  STORAGE_MODULE_TYPE,
  TOOL_MODULE_TYPE,
  VISION_MODULE_TYPE,
]

const moduleGameTypes = [
  BATTERY_MODULE_TYPE,
  CHAINSAW_MODULE_TYPE,
  CHASSIS_MODULE_TYPE,
  GRIPPER_MODULE_TYPE,
  MEMORY_MODULE_TYPE,
  STORAGE_MODULE_TYPE,
  VISION_MODULE_TYPE,
]

export default moduleTypes
export {moduleGameTypes}
