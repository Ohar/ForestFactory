import './styles.less'
import nameSizeSetter from '@/gui/components/RobotBtn/nameSizeSetter'
import front from 'root/resources/images/robot/original/front'

import React from 'react'
import classNames from 'classnames'
import { connect } from 'react-redux'

function RobotBtn ({robot, editorRobot}) {
  const nameSize = nameSizeSetter(robot.name)

  return (
    <button
      onClick={() => robot.getPlayerFocus()}
      className={classNames(
        'RobotBtn',
        `RobotBtn-nameSize-${nameSize}`,
        {
          'RobotBtn-chosen': editorRobot === robot,
        }
      )}
      title={robot.name}
    >
      <img
        src={front}
        className='RobotBtn-img'
        alt={robot.name}
      />
    </button>
  )
}

function mapStateToProps (state, ownProps) {
  return {
    ...ownProps,
    editorRobot: state.editorState.robot,
  }
}

export default connect(mapStateToProps)(RobotBtn)
