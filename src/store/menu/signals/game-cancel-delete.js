import { sortBy } from 'lodash'
import { DELETE_GAME_FAILURE } from '@/store/menu/action-types'
import store from '@/store/index'

export default function signalCancelDeleteGame () {
  return dispatch => {
    const {deletingTimeout} = store.getState().menuState

    dispatch({type: DELETE_GAME_FAILURE})
    clearTimeout(deletingTimeout)
  }
}
