// IndexedDB: обновить запись

import dbConnectionPromise from '@/singletons/db/db-connection-promise'

export default function putItem (storeName) {
	return item => {
    return dbConnectionPromise
      .then(
        dbConnection => {
          const store = dbConnection.transaction(storeName, 'readwrite').objectStore(storeName)

          return new Promise(
            (resolve, reject) => {
              const request = store.put(item)

              request.onsuccess = () => {
                resolve(true)
              }

              request.onerror = event => {
                reject(event.target.errorCode)
              }
            }
          )
        }
      )
      .catch(
        err => {
          console.error('Fail putItem', err);
        }
      )
	}
}
