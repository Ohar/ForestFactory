import { SET_REPOSITORY_AVAILABILITY } from '@/store/gui/action-types'

export default function actionSetRepositoryAvailability (isRepositoryEnabled) {
  return dispatch => {
    dispatch({
      type: SET_REPOSITORY_AVAILABILITY,
      isRepositoryEnabled,
    })
  }
}
