#!/usr/bin/env bash

# Generate release page

generateReleasePage () {
    # Save script directory path
    # https://stackoverflow.com/a/24112741/2059884
    local -r PARENT_PATH=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
    cd "$PARENT_PATH"

    local -r DEPLOY_FOLDER=`node -p 'require("./../config.deploy.json")["path"]'`
    local -r SERVER=`node -p 'require("./../config.deploy.json")["server"]'`
    local -r USERNAME=`node -p 'require("./../config.deploy.json")["user"]'`
    local -r RELEASE_PAGE_FILE_NAME=`node -p 'require("./../config.deploy.json")["releasePageFileName"]'`

    local -r BUILD_FOLDER=`node -p 'require("./../config.build.json")["buildFolder"]'`
    IFS=',' read -a PLATFORM_LIST <<< `node -p 'require("./../config.build.json")["platform"].join(",")'`

    local -r URL=$USERNAME@$SERVER

    node -p 'require("./generate-release-page-html.js")()'

    scp ./$RELEASE_PAGE_FILE_NAME $URL:$DEPLOY_FOLDER
    scp ./robots.txt $URL:$DEPLOY_FOLDER
    rm -rf ./$RELEASE_PAGE_FILE_NAME

    echo "Release page is generated"
}

generateReleasePage
