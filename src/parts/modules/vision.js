import FFPoint from '@/classes/ff-point'
import { VISION_MODULE_TYPE } from '@/constants/moduleTypes'
import Fov from '@/parts/modules/fov'
import Module from '@/parts/modules/module'
import getGroupWithNestedChildGroups from '@/utils/get-group-with-nested-child-groups'
import inCentredSquare from '@/utils/in-centred-square'
import inCircle from '@/utils/in-circle'
import isTileNotEmpty from '@/utils/is-tile-not-empty'
import map from 'lodash/map'

const VISION_RANGE = 500

export default class Vision extends Module {
  constructor ({id, owner, ownerId, range = VISION_RANGE}) {
    super({id, moduleType: VISION_MODULE_TYPE, owner, ownerId})

    this.range = range
  }

  showFov () {
    if (!this.fov && this.owner) {
      this.fov = new Fov(this.owner, this.range)
    }
    if (this.fov) {
      this.fov.show()
    }
  }

  hideFov () {
    if (this.fov) {
      this.fov.hide()
    }
  }

  seek (name, type, ...tiles) {
    switch (type) {
      case 'group':
        return this.seekInGroup(name)

      default:
        return this.seekInLayer(name, tiles)
    }
  }

  seekInGroup (groupName) {
    const group = getGroupWithNestedChildGroups(this.owner.game.world, groupName)

    const result = group
                   ? group
                     .filter(this.checkInFOV.bind(this))
                     .list
                     .filter(obj => obj !== this.owner)
                   : []

    return {
      finished     : true,
      result,
      resultForSave: map(result, 'ffId'),
    }
  }

  seekInLayer (layerName, tiles) {
    const layer     = this.owner.game.world.getByName(layerName),
          ffVarList = this.owner.game.ffVarList

    const result = layer
      ? layer
        .getTiles(
          this.owner.body.position.x - this.range,
          this.owner.body.position.y - this.range,
          this.range * 2,
          this.range * 2,
        )
        .filter(isTileNotEmpty)
        .filter(
          tile => tiles && tiles.length
            ? tiles.includes(String(tile.index))
            : true,
        )
        .filter(this.checkInFOV.bind(this))
        .map(tile => ffVarList.add(tile))
      : []

    return {
      finished     : true,
      result,
      resultForSave: map(result, 'ffId'),
    }
  }

  seekNearest (what, type, ...tiles) {
    const arr = this.seek(what, type, ...tiles).result

    const result = arr.length
      ? arr.reduce(
        (prev, curr) => this.getDistance(prev) <= this.getDistance(curr)
          ? prev
          : curr,
      )
      : null

    return {
      finished     : true,
      result,
      resultForSave: result
        ? result.ffId
        : null,
    }
  }

  // Проверка попадания в квадрат намного быстрее (теоретически) и позволяет отсечь большую часть неподходящих точек
  // Проверка попадания в круг точная, но сильно медленнее (теоретически), поэтому идёт второй
  // TODO: Проверить на примерах, надо оно вообще или нет
  checkInFOV (obj) {
    const point = new FFPoint(obj)

    return inCentredSquare(this.owner.body.position, this.range, point)
      && inCircle(this.owner.body.position, this.range, point)
  }

  getDistance (obj) {
    return obj
      ? this.owner.body.position.distance(new FFPoint(obj), true)
      : Infinity
  }
}
