import { SET_EDITOR_AVAILABILITY } from '@/store/gui/action-types'

export default function actionSetEditorAvailability (isEditorEnabled) {
  return dispatch => {
    dispatch({
      type: SET_EDITOR_AVAILABILITY,
      isEditorEnabled,
    })
  }
}
