import guiDefaultState from '@/store/gui/default_state'

const levelDefaultOptions = {
  isRepositoryEnabled      : guiDefaultState.isRepositoryEnabled,
  isEditorEnabled          : guiDefaultState.isEditorEnabled,
  isProgramCopyBtnEnabled  : guiDefaultState.isProgramCopyBtnEnabled,
  isProgramDeleteBtnEnabled: guiDefaultState.isProgramDeleteBtnEnabled,
  isProgramRenameBtnEnabled: guiDefaultState.isProgramRenameBtnEnabled,
}

export default levelDefaultOptions
