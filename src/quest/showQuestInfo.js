import programLoadWatcher from '@/gui/components/ProgramLoadBtn/programLoadWatcher'
import onWorkspaceAddBlockWatcher from '@/store/editor/listeners/onWorkspaceAddBlockWatcher'
import openTabListener from '@/store/tab/listeners/openTabListener'
import signalIncrementQuestCounter from '@/store/plot/actions/quest-counter-increment'
import store from '@/store'
import programAddListener from '@/gui/components/ProgramAddBtn/programAddListener'
import checkQuestsDone from '@/quest/checkQuestsDone'
import onUpdateMoveQuest from '@/quest/handlers/onUpdateMoveQuest'
import onUpdateChooseObjectQuest from '@/quest/handlers/onUpdateChooseObjectQuest'
import workspaceSingletone from '@/singletons/workspace'

export default function showQuestInfo (game) {
  const {questList, questIndex} = store.getState().plotState
  const quest = questList[questIndex]

  if (quest) {
    switch (quest.type) {
      case 'choose-object': {
        const {objectGroupName, objectId, id: questId} = quest
        const group = game.world.children.find(e => e.name === objectGroupName)

        if (group && group.children) {
          group.children.forEach(
            agent => {
              if ((!objectId || (objectId && agent.ffId === objectId)) && !agent.checkInQuestDoneList(questId)) {
                agent.addToQuestHandlerList(onUpdateChooseObjectQuest(agent, questId))
              }
            },
          )
        }
        break
      }

      case 'move': {
        const {placeName, objectGroupName, id: questId} = quest

        if (placeName) {
          const {places} = game.world

          if (places && Array.isArray(places) && places.length) {
            const group = game.world.children.find(e => e.name === objectGroupName)

            if (group && group.children) {
              const zone = places.find(e => e.name === placeName)

              group.children.forEach(
                agent => {
                  if (!agent.checkInQuestDoneList(questId)) {
                    agent.addToQuestHandlerList(onUpdateMoveQuest(zone, agent, questId))
                  }
                },
              )
            }
          }
        }
        break
      }

      case 'open-tab': {
        const {id: questId, tabId} = quest
        const isAlreadyDone = store.getState().tabState.activeTabId === tabId

        let unSubScriber = null

        const callback = () => {
          store.dispatch(signalIncrementQuestCounter(questId))
          checkQuestsDone(game)

          if (unSubScriber) {
            unSubScriber()
            unSubScriber = null
          }
        }

        if (isAlreadyDone) {
          callback()
        } else {
          // Subscribe to open tab
          unSubScriber = store.subscribe(
            openTabListener(callback, store.getState().tabState, tabId),
          )
        }

        break
      }

      case 'create-empty-program': {
        const {id: questId} = quest
        const {robot} = store.getState().editorState
        const isAlreadyDone = Boolean(robot && robot.program)

        const callback = () => {
          store.dispatch(signalIncrementQuestCounter(questId))
          checkQuestsDone(game)
        }

        if (isAlreadyDone) {
          callback()
        } else {
          programAddListener.addCallback(callback)
        }

        break
      }

      case 'add-program-block': {
        const {id: questId, block} = quest

        onWorkspaceAddBlockWatcher.addListener(
          {
            block,
            callback () {
              store.dispatch(signalIncrementQuestCounter(questId))
              checkQuestsDone(game)
            }
          }
        )

        // Maybe already done?
        onWorkspaceAddBlockWatcher.checker(workspaceSingletone.workspace.getAllBlocks())

        break
      }

      case 'set-program-to-robot': {
        const {id: questId, programId, robotId} = quest
        const {robot} = store.getState().editorState
        const isAlreadyDone = (
             robot
          && robot.program
          && robot.ffId === robotId
          && (
            programId === robot.program.id ||
            programId === robot.program.originId
             )
        )

        const callback = () => {
          store.dispatch(signalIncrementQuestCounter(questId))
          checkQuestsDone(game)
        }

        if (isAlreadyDone) {
          callback()
        } else {
          programLoadWatcher.addListener({
            programId,
            robotId,
            callback,
          })
        }

        break
      }
    }
  }
}
