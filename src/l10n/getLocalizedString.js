import store from '@/store/index'
import { UNKNOWN_KEY } from '@/l10n/keys'
import { defaultLocale } from '@/l10n/locales'

export default function getLocalizedString (data) {
  const {optionsState: {l10n, locale}} = store.getState()

  return data
         ? data[locale] || data[defaultLocale] || l10n[UNKNOWN_KEY]
         : l10n[UNKNOWN_KEY]
}
