import PROGRAM_NAME from '@/constants/PROGRAM_NAME'
import ROBOT_NAME from '@/constants/ROBOT_NAME'
import store from '@/store'
import { EMPTY_PROGRAM, EMPTY_ROBOT } from '@/l10n/keys'

export default function generateEditorBtnTitle ({isDisabled, program, robot, templateDisabled, templateCommon}) {
  const {optionsState: {l10n}} = store.getState()

  const robotName   = robot
                      ? robot.name
                      : l10n[EMPTY_ROBOT]

  const programName = program
                      ? program.name
                      : l10n[EMPTY_PROGRAM]

  const titleDisabled = templateDisabled
                        ? templateDisabled
                          .replace(ROBOT_NAME, robotName)
                          .replace(PROGRAM_NAME, programName)
                          .concat('\n\n')
                        : ''

  const titleCommon = templateCommon
    .replace(ROBOT_NAME, robotName)
    .replace(PROGRAM_NAME, programName)

  return isDisabled
         ? `${titleDisabled}${titleCommon}`
         : titleCommon
}
