import Blockly from '@/blockly/blockly'
import { CHASSIS_MODULE_TYPE } from '@/constants/moduleTypes'
import store from '@/store'
import { BLOCKLY_TO_OBJ_TEXT } from '@/l10n/keys'

const BJS = Blockly.JavaScript

const BlockMoveToObj = {
	name      : 'FF_MoveToObj',
	generator : block => {
		const obj = BJS.valueToCode(block, 'obj', BJS.ORDER_ATOMIC)

		return `${CHASSIS_MODULE_TYPE}.moveToObj(${obj})\n`
	},
	definition: {
		init () {
      const {optionsState: {l10n}} = store.getState()

      this.appendValueInput('obj')
				.setCheck('Sprite')
				.appendField(l10n[BLOCKLY_TO_OBJ_TEXT]);
			this.setInputsInline(true);
			this.setPreviousStatement(true, null);
			this.setNextStatement(true, null);
			this.setColour(0);
		}
	}
}

export default BlockMoveToObj
