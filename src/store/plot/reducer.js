import * as actionTypes from '@/store/plot/action-types'
import DEFAULT_STATE from '@/store/plot/default_state'

export default function plotReducer (state = DEFAULT_STATE, action) {
  switch (action.type) {
    case actionTypes.SET_PLOT: {
      const {questIndex, questList} = action.plot

      return {
        ...state,
        questIndex,
        questList,
      }
    }

    case actionTypes.SET_QUEST_BY_INDEX: {
      const {questIndex} = action

      return {
        ...state,
        questIndex,
      }
    }

    case actionTypes.INCREMENT_QUEST_COUNTER: {
      return {
        ...state,
        questList: state.questList.map(
          quest => quest.id === action.questId
                   ? {
                     ...quest,
                     counter: quest.counter + 1,
                   }
                   : quest,
        ),
      }
    }

    default:
      return state
  }
}
