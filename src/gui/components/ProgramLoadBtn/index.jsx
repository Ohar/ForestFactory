import programLoadWatcher from '@/gui/components/ProgramLoadBtn/programLoadWatcher'
import signalSetProgramToRobot from '@/store/editor/signals/set-program-to-robot'
import generateEditorBtnTitle from '@/utils/generateEditorBtnTitle'
import getProgramById from '@/utils/getProgramById'
import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  PROGRAM_LOAD_BTN_TEMPLATE_DISABLED,
  PROGRAM_LOAD_BTN_TEXT,
  PROGRAM_LOAD_BTN_TITLE,
  PROGRAM_NOT_SELECTED_DISABLED,
  ROBOT_NOT_SELECTED_DISABLED,
} from '@/l10n/keys'
import './styles.less'

class ProgramLoadBtn extends Component {
  constructor (...args) {
    super(...args)

    this.onClick = this.onClick.bind(this)
  }

  get isDisabled () {
    const {robot, selectedProgramId} = this.props

    return !selectedProgramId || !robot
  }

  onClick () {
    const {robot, selectedProgramId, setProgramToRobot} = this.props

    setProgramToRobot(robot, selectedProgramId)

    if (programLoadWatcher.hasListeners) {
      programLoadWatcher.checker(selectedProgramId, robot.ffId)
    }
  }

  composeTitle () {
    const {l10n, robot, selectedProgramId} = this.props
    const program                    = getProgramById(selectedProgramId)

    let templateDisabled = ''

    switch (true) {
      case !selectedProgramId && !robot:
        templateDisabled = l10n[PROGRAM_LOAD_BTN_TEMPLATE_DISABLED]
        break

      case !selectedProgramId:
        templateDisabled = l10n[PROGRAM_NOT_SELECTED_DISABLED]
        break

      case !robot:
        templateDisabled = l10n[ROBOT_NOT_SELECTED_DISABLED]
        break
    }

    return generateEditorBtnTitle({
      program,
      robot,
      templateDisabled,
      isDisabled    : this.isDisabled,
      templateCommon: l10n[PROGRAM_LOAD_BTN_TITLE],
    })
  }

  render () {
    const {l10n} = this.props
    return (
      <button
        className='ProgramLoadBtn'
        onClick={this.onClick}
        disabled={this.isDisabled}
        title={this.composeTitle()}
      >
        {l10n[PROGRAM_LOAD_BTN_TEXT]}
      </button>
    )
  }
}

ProgramLoadBtn.propTypes = {
  robot            : PropTypes.object,
  selectedProgramId: PropTypes.string,
  setProgramToRobot: PropTypes.func,
}

function mapStateToProps (state, ownProps) {
  return {
    ...ownProps,
    robot            : state.editorState.robot,
    selectedProgramId: state.editorState.selectedProgramId,
    l10n             : state.optionsState.l10n,
  }
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators({setProgramToRobot: signalSetProgramToRobot}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ProgramLoadBtn)
