// IndexedDB: получить одну запись

import dbConnectionPromise from '@/singletons/db/db-connection-promise'

export default function getItem (storeName) {
	return id => dbConnectionPromise
    .then(
      dbConnection => {
        const store = dbConnection.transaction(storeName).objectStore(storeName)

        return new Promise(
          resolve => {
            const request = store.get(id)

            request.onsuccess = event => {
              resolve(event.target.result)
            }

            request.onerror = event => {
              console.error('Fail getItem', event.target.errorCode)
              resolve(null)
            }
          }
        )
      }
    )
}
