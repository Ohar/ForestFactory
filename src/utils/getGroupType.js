export default function getGroupType ({name}) {
  let type = ''

  switch (name) {
    case 'chests':
      type = 'chest'
      break

    case 'trees':
      type = 'tree'
      break

    default:
      type = ''
      break
  }

  return type
}
