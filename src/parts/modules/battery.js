// Аккумулятор

import { BATTERY_MODULE_TYPE } from '@/constants/moduleTypes'
import Module from '@/parts/modules/module'

const BATTERY_CAPACITY = 1000000

export default class Battery extends Module {
  constructor ({capacity = BATTERY_CAPACITY, id, owner, ownerId}) {
    super({id, moduleType: BATTERY_MODULE_TYPE, owner, ownerId})

    this.capacity = capacity
    this.charge   = capacity
  }
}
