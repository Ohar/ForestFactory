import { isUndefined } from 'lodash'
import uid from 'uid'

// TODO: use Symbol
const listenerList   = '_listenerList'
const listenerLength = '_listenerLength'

const programLoadWatcher = {
  get hasListeners () {
    return Boolean(this[listenerLength])
  },
  [listenerLength]: 0,
  [listenerList]  : {},
  addListener (data) {
    const id = uid()

    this[listenerList][id] = {
      ...data,
      id,
    }

    this[listenerLength]++
  },
  checker (programIdToCheck, robotIdToCheck) {
    Object
      .keys(this[listenerList])
      .forEach(
        id => {
          const {callback, programId, robotId} = this[listenerList][id]
          const isValid                        = programIdToCheck === programId && robotId === robotIdToCheck

          if (isValid) {
            callback()
            delete this[listenerList][id]
            this[listenerLength]--
          }
        },
      )
  },
}

export default programLoadWatcher
