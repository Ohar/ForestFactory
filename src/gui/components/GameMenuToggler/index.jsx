import actionMenuToggle from '@/store/menu/actions/menu-toggle'
import classNames from 'classnames'
import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { GAME_MENU_BTN_HIDE, GAME_MENU_BTN_SHOW } from '@/l10n/keys'
import './styles.less'

function GameMenuToggler ({l10n, toggle, isMenuOpen}) {
  return (
    <button
      className={classNames('GameMenuToggler', {
        'GameMenuToggler-open': isMenuOpen,
      })}
      onClick={toggle}
      title={
        isMenuOpen
          ? l10n[GAME_MENU_BTN_HIDE]
          : l10n[GAME_MENU_BTN_SHOW]
      }
    >
      ⚙
    </button>
  )
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators(
    {toggle: actionMenuToggle},
    dispatch,
  )
}

function mapStateToProps (state, ownProps) {
  return {
    ...ownProps,
    isMenuOpen: state.menuState.isMenuOpen,
    l10n      : state.optionsState.l10n,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(GameMenuToggler)
