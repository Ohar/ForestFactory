import {BIND_PROGRAM_TO_ROBOT} from '@/store/editor/action-types'
import getProgramById from '@/utils/getProgramById'

export default function signalSetProgramToRobot (robot, programId, setChild = true) {
  const program = getProgramById(programId)

  const programToSet = setChild && program
    ? program.createChild()
    : program

	return dispatch => {
    if (programToSet) {
      robot.setProgram(programToSet)

      // Костыль
      setTimeout(() => {
        dispatch({
          type     : BIND_PROGRAM_TO_ROBOT,
          robotId  : robot.ffId,
          programId: programToSet.id,
        })
      }, 200)
		}
	}
}
