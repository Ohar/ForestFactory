import getLabelName from '@/utils/get-label-name'

export default function createCommandFunction (program, params) {
	const labelName      = getLabelName(params[0]),
		  labelAfterCall = getLabelName(params[1])

	return () => {
		const nextStage = program.labels[labelName]

		program.stage = nextStage
		program.returnToLabelsStack.push(labelAfterCall)

		return program.run()
	}
}
