import BlockGrab from '@/blockly/custom/tools/gripper/block-grab'
import BlockMoveByXY from '@/blockly/custom/chassis/block-move-by-x-y'
import BlockMoveToXY from '@/blockly/custom/chassis/block-move-to-x-y'
import BlockMoveToObj from '@/blockly/custom/chassis/block-move-to-obj'
import BlockPutInto from '@/blockly/custom/tools/gripper/block-put-into'
import BlockRelease from '@/blockly/custom/tools/gripper/block-release'
import BlockSaw from '@/blockly/custom/tools/chainsaw/block-saw'
import BlockSeek from '@/blockly/custom/vision/block-seek'
import BlockSeekNearest from '@/blockly/custom/vision/block-seek-nearest'

const blocklyBlockList = [
    BlockGrab,
    BlockMoveByXY,
    BlockMoveToXY,
    BlockMoveToObj,
    BlockPutInto,
    BlockRelease,
    BlockSaw,
    BlockSeek,
    BlockSeekNearest,
]

export default blocklyBlockList
