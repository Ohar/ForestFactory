const chests = {
	open    : {
		gid  : 1430,
		frame: 5,
	},
	halfOpen: {
		gid  : 1428,
		frame: 3,
	},
	closed  : {
		gid  : 1426,
		frame: 1,
	},
}

const wood = {
	big   : {
		gid   : 1046,
		frame : 21,
		volume: 10,
	},
	medium: {
		gid   : 1050,
		frame : 25,
		volume: 5,
	},
	small : {
		gid   : 1030,
		frame : 5,
		volume: 1,
	},
}

const tree = {
	foliar: {
		gid   : 1431,
		frame : 0,
	},
	pine  : {
		gid   : 1432,
		frame : 1,
	},
}

const robot = {
  look_up: {
		gid   : 1436,
		frame : 0,
	},
  look_left: {
		gid   : 1435,
		frame : 2,
	},
  look_down: {
		gid   : 1434,
		frame : 1,
	},
  look_right: {
		gid   : 1433,
		frame : 3,
	},
}

export {wood, chests, tree, robot}
