'use strict'

import chai         from 'chai'
import BlockRelease from '@/blockly/custom/tools/gripper/block-release'

describe(
    'BlockRelease', () => {
        it(
            'Возвращает объект', () => {
                chai.assert.isObject(BlockRelease)
            }
        )

        describe(
            'Свойства', () => {
                describe(
                    'name', () => {
                        it(
                            'Это строка', () => {
                                chai.assert.isString(BlockRelease.name)
                            }
                        )
                    }
                )

                describe(
                    'generator', () => {
                        it(
                            'Это функция', () => {
                                chai.assert.isFunction(BlockRelease.generator)
                            }
                        )

                        it(
                            'Возвращает строку', () => {
                                chai.assert.isString(BlockRelease.generator())
                            }
                        )
                    }
                )


                describe(
                    'definition', () => {
                        it(
                            'Это объект', () => {
                                chai.assert.isObject(BlockRelease.definition)
                            }
                        )

                        it(
                            'Имеет метод init', () => {
                                chai.assert.isFunction(BlockRelease.definition.init)
                            }
                        )
                    }
                )

            }
        )
    }
)
