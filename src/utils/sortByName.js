export default function sortByName (a, b) {
  return a.name > b.name ? 1 : -1
}
