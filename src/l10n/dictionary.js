import PROGRAM_NAME from '@/constants/PROGRAM_NAME'
import ROBOT_NAME from '@/constants/ROBOT_NAME'
import {
  EDITOR_HEADER,
  EMPTY_PROGRAM,
  EMPTY_ROBOT,
  GAME_MENU_BTN_HIDE,
  GAME_MENU_BTN_LOAD,
  GAME_MENU_BTN_MAIN_MENU,
  GAME_MENU_BTN_SAVE,
  GAME_MENU_BTN_SHOW,
  LOAD_GAME_MENU_BTN_CANCEL_DELETING,
  LOAD_GAME_MENU_BTN_DELETE,
  LOAD_GAME_MENU_BTN_DELETING,
  LOAD_GAME_MENU_BTN_LOAD,
  LOAD_GAME_MENU_EMPTY,
  LOAD_GAME_MENU_HEADER,
  LVL_SCREEN_PRESS_ANY_KEY,
  MAIN_MENU_EXIT_TEXT,
  MAIN_MENU_LOAD_GAME_TEXT,
  MAIN_MENU_LOAD_GAME_TITLE,
  MAIN_MENU_LOAD_LAST_SAVE,
  MAIN_MENU_LOGO_ALT,
  MAIN_MENU_OPTIONS_TEXT,
  MAIN_MENU_START_NEW_GAME_TEXT,
  MAIN_MENU_START_NEW_GAME_TITLE,
  MODULE_EMPTY,
  MODULE_LIST_EMPTY,
  MODULE_LIST_HEADER,
  MODULE_NAME_BATTERY,
  MODULE_NAME_CHAINSAW,
  MODULE_NAME_CHASSIS,
  MODULE_NAME_DEFAULT,
  MODULE_NAME_GRIPPER,
  MODULE_NAME_MEMORY,
  MODULE_NAME_STORAGE,
  MODULE_NAME_TOOL,
  MODULE_NAME_VISION,
  NOT_READY_YET,
  PROGRAM_ADD_BTN_CLEAR,
  PROGRAM_ADD_BTN_NEW,
  PROGRAM_CONTROL_BTN_PAUSE,
  PROGRAM_CONTROL_BTN_START,
  PROGRAM_CONTROL_BTN_STOP,
  PROGRAM_COPY_BTN_TEMPLATE_COMMON,
  PROGRAM_COPY_BTN_TEXT,
  PROGRAM_COPY_TO_STORAGE_BTN_TEXT,
  PROGRAM_COPY_TO_STORAGE_NEW_NAME,
  PROGRAM_COPY_TO_STORAGE_TEMPLATE_COMMON,
  PROGRAM_COPY_TO_STORAGE_TEMPLATE_DISABLED,
  PROGRAM_DELETE_BTN_TEMPLATE_COMMON,
  PROGRAM_DELETE_BTN_TEXT,
  PROGRAM_LOAD_BTN_TEMPLATE_DISABLED,
  PROGRAM_LOAD_BTN_TEXT,
  PROGRAM_LOAD_BTN_TITLE,
  PROGRAM_NOT_SELECTED,
  PROGRAM_RENAME_BTN_TEXT,
  PROGRAM_RENAME_REQUEST_TEXT,
  PROGRAM_RENAME_TEMPLATE_COMMON,
  PROGRAM_NOT_SELECTED_DISABLED,
  PROGRAM_UPDATE_TITLE_TEMPLATE_1,
  PROGRAM_UPDATE_TITLE_TEMPLATE_2,
  REPOSITORY_EMPTY,
  REPOSITORY_HEADER,
  ROBOT_DEFAULT_NAME,
  ROBOT_NOT_SELECTED,
  ROBOT_NOT_SELECTED_DISABLED,
  TAB_FULLSCREEN_BTN_NARROWER,
  TAB_FULLSCREEN_BTN_WIDER,
  TAB_HEADER_EDITOR,
  TAB_HEADER_MODULES,
  TAB_HEADER_REPOSITORY,
  UNKNOWN_KEY,
  UNKNOWN_MODULE,
  TAB_BTN_HIDE_TEXT,
  ROBOT_NO_PROGRAM,
  BLOCKLY_MOVE_BY_TEXT,
  BLOCKLY_MOVE_BY_X_ARROW_TEXT,
  BLOCKLY_MOVE_BY_Y_ARROW_TEXT,
  BLOCKLY_TO_OBJ_TEXT,
  BLOCKLY_TO_XY_TEXT,
  BLOCKLY_TO_XY_LABEL_X,
  BLOCKLY_TO_XY_LABEL_Y,
  BLOCKLY_CHAINSAW_TEXT,
  BLOCKLY_GRAB_TEXT,
  BLOCKLY_PUT_INFO_TEXT,
  BLOCKLY_RELEASE_TEXT,
  BLOCKLY_SEEK_ALL_TEXT,
  BLOCKLY_SEEK_NEAREST_TEXT,
  LEVEL_NAME_DEFAULT,
  COMPILER_ERROR_PARSING,
  SEEK_LIST_ROBOTS,
  SEEK_LIST_TREES,
  QUEST_HEADER_TEXT,
  MAIN_MENU_TITLE_TEXT,
  MODULE_OWNER_EMPTY_NAME,
  MODULE_EMPTY_NAME,
  EMPTY_OPTION_TEXT,
  VERSION_TEXT,
  WINSCREEN_VICTORY_HEADER,
  WINSCREEN_VICTORY_TEXT,
  WINSCREEN_VICTORY_BTN_NEXT,
  WINSCREEN_VICTORY_BTN_TO_MAIN_MANU,
  FOV_TEXT,
  MAIN_MENU_OPTIONS_TITLE,
  OPTIONS_HEADER_TEXT,
  LANGUAGE_TOGGLER_EN_GB,
  LANGUAGE_TOGGLER_RU_RU, LANGUAGE_TOGGLER_HEADER_TEXT,
} from '@/l10n/keys'
import { en_GB, ru_RU } from '@/l10n/locales'

export default {
  [BLOCKLY_MOVE_BY_TEXT]: {
    [en_GB]: 'Move by',
    [ru_RU]: 'Сместиться на',
  },
  [BLOCKLY_MOVE_BY_X_ARROW_TEXT]: {
    [en_GB]: 'X →',
    [ru_RU]: 'X →',
  },
  [BLOCKLY_MOVE_BY_Y_ARROW_TEXT]: {
    [en_GB]: 'Y ↓',
    [ru_RU]: 'Y ↓',
  },

  [BLOCKLY_TO_OBJ_TEXT]: {
    [en_GB]: 'Go to',
    [ru_RU]: 'Идти к',
  },

  [BLOCKLY_TO_XY_LABEL_X]: {
    [en_GB]: 'X',
    [ru_RU]: 'X',
  },
  [BLOCKLY_TO_XY_LABEL_Y]: {
    [en_GB]: 'Y',
    [ru_RU]: 'Y',
  },
  [BLOCKLY_TO_XY_TEXT]: {
    [en_GB]: 'Move to',
    [ru_RU]: 'Идти в',
  },

  [BLOCKLY_CHAINSAW_TEXT]: {
    [en_GB]: 'Saw',
    [ru_RU]: 'Пилить',
  },

  [BLOCKLY_GRAB_TEXT]: {
    [en_GB]: 'Grab',
    [ru_RU]: 'Схватить',
  },

  [BLOCKLY_PUT_INFO_TEXT]: {
    [en_GB]: 'Put into',
    [ru_RU]: 'Положить ношу в',
  },

  [BLOCKLY_RELEASE_TEXT]: {
    [en_GB]: 'Release',
    [ru_RU]: 'Бросить ношу',
  },

  [BLOCKLY_SEEK_ALL_TEXT]: {
    [en_GB]: 'all',
    [ru_RU]: 'все',
  },

  [BLOCKLY_SEEK_NEAREST_TEXT]: {
    [en_GB]: 'nearest:',
    [ru_RU]: 'ближайший:',
  },

  [COMPILER_ERROR_PARSING]: {
    [en_GB]: 'Compiler: Code parsing problem',
    [ru_RU]: 'Компилятор: Ошибка парсинга',
  },

  [EDITOR_HEADER]: {
    [en_GB]: 'Program Editor',
    [ru_RU]: 'Редактор программ',
  },

  [EMPTY_PROGRAM]: {
    [en_GB]: 'NO PROGRAM',
    [ru_RU]: 'НЕТ ПРОГРАММЫ',
  },
  [EMPTY_OPTION_TEXT]: {
    [en_GB]: '—',
    [ru_RU]: '—',
  },
  [EMPTY_ROBOT]: {
    [en_GB]: 'NO ROBOT',
    [ru_RU]: 'НЕТ РОБОТА',
  },

  [LVL_SCREEN_PRESS_ANY_KEY]: {
    [en_GB]: 'Press any key to continue',
    [ru_RU]: 'Чтобы продолжить, нажмите любую клавишу',
  },

  [FOV_TEXT]: {
    [en_GB]: 'Vision range',
    [ru_RU]: 'Дальность зерния',
  },

  [GAME_MENU_BTN_HIDE]: {
    [en_GB]: 'Hide menu',
    [ru_RU]: 'Скрыть меню',
  },
  [GAME_MENU_BTN_LOAD]: {
    [en_GB]: '🗁 Load',
    [ru_RU]: '🗁 Загрузить',
  },
  [GAME_MENU_BTN_MAIN_MENU]: {
    [en_GB]: '🖹 Main Menu',
    [ru_RU]: '🖹 Главное меню',
  },
  [GAME_MENU_BTN_SAVE]: {
    [en_GB]: '💾 Save',
    [ru_RU]: '💾 Сохранить',
  },
  [GAME_MENU_BTN_SHOW]: {
    [en_GB]: 'Show menu',
    [ru_RU]: 'Показать меню',
  },

  [LANGUAGE_TOGGLER_HEADER_TEXT]: {
    [en_GB]: 'Language',
    [ru_RU]: 'Язык',
  },
  [LANGUAGE_TOGGLER_EN_GB]: {
    [en_GB]: 'English',
    [ru_RU]: 'English',
  },
  [LANGUAGE_TOGGLER_RU_RU]: {
    [en_GB]: 'Русский',
    [ru_RU]: 'Русский',
  },

  [LEVEL_NAME_DEFAULT]: {
    [en_GB]: 'Level name',
    [ru_RU]: 'Название уровня',
  },

  [LOAD_GAME_MENU_BTN_CANCEL_DELETING]: {
    [en_GB]: 'Cancel deleting',
    [ru_RU]: 'Отменить удаление',
  },
  [LOAD_GAME_MENU_BTN_DELETING]: {
    [en_GB]: 'Deleting…',
    [ru_RU]: 'Удаляется…',
  },
  [LOAD_GAME_MENU_BTN_DELETE]: {
    [en_GB]: '× Delete',
    [ru_RU]: '× Удалить',
  },
  [LOAD_GAME_MENU_BTN_LOAD]: {
    [en_GB]: 'Load',
    [ru_RU]: 'Загрузить',
  },
  [LOAD_GAME_MENU_EMPTY]: {
    [en_GB]: '— No saved games —',
    [ru_RU]: '— Нет сохранённых игр —',
  },
  [LOAD_GAME_MENU_HEADER]: {
    [en_GB]: 'Load game',
    [ru_RU]: 'Загрузить игру',
  },

  [MAIN_MENU_EXIT_TEXT]: {
    [en_GB]: 'Exit',
    [ru_RU]: 'Выход',
  },
  [MAIN_MENU_LOAD_GAME_TEXT]: {
    [en_GB]: 'Load game',
    [ru_RU]: 'Загрузить игру',
  },
  [MAIN_MENU_LOAD_GAME_TITLE]: {
    [en_GB]: 'Choose and load game',
    [ru_RU]: 'Выбрать и загрузить игру',
  },
  [MAIN_MENU_LOAD_LAST_SAVE]: {
    [en_GB]: 'Load last save',
    [ru_RU]: 'Загрузить последнее сохранение',
  },
  [MAIN_MENU_LOGO_ALT]: {
    [en_GB]: 'Logo Forest Factory',
    [ru_RU]: 'Лого Forest Factory',
  },
  [MAIN_MENU_OPTIONS_TEXT]: {
    [en_GB]: 'Options',
    [ru_RU]: 'Настройки',
  },
  [MAIN_MENU_OPTIONS_TITLE]: {
    [en_GB]: 'Open game options',
    [ru_RU]: 'Открыть игровые настройки',
  },
  [MAIN_MENU_START_NEW_GAME_TEXT]: {
    [en_GB]: 'New game',
    [ru_RU]: 'Новая игра',
  },
  [MAIN_MENU_START_NEW_GAME_TITLE]: {
    [en_GB]: 'Start a new game from first level',
    [ru_RU]: 'Начать новую игру с первого уровня',
  },
  [MAIN_MENU_TITLE_TEXT]: {
    [en_GB]: 'Forest Factory',
    [ru_RU]: 'Forest Factory',
  },

  [MODULE_EMPTY]: {
    [en_GB]: 'No module',
    [ru_RU]: 'Модуля нет',
  },
  [MODULE_EMPTY_NAME]: {
    [en_GB]: '—',
    [ru_RU]: '—',
  },

  [MODULE_LIST_EMPTY]: {
    [en_GB]: 'No modules yet',
    [ru_RU]: 'Модулей пока что нет',
  },
  [MODULE_LIST_HEADER]: {
    [en_GB]: 'Modules',
    [ru_RU]: 'Модули',
  },

  [MODULE_NAME_BATTERY]: {
    [en_GB]: 'Battery',
    [ru_RU]: 'Батарея',
  },
  [MODULE_NAME_CHAINSAW]: {
    [en_GB]: 'Chainsaw',
    [ru_RU]: 'Электропила',
  },
  [MODULE_NAME_CHASSIS]: {
    [en_GB]: 'Chassis',
    [ru_RU]: 'Шасси',
  },
  [MODULE_NAME_DEFAULT]: {
    [en_GB]: 'Module',
    [ru_RU]: 'Модуль',
  },
  [MODULE_NAME_GRIPPER]: {
    [en_GB]: 'Gripper',
    [ru_RU]: 'Хватало',
  },
  [MODULE_NAME_MEMORY]: {
    [en_GB]: 'Memory',
    [ru_RU]: 'Память',
  },
  [MODULE_NAME_STORAGE]: {
    [en_GB]: 'Storage',
    [ru_RU]: 'Хранилище',
  },
  [MODULE_NAME_TOOL]: {
    [en_GB]: 'Tool',
    [ru_RU]: 'Инструмент',
  },
  [MODULE_NAME_VISION]: {
    [en_GB]: 'Vision',
    [ru_RU]: 'Зрение',
  },

  [MODULE_OWNER_EMPTY_NAME]: {
    [en_GB]: '—',
    [ru_RU]: '—',
  },

  [OPTIONS_HEADER_TEXT]: {
    [en_GB]: 'Options',
    [ru_RU]: 'Настройки',
  },

  [NOT_READY_YET]: {
    [en_GB]: 'Not ready yet',
    [ru_RU]: 'Пока что не сделано',
  },

  [PROGRAM_ADD_BTN_CLEAR]: {
    [en_GB]: '🗋 Clear program',
    [ru_RU]: '🗋 Очистить программу',
  },
  [PROGRAM_ADD_BTN_NEW]: {
    [en_GB]: '🗋 New program',
    [ru_RU]: '🗋 Новая программа',
  },

  [PROGRAM_DELETE_BTN_TEXT]: {
    [en_GB]: `╳ Delete program`,
    [ru_RU]: `╳ Удалить программу`,
  },
  [PROGRAM_DELETE_BTN_TEMPLATE_COMMON]: {
    [en_GB]: `Delete chosen program “${PROGRAM_NAME}” from repository.`,
    [ru_RU]: `Удалить выбранную программу «${PROGRAM_NAME}» из хранилища.`,
  },

  [PROGRAM_LOAD_BTN_TEMPLATE_DISABLED]: {
    [en_GB]: 'Not available: you didn\'t select nor robot, nor program',
    [ru_RU]: 'Недоступно: не выбраны ни робот, ни программа.',
  },
  [PROGRAM_LOAD_BTN_TEXT]: {
    [en_GB]: `🠊🤖 Load program into robot`,
    [ru_RU]: `🠊🤖 Загрузить программу в робота`,
  },
  [PROGRAM_LOAD_BTN_TITLE]: {
    [en_GB]: `Load chosen program “${PROGRAM_NAME}” into robot “${ROBOT_NAME}” instead of its current program.`,
    [ru_RU]: `Загрузить выбранную программу «${PROGRAM_NAME}» в робота «${ROBOT_NAME}» вместо текущей.`,
  },

  [PROGRAM_NOT_SELECTED]: {
    [en_GB]: 'Program is not selected',
    [ru_RU]: 'Программа не выбрана',
  },
  [PROGRAM_NOT_SELECTED_DISABLED]: {
    [en_GB]: 'Not available: program is not selected.',
    [ru_RU]: 'Недоступно: программа не выбрана.',
  },

  [PROGRAM_RENAME_BTN_TEXT]: {
    [en_GB]: '✎ Rename program',
    [ru_RU]: '✎ Переименовать программу',
  },
  [PROGRAM_RENAME_REQUEST_TEXT]: {
    [en_GB]: 'Enter new program name',
    [ru_RU]: 'Введите новое имя программы',
  },
  [PROGRAM_RENAME_TEMPLATE_COMMON]: {
    [en_GB]: `Rename selected program “${PROGRAM_NAME}”`,
    [ru_RU]: `Переименовать выбранную программу «${PROGRAM_NAME}»`,
  },

  [PROGRAM_UPDATE_TITLE_TEMPLATE_1]: {
    [en_GB]: `Create new program for current robot “${ROBOT_NAME}”`,
    [ru_RU]: `Создать новую программу для текущего робота «${ROBOT_NAME}»`,
  },
  [PROGRAM_UPDATE_TITLE_TEMPLATE_2]: {
    [en_GB]: `Current program “${PROGRAM_NAME}” would be deleted.`,
    [ru_RU]: `Нынешняя программа «${PROGRAM_NAME}» будет удалена.`,
  },

  [PROGRAM_CONTROL_BTN_PAUSE]: {
    [en_GB]: 'Pause',
    [ru_RU]: 'Приостановить',
  },
  [PROGRAM_CONTROL_BTN_START]: {
    [en_GB]: 'Start',
    [ru_RU]: 'Запустить',
  },
  [PROGRAM_CONTROL_BTN_STOP]: {
    [en_GB]: 'Stop',
    [ru_RU]: 'Остановить',
  },

  [PROGRAM_COPY_BTN_TEMPLATE_COMMON]: {
    [en_GB]: `Add copy of selected program “${PROGRAM_NAME}” into program storage.`,
    [ru_RU]: `Добавить копию выбранной программы «${PROGRAM_NAME}» в хранилище.`,
  },
  [PROGRAM_COPY_BTN_TEXT]: {
    [en_GB]: 'Copy program',
    [ru_RU]: 'Копировать программу',
  },

  [PROGRAM_COPY_TO_STORAGE_BTN_TEXT]: {
    [en_GB]: '🖫 Copy program into storage',
    [ru_RU]: '🖫 Скопировать программу в хранилище',
  },
  [PROGRAM_COPY_TO_STORAGE_NEW_NAME]: {
    [en_GB]: programName => `${programName} (copy)`,
    [ru_RU]: programName => `${programName} (копия)`,
  },
  [PROGRAM_COPY_TO_STORAGE_TEMPLATE_COMMON]: {
    [en_GB]: `Add copy of the program “${PROGRAM_NAME}” of the current robot “${ROBOT_NAME}” to the common program storage.`,
    [ru_RU]: `Добавить копию программы «${PROGRAM_NAME}» текущего робота «${ROBOT_NAME}» в общее хранилище программ.`,
  },
  [PROGRAM_COPY_TO_STORAGE_TEMPLATE_DISABLED]: {
    [en_GB]: `Not available: current robot “${ROBOT_NAME}” has no program to copy.`,
    [ru_RU]: `Недоступно: у текущего робота «${ROBOT_NAME}» нет программы для копирования.`,
  },

  [REPOSITORY_EMPTY]: {
    [en_GB]: 'Storage is empty',
    [ru_RU]: 'Хранилище пока что пусто',
  },
  [REPOSITORY_HEADER]: {
    [en_GB]: 'Program Storage',
    [ru_RU]: 'Хранилище программ',
  },

  [ROBOT_DEFAULT_NAME]: {
    [en_GB]: 'Robot',
    [ru_RU]: 'Робот',
  },
  [ROBOT_NO_PROGRAM]: {
    [en_GB]: 'Program is not loaded',
    [ru_RU]: 'Программа не загружена',
  },
  [ROBOT_NOT_SELECTED]: {
    [en_GB]: 'Robot is not selected',
    [ru_RU]: 'Робот не выбран',
  },
  [ROBOT_NOT_SELECTED_DISABLED]: {
    [en_GB]: 'Not available: robot is not selected.',
    [ru_RU]: 'Недоступно: робот не выбран.',
  },

  [SEEK_LIST_ROBOTS]: {
    [en_GB]: 'Robots',
    [ru_RU]: 'Роботы',
  },
  [SEEK_LIST_TREES]: {
    [en_GB]: 'Trees',
    [ru_RU]: 'Деревья',
  },

  [TAB_FULLSCREEN_BTN_WIDER]: {
    [en_GB]: 'Expand',
    [ru_RU]: 'Развернуть пошире',
  },
  [TAB_FULLSCREEN_BTN_NARROWER]: {
    [en_GB]: 'Squeeze',
    [ru_RU]: 'Свернуть поуже',
  },
  [TAB_HEADER_EDITOR]: {
    [en_GB]: 'Editor',
    [ru_RU]: 'Редактор',
  },
  [TAB_HEADER_MODULES]: {
    [en_GB]: 'Modules',
    [ru_RU]: 'Модули',
  },
  [TAB_HEADER_REPOSITORY]: {
    [en_GB]: 'Repository',
    [ru_RU]: 'Хранилище',
  },
  [TAB_BTN_HIDE_TEXT]: {
    [en_GB]: 'Hide?',
    [ru_RU]: 'Скрыть?',
  },

  [QUEST_HEADER_TEXT]: {
    [en_GB]: questIndex => `Quest ${questIndex}`,
    [ru_RU]: questIndex => `Задача № ${questIndex}`,
  },

  [UNKNOWN_KEY]: {
    [en_GB]: 'Not localized',
  },
  [UNKNOWN_MODULE]: {
    [en_GB]: 'Unknown module',
    [ru_RU]: 'Неизвестный модуль',
  },

  [VERSION_TEXT]: {
    [en_GB]: version => `v ${version}`,
    [ru_RU]: version => `v ${version}`,
  },

  [WINSCREEN_VICTORY_BTN_NEXT]: {
    [en_GB]: 'Next level',
    [ru_RU]: 'Следующий уровень',
  },
  [WINSCREEN_VICTORY_BTN_TO_MAIN_MANU]: {
    [en_GB]: 'Main menu',
    [ru_RU]: 'В главное меню',
  },
  [WINSCREEN_VICTORY_HEADER]: {
    [en_GB]: 'Победа!',
    [ru_RU]: 'Победа!',
  },
  [WINSCREEN_VICTORY_TEXT]: {
    [en_GB]: levelName => `Level ${levelName} completed!`,
    [ru_RU]: levelName => `Уровень ${levelName} пройден!`,
  },
}
