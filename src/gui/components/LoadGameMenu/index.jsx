import { orderBy } from 'lodash'
import moment from 'moment'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import actionLoadGameMenuHide from '@/store/gui/actions/load-game-menu-hide'
import signalCancelDeleteGame from '@/store/menu/signals/game-cancel-delete'
import signalDeleteGame from '@/store/menu/signals/game-delete'
import signalLoadGame from '@/store/menu/signals/game-load'
import Popup from '@/gui/components/Popup'
import Spinner from '@/gui/components/Spinner'
import dbApi from '@/singletons/db/api'
import './styles.less'
import getLocalizedString from '@/l10n/getLocalizedString'
import {
  LOAD_GAME_MENU_BTN_CANCEL_DELETING,
  LOAD_GAME_MENU_BTN_DELETE,
  LOAD_GAME_MENU_BTN_DELETING,
  LOAD_GAME_MENU_BTN_LOAD,
  LOAD_GAME_MENU_EMPTY,
  LOAD_GAME_MENU_HEADER,
} from '@/l10n/keys'

moment.locale('ru')

const INPUT_NAME = 'game_to_load'

class LoadGameMenu extends Component {
  constructor (...args) {
    super(...args)

    this.state = {
      chosen       : null,
      savedGameList: [],
    }

    this.selectGame       = this.selectGame.bind(this)
    this.getSavedGameList = this.getSavedGameList.bind(this)
    this.loadGame         = this.loadGame.bind(this)
    this.renderList       = this.renderList.bind(this)
    this.renderFullList   = this.renderFullList.bind(this)
    this.renderEmptyList  = this.renderEmptyList.bind(this)
    this.renderControls   = this.renderControls.bind(this)
    this.deleteGame       = this.deleteGame.bind(this)
  }

  componentWillReceiveProps ({visible, isGameDeleting}) {
    if (
      visible && visible !== this.props.visible
      || !isGameDeleting && isGameDeleting !== this.props.isGameDeleting
    ) {
      this.getSavedGameList()
    }
  }

  getSavedGameList () {
    dbApi
      .getAllLevel()
      .then(
        savedGameList => {
          this.setState({
            savedGameList,
            chosen: null,
          })
        },
      )
  }

  selectGame (e) {
    this.setState({chosen: e.target.value})
  }

  loadGame () {
    const {chosen}   = this.state
    const {loadGame} = this.props

    loadGame(chosen)
  }

  deleteGame () {
    const {chosen}     = this.state
    const {deleteGame} = this.props

    deleteGame(chosen)
  }

  renderControls () {
    const {isGameDeleting, cancelDeleteGame, l10n} = this.props
    const {chosen}                                 = this.state

    return (
      <div className='LoadGameMenu_controls'>
        {
          isGameDeleting
          ? (
            <button
              onClick={cancelDeleteGame}
              disabled={!isGameDeleting}
              className='LoadGameMenu_button LoadGameMenu_button-cancelDelete'
            >
              <Spinner visible={isGameDeleting}/>
              {l10n[LOAD_GAME_MENU_BTN_CANCEL_DELETING]}
            </button>
          )
          : (
            <button
              onClick={this.deleteGame}
              disabled={!chosen}
              className='LoadGameMenu_button LoadGameMenu_button-delete'
            >
              <Spinner visible={isGameDeleting}/>
              {l10n[LOAD_GAME_MENU_BTN_DELETE]}
            </button>
          )
        }
        <button
          onClick={this.loadGame}
          disabled={!chosen}
          className='LoadGameMenu_button LoadGameMenu_button-load'
        >
          {l10n[LOAD_GAME_MENU_BTN_LOAD]}
        </button>
      </div>
    )
  }

  renderFullList (savedGameListToShow) {
    const {chosen}          = this.state
    const {deletingLevelId, l10n} = this.props

    return (
      <ul className='LoadGameMenu_list'>
        {
          orderBy(savedGameListToShow, 'date', 'desc').map(
            ({id: savedGameId, name, date}) => {
              const inputId = `${INPUT_NAME}_${savedGameId}`
              const time    = moment(date).format('LLLL')

              return (
                <li
                  className='LoadGameMenu_item'
                  key={savedGameId}
                >
                  <input
                    id={inputId}
                    value={savedGameId}
                    type='radio'
                    name={INPUT_NAME}
                    className='LoadGameMenu_radio'
                    onChange={this.selectGame}
                    checked={savedGameId === chosen}
                  />
                  <label
                    className='LoadGameMenu_label'
                    htmlFor={inputId}
                  >
                    {
                      savedGameId === deletingLevelId
                      ? (
                        <span>
                          <Spinner/>
                          <span className='LoadGameMenu_deleting'>
                            {l10n[LOAD_GAME_MENU_BTN_DELETING]}
                          </span>
                        </span>
                      )
                      : null
                    }
                    <span className='LoadGameMenu_name'>
                      {getLocalizedString(name)}
                    </span>
                    {
                      __DEV__
                      ? <code>[{savedGameId}]</code>
                      : null
                    }
                    <time
                      className='LoadGameMenu_time'
                      dateTime={date}
                    >
                      {time}
                    </time>
                  </label>
                </li>
              )
            },
          )
        }
      </ul>
    )
  }

  renderEmptyList () {
    const {l10n} = this.props

    return (
      <ul className='LoadGameMenu_list'>
        <li className='LoadGameMenu_item'>
          {l10n[LOAD_GAME_MENU_EMPTY]}
        </li>
      </ul>
    )
  }

  renderList () {
    const {savedGameList} = this.state

    return savedGameList.length
           ? this.renderFullList(savedGameList)
           : this.renderEmptyList()
  }

  render () {
    const {close, l10n, visible} = this.props

    return visible
           ? (
             <Popup
               className='LoadGameMenu'
               close={close}
             >
               <header className='LoadGameMenu_header'>
                 {l10n[LOAD_GAME_MENU_HEADER]}
               </header>
               {this.renderList()}
               {this.renderControls()}
             </Popup>
           )
           : null
  }
}

function mapStateToProps (state, ownProps) {
  return {
    ...ownProps,
    visible        : state.guiState.loadGameMenuVisible,
    isGameLoading  : state.menuState.isGameLoading,
    isGameDeleting : state.menuState.isGameDeleting,
    deletingLevelId: state.menuState.deletingLevelId,
    l10n           : state.optionsState.l10n,
  }
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators(
    {
      close           : actionLoadGameMenuHide,
      loadGame        : signalLoadGame,
      deleteGame      : signalDeleteGame,
      cancelDeleteGame: signalCancelDeleteGame,
    },
    dispatch,
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(LoadGameMenu)
