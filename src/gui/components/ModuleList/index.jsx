import ModuleDresser from '@/gui/components/ModuleDresser'
import ModuleInfo from '@/gui/components/ModuleInfo'
import ModulePicker from '@/gui/components/ModulePicker'
import React from 'react'
import connect from 'react-redux/es/connect/connect'
import { MODULE_LIST_HEADER } from '@/l10n/keys'
import './styles.less'

function ModuleList ({l10n}) {
  return (
    <section className='ModuleList'>
      <header className='ModuleList_header'>
        {l10n[MODULE_LIST_HEADER]}
      </header>
      <section className='ModuleList_panels'>
        <ModulePicker/>
        <ModuleInfo/>
        <ModuleDresser/>
      </section>
    </section>
  )
}

function mapStateToProps (state, ownProps) {
  return {
    ...ownProps,
    l10n: state.optionsState.l10n,
  }
}

export default connect(mapStateToProps)(ModuleList)
