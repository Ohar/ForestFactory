import compact from 'lodash/compact'
import createCommand from '@/compiler/create-command'
import IR_COMMANDS_LIST from '@/constants/ir_commands_list'

export default function convertIRToCommands (program) {
  return program
    .listingIR
    .split('\n')
    .map(row => row.trim())
    .filter(e => e)
    .map(
      row => {
        const tokens      = compact(row.trim().split(' ')),
              commandName = tokens.shift()

        return {commandName, tokens}
      },
    )
    .filter(({commandName}) => IR_COMMANDS_LIST.find(e => e === commandName))
    .map(
      ({commandName, tokens}, i) => createCommand(commandName, program, tokens, i),
    )
}
