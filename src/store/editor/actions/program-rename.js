import {PROGRAM_RENAME} from '@/store/editor/action-types'

export default function actionProgramRename (programId, name) {
    return dispatch => {
      dispatch({
        type: PROGRAM_RENAME,
        programId,
        name,
      })
    }
}
