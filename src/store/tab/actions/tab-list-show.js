import { SET_TAB_LIST_VISIBILITY } from '@/store/tab/action-types'

export default function signalTabListShow () {
  return dispatch => {
    dispatch(
      {
        type: SET_TAB_LIST_VISIBILITY,
        tabListVisibility: true,
      },
    )
  }
}
