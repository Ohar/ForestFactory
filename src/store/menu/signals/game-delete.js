import { sortBy } from 'lodash'
import { DELETE_GAME_REQUEST, DELETE_GAME_SUCCESS } from '@/store/menu/action-types'
import dbApi from '@/singletons/db/api'

const DELETE_DELAY = 5000

export default function signalDeleteGame (levelId) {
  return dispatch => {
    const deletingTimeout = setTimeout(
      () => {
        dbApi.delLevel(levelId)
        dispatch({type: DELETE_GAME_SUCCESS})
      },
      DELETE_DELAY,
    )

    dispatch({
      type           : DELETE_GAME_REQUEST,
      deletingLevelId: levelId,
      deletingTimeout,
    })
  }
}
