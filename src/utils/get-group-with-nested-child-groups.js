import flattenDeep from 'lodash/flattenDeep'
import getAllNestedGroups from '@/utils/get-all-nested-groups'

function getGroupWithNestedChildGroups (parentGroup, groupName) {
	return flattenDeep(getAllNestedGroups(parentGroup)).find(({name}) => name === groupName)
}

export default getGroupWithNestedChildGroups
