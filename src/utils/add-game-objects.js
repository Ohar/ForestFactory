import Place from '@/classes/Place'
import { chests, tree, wood } from '@/constants/game_objects_data'
import Chest from '@/sprites/buildings/chest'
import Resource from '@/sprites/pobs/resource'
import FoliarTree from '@/sprites/trees/foliar-tree'
import PineTree from '@/sprites/trees/pine-tree'
import Robot from '@/sprites/mobs/robot'

export default function addGameObjects (world, levelMap) {
  const objParams = [
    {
      layer      : 'buildings',
      cacheName  : 'chests',
      gid        : chests.open.gid,
      frame      : chests.open.frame,
      spriteClass: Chest,
      group      : world
        .getByName('buildings')
        .getByName('chests'),
    },
    {
      layer      : 'buildings',
      cacheName  : 'chests',
      gid        : chests.halfOpen.gid,
      frame      : chests.halfOpen.frame,
      spriteClass: Chest,
      group      : world
        .getByName('buildings')
        .getByName('chests'),
    },
    {
      layer      : 'buildings',
      cacheName  : 'chests',
      gid        : chests.closed.gid,
      frame      : chests.closed.frame,
      spriteClass: Chest,
      group      : world
        .getByName('buildings')
        .getByName('chests'),
    },
    {
      layer      : 'resources',
      cacheName  : 'farming_fishing',
      gid        : wood.big.gid,
      frame      : wood.big.frame,
      spriteClass: Resource,
      group      : world
        .getByName('pobs')
        .getByName('resources'),
    },
    {
      layer      : 'resources',
      cacheName  : 'farming_fishing',
      gid        : wood.medium.gid,
      frame      : wood.medium.frame,
      spriteClass: Resource,
      group      : world
        .getByName('pobs')
        .getByName('resources'),
    },
    {
      layer      : 'resources',
      cacheName  : 'farming_fishing',
      gid        : wood.small.gid,
      frame      : wood.small.frame,
      spriteClass: Resource,
      group      : world
        .getByName('pobs')
        .getByName('resources'),
    },
    {
      layer      : 'trees',
      cacheName  : 'trunk',
      gid        : tree.foliar.gid,
      frame      : tree.foliar.frame,
      spriteClass: FoliarTree,
      group      : world.getByName('trees'),
    },
    {
      layer      : 'trees',
      cacheName  : 'trunk',
      gid        : tree.pine.gid,
      frame      : tree.pine.frame,
      spriteClass: PineTree,
      group      : world.getByName('trees'),
    },
  ]

  objParams.forEach(
    ({layer, cacheName, gid, frame, group, spriteClass}) => {
      if (levelMap.objects && levelMap.objects[layer] && levelMap.objects[layer].length) {
        levelMap.objects[layer].forEach(
          ({x, y, name, gid: objGid, properties = {}}) => {
            if (gid === objGid) {
              const sprite = new spriteClass({game: world.game, x, y, key: cacheName, frame, ...properties})

              // Copied from “levelMap.createFromObjects”
              sprite.name = name;

              group.add(sprite);

              for (let property in properties) {
                group.set(sprite, property, properties[property], false, false, 0, true);
              }
            }
          }
        )
      }
    },
  )

  if (levelMap.objects && levelMap.objects.robots) {
    levelMap.objects.robots.forEach(
      ({x, y, properties = {}, ...otherProps}) => new Robot({game: world.game, x, y, ...otherProps, ...properties}),
    )
  }

  if (levelMap.objects && levelMap.objects.places) {
    levelMap.objects.places.forEach(
      ({x, y, width, height, name}) => new Place({game: levelMap.game, x, y, width, height, name}),
    )
  }
}
