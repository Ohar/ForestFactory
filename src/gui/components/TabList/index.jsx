import tabIdList from '@/constants/tabIdList'
import Editor from '@/gui/components/Editor'
import ModuleList from '@/gui/components/ModuleList'
import Repository from '@/gui/components/Repository'
import Tab from '@/gui/components/Tab'
import TabControl from '@/gui/components/TabControl'
import TabListFullScreenToggler from '@/gui/components/TabListFullScreenToggler'
import signalTabListHide from '@/store/tab/actions/tab-list-hide'
import classNames from 'classnames'
import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { TAB_BTN_HIDE_TEXT, TAB_HEADER_EDITOR, TAB_HEADER_MODULES, TAB_HEADER_REPOSITORY } from '@/l10n/keys'
import './styles.less'

function TabList (
  {
    fullScreen,
    hide,
    guiVisible,
    isEditorEnabled,
    isModulesEnabled,
    isRepositoryEnabled,
    isVisible,
    l10n,
  }
) {
  const tabHeaderList = [
    (
      isEditorEnabled
      ? {
          id  : tabIdList.editor,
          text: l10n[TAB_HEADER_EDITOR],
      }
      : null
    ),
    (
      isRepositoryEnabled
      ? {
        id  : tabIdList.repository,
        text: l10n[TAB_HEADER_REPOSITORY],
      }
      : null
    ),
    (
      isModulesEnabled
      ? {
        id  : tabIdList.modules,
        text: l10n[TAB_HEADER_MODULES],
      }
      : null
    ),
  ]
    .filter(e => e)

  const tabBodyList = [
    {
      id  : tabIdList.editor,
      body: (guiVisible && <Editor/>),
    },
    (
      isRepositoryEnabled
      ? {
        id  : tabIdList.repository,
        body: <Repository/>,
      }
      : null
    ),
    {
      id  : tabIdList.modules,
      body: <ModuleList/>,
    },
  ]
    .filter(e => e)

  return (
    <section
      className={classNames(
        'TabList',
        {
          'TabList-hidden'    : !isVisible,
          'TabList-visible'   : isVisible,
          'TabList-fullScreen': fullScreen,
          'TabList-len_1'    : tabHeaderList.length === 1,
          'TabList-len_2'    : tabHeaderList.length === 2,
          'TabList-len_3'    : tabHeaderList.length === 3,
        }
      )}
    >
      <button
        className='TabList_hideBtn'
        onClick={hide}
        title={l10n[TAB_BTN_HIDE_TEXT]}
      >
        ↑
      </button>

      <TabListFullScreenToggler/>

      <section className='TabList_controlList'>
        {
          tabHeaderList.map(
            ({id, text}) => (
              <TabControl
                tabId={id}
                key={id}
              >
                {text}
              </TabControl>
            ),
          )
        }
      </section>
      {
        tabBodyList.map(
          ({id, body}) => (
            <Tab
              tabId={id}
              key={id}
            >
              {body}
            </Tab>
          ),
        )
      }
    </section>
  )
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators(
    {
      hide: signalTabListHide,
    },
    dispatch,
  )
}

function mapStateToProps (state, ownProps) {
  return {
    ...ownProps,
    guiVisible         : state.guiState.gameplayUIVisible,
    isVisible          : state.tabState.tabListVisibility,
    fullScreen         : state.tabState.fullScreen,
    isEditorEnabled    : state.guiState.isEditorEnabled,
    isModulesEnabled   : state.guiState.isModulesEnabled,
    isRepositoryEnabled: state.guiState.isRepositoryEnabled,
    l10n               : state.optionsState.l10n,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TabList)
