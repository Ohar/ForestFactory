import store from '@/store'

export default function getRobotById (robotId) {
  const {editorState: {robotList}} = store.getState()

  return robotList.find(({ffId}) => ffId === robotId)
}
