import { GRIPPER_MODULE_TYPE } from '@/constants/moduleTypes'
import Tool from '@/parts/modules/tool'

const GRIPPER_RANGE = 10

export default class Gripper extends Tool {
  constructor ({id, owner, ownerId}) {
    super({
      id,
      moduleType: GRIPPER_MODULE_TYPE,
      range: GRIPPER_RANGE,
      owner,
      ownerId,
    })

    this.grabbedObj       = null
    this.grabbedObjParent = null
  }

  grab (ffId) {
    if (this.grabbedObj) {
      this.release()
    }

    const grabTarget = this.getTargetByFFId(ffId)
    const {vision} = this.owner.modules

    let result = false

    if (grabTarget && vision && vision.getDistance(grabTarget) <= this.range) {
      this.grabbedObj       = grabTarget
      this.grabbedObjParent = this.grabbedObj.parent
      this.grabbedObj.x     = this.grabbedObj.x - this.owner.x
      this.grabbedObj.y     = this.grabbedObj.y - this.owner.y

      this.owner.addChild(this.grabbedObj)

      result = true
    }

    return {
      finished     : true,
      result,
      resultForSave: result,
    }
  }

  release () {
    if (this.grabbedObj && this.owner.contains(this.grabbedObj)) {
      this.grabbedObj.x = this.grabbedObj.x + this.owner.x
      this.grabbedObj.y = this.grabbedObj.y + this.owner.y

      this.owner.removeChild(this.grabbedObj)

      if (this.grabbedObjParent) {
        this.grabbedObjParent.add(this.grabbedObj)
      }

      this.grabbedObj       = null
      this.grabbedObjParent = null
    }

    return {
      finished     : true,
      result       : true,
      resultForSave: true,
    }
  }

  putInto (ffId) {
    const putTarget = this.getTargetByFFId(ffId)

    let result = false

    if (
      this.grabbedObj
      && putTarget
      && putTarget.modules
      && putTarget.modules.storage
    ) {
      const {storage} = putTarget.modules

      if (storage.ifWillFit(this.grabbedObj)) {
        this.owner.removeChild(this.grabbedObj)
        storage.put(this.grabbedObj)

        this.grabbedObj       = null
        this.grabbedObjParent = null
        result                = true
      } else if (this.grabbedObj.isSeparable && storage.ifWillFitPartly(this.grabbedObj)) {
        const separatedPart = this.grabbedObj.separate(storage.emptyVolume)

        storage.put(separatedPart)

        this.release()

        result = true
      } else {
        this.release()
        result = false
      }
    } else {
      result = false
    }

    return {
      finished     : true,
      result,
      resultForSave: result,
    }
  }
}
