import { SET_WINSCREEN_VISIBILITY } from '@/store/gui/action-types'

export default function actionWinScreenHide () {
  return dispatch => {
    dispatch({
      type            : SET_WINSCREEN_VISIBILITY,
      winScreenVisible: false,
    })
  }
}
