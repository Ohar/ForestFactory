import langNamesList from '@/gui/components/LocaleToggler/langNamesList'
import { LANGUAGE_TOGGLER_HEADER_TEXT } from '@/l10n/keys'
import localesList from '@/l10n/locales'
import signalLocaleSet from '@/store/options/signals/locale-set'
import classNames from 'classnames'
import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import './styles.less'

function LocaleToggler ({l10n, localeSet, locale: currentLocale}) {
  return (
    <section className='LocaleToggler'>
      <header className='LocaleToggler_header'>
        {l10n[LANGUAGE_TOGGLER_HEADER_TEXT]}
      </header>
      <ul className='LocaleToggler_list'>
        {
          localesList.map(
            locale => (
              <li
                key={locale}
                className={classNames(
                  'LocaleToggler_item',
                  `LocaleToggler_item-${locale}`,
                )}
              >
                <input
                  id={`locale_${locale}`}
                  type='radio'
                  value={locale}
                  name='locale'
                  checked={locale === currentLocale}
                  onClick={() => localeSet(locale)}
                  className='LocaleToggler_input'
                />
                <label
                  htmlFor={`locale_${locale}`}
                  className='LocaleToggler_label'>
                  {l10n[langNamesList[locale]]}
                </label>
              </li>
            ),
          )
        }
      </ul>
    </section>
  )
}

function mapStateToProps (state, ownProps) {
  return {
    ...ownProps,
    l10n  : state.optionsState.l10n,
    locale: state.optionsState.locale,
  }
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators(
    {
      localeSet: signalLocaleSet,
    },
    dispatch,
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(LocaleToggler)
