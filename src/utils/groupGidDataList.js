import { chests, robot, tree, wood } from '@/constants/game_objects_data'
import FoliarTree from '@/sprites/trees/foliar-tree'
import Resource from '@/sprites/pobs/resource'
import PineTree from '@/sprites/trees/pine-tree'
import Chest from '@/sprites/buildings/chest'
import Robot from '@/sprites/mobs/robot'

const groupGidDataList = [
  {
    constructor: Resource,
    data       : wood,
  },
  {
    constructor: Chest,
    data       : chests,
  },
  {
    constructor: Robot,
    data       : robot,
  },
  {
    constructor: FoliarTree,
    data       : tree,
  },
  {
    constructor: PineTree,
    data       : tree,
  },
]

export default groupGidDataList
