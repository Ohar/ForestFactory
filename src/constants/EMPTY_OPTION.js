import store from '@/store'
import { EMPTY_OPTION_TEXT } from '@/l10n/keys'

const {optionsState: {l10n}} = store.getState()

const EMPTY_OPTION = {
  text : l10n[EMPTY_OPTION_TEXT],
  value: '-',
}

export default EMPTY_OPTION
