const levelMap = require('./levelMap.json')
const modules  = require('./modules.json')
const options  = require('./options.json')
const plot     = require('./plot.json')
const programs = require('./programs.json')
const toolbox  = require('./toolbox.xml')
const level_3  = require('./../level-3')

module.exports = {
  levelMap,
  modules,
  options,
  plot,
  programs,
  toolbox,
  name     : {
    en_GB: 'Level 2',
    ru_RU: 'Уровень 2',
  },
  id       : 'level-2',
  nextLvlId: level_3.id,
}
