import Phaser from 'phaser'

class DefaultGroup extends Phaser.Group {
	constructor (game, name = 'default-group') {
		const parent          = game.world,
		      addToStage      = true,
		      enableBody      = true,
		      physicsBodyType = Phaser.Physics.ARCADE;
		
		super(game, parent, name, addToStage, enableBody, physicsBodyType)
		
		this.game = game
	}
	
}

export default  DefaultGroup
