import {noop} from 'lodash'
import React, {Component, PropTypes} from 'react'
import {connect} from 'react-redux'
import getProgramId from '@/utils/getProgramId'
import getRobotProgram from '@/utils/getRobotProgram'
import './styles.less'

class ProgramControlBtn extends Component {
  constructor (...args) {
    super(...args)

    this.onClick = this.onClick.bind(this)
  }

  onClick () {
    const {robot, commandName} = this.props

    if (robot) {
      const program = getRobotProgram(robot)

      if (
        program
        && program[commandName]
      ) {
        program[commandName]()
      }
    }
  }

  render () {
    const {robot, robotsPrograms, children} = this.props

    const programId = getProgramId(robotsPrograms, robot)

    return (
      <button
        onClick={this.onClick}
        className='ProgramControlBtn'
        disabled={!programId}
      >
        {children}
      </button>
    )
  }
}

ProgramControlBtn.propTypes = {
  children      : PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
  commandName   : PropTypes.string.isRequired,
  programs      : PropTypes.array.isRequired,
  robot         : PropTypes.object,
  robotsPrograms: PropTypes.array.isRequired,
}

ProgramControlBtn.defaultProps = {
  children      : null,
  commandName   : '',
  programs      : [],
  robot         : {},
  robotsPrograms: [],
}

function mapStateToProps (state, ownProps) {
  return {
    ...ownProps,
    robotsPrograms: state.editorState.robotsPrograms,
  }
}

export default connect(mapStateToProps)(ProgramControlBtn)
