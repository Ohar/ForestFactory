import Level from '@/classes/level'
import level_1 from '@/levels/level-1'
import loadModules from '@/loaders/loadModules'
import loadOptions from '@/loaders/loadOptions'
import loadPlot from '@/loaders/loadPlot'
import loadPrograms from '@/loaders/loadPrograms'
import loadSprites from '@/loaders/loadSprites'
import loadToolbox from '@/loaders/loadToolbox'
import dbApi from '@/singletons/db/api'
import store from '@/store'
import actionSetLevelLoadedData from '@/store/editor/actions/set-level-loaded-data'
import signalTabListHide from '@/store/tab/actions/tab-list-hide'
import fixLevelObjectsOrder from '@/utils/fixLevelObjectsOrder'
import loadLocalFile from '@/utils/load-local-file'
import normalizeLevelObj from '@/utils/normalizeLevelObj'
import {startDevLevel} from 'root/config.level'

export default function loadLevel ({load, levelId = __DEV__ && startDevLevel || level_1.id}) {
  return dbApi
    .getLevel(levelId)
    // Если существует сохранённый уровень, загрузить его
    // Иначе — загрузить первый уровень
    .then(
      levelSaved => new Promise(resolve => {
        if (levelSaved) {
          resolve(new Level(levelSaved))
        } else {
          loadLocalFile(`./dist/levels/${levelId}.json`, 'json')
            .then(
              plotlevel => {
                if (plotlevel) {
                  resolve(plotlevel)
                } else {
                  resolve(level_1)
                }
              },
            )
            .catch(
              e => {
                console.error('loadLevel fail', e) // eslint-disable-line no-console
              },
            )
        }
      }),
    )
    .then(fixLevelObjectsOrder)
    .then(level => {
      if (__DEV__) {
        store.dispatch(actionSetLevelLoadedData(normalizeLevelObj(level)))
      }

      store.dispatch(signalTabListHide())

      return level
    })
    .then(
      level => Promise
        .all([
          loadModules(level.modules),
          loadToolbox(level.toolbox),
          loadPrograms(level.programs),
          loadPlot(level.plot),
          loadOptions(level.options),
          loadSprites(level.levelMap, load),
        ])
        .then(() => level),
    )
    .catch(
      err => {
        console.error('Fail loadLevel', err)
      },
    )
}
