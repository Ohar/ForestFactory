import IRNode from '@/classes/ir-node'
import convertAstNodeToIR from '@/compiler/converters/convert-ast-node-to-ir'
import uid from 'uid'

export default function convertMemberExpression (node, program, loopLabels) {
	const obj  = convertAstNodeToIR(node.object, program, loopLabels).id,
		  prop = convertAstNodeToIR(node.property, program, loopLabels).id,
		  id   = uid(20)

	return new IRNode({
		id,
		code: `${obj}.${prop}`,
	})
}
