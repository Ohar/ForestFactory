import { SET_PROGRAM_DELETE_BTN_AVAILABILITY } from '@/store/gui/action-types'

export default function actionSetProgramDeleteBtnAvailability (isProgramDeleteBtnEnabled) {
  return dispatch => {
    dispatch({
      type: SET_PROGRAM_DELETE_BTN_AVAILABILITY,
      isProgramDeleteBtnEnabled,
    })
  }
}
