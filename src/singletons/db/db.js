// Работа с IndexedDB

import addItemList from '@/singletons/db/add-item-list'
import deleteItem from '@/singletons/db/delete-item'
import getAllItems from '@/singletons/db/get-all-items'
import getItem from '@/singletons/db/get-item'
import putItem from '@/singletons/db/put-item'

const db = {
	add    : addItemList,
	del    : deleteItem,
	getAll : getAllItems,
  getItem,
  putItem,
}

export default db
