import CAMERA_SPEED from '@/constants/camera_speed'
import DefaultGroup from '@/groups/default-group'
import showQuestInfo from '@/quest/showQuestInfo'
import store from '@/store'
import actionGameplayUIShow from '@/store/gui/actions/gameplay-ui-show'
import savingListener from '@/store/menu/listeners/savingListener'
import addGameObjects from '@/utils/add-game-objects'
import throttle from 'lodash/throttle'
import Phaser from 'phaser'
import KineticScrolling from 'phaser-kinetic-scrolling-plugin'

export default class GameState extends Phaser.State {
  init () {
    this.game.kineticScrolling = this.game.plugins.add(KineticScrolling(Phaser))

    this.game.kineticScrolling.configure({
      kineticMovement   : false,
      timeConstantScroll: 0,
      verticalScroll    : true,
    })
  }

  preload () {
    this.game.physics.startSystem(Phaser.Physics.ARCADE)

    this.game.cursors = this.game.input.keyboard.createCursorKeys()

    this.levelMap = this.game.add.tilemap('levelMap')
    this.levelMap.addTilesetImage('terrain')
    this.levelMap.addTilesetImage('chests')
    this.levelMap.addTilesetImage('farming_fishing')

    this.layers = {
      ground     : this.levelMap.createLayer('ground'),
      environment: this.levelMap.createLayer('environment'),
      surfaces   : this.levelMap.createLayer('surfaces'),
    }

    this.layers.environment.name = 'environment'

    const pobs      = new DefaultGroup(this.game, 'pobs'),
          buildings = new DefaultGroup(this.game, 'buildings'),
          resources = new DefaultGroup(this.game, 'resources'),
          chests    = new DefaultGroup(this.game, 'chests'),
          robots    = new DefaultGroup(this.game, 'robots'),
          trees     = new DefaultGroup(this.game, 'trees')

    pobs.add(resources)
    buildings.add(chests)

    this.game.world.add(robots)
    this.game.world.add(pobs)
    this.game.world.add(buildings)
    this.game.world.add(trees)

    // Places are not displayObjects, it couldn't be inside group, only inside array
    this.game.world.places = []
    this.game.world.places.name = 'places'

    addGameObjects(this.game.world, this.levelMap)

    this.layers.ground.resizeWorld()
  }

  create () {
    window.onresize = throttle(
      () => {
        this.game.scale.setGameSize(window.innerWidth, window.innerHeight)

        Object.entries(this.layers).forEach(
          ([layerName, layer]) => {
            layer.resize(window.innerWidth, window.innerHeight)
          },
        )
      },
      500,
    )

    showQuestInfo(this.game)
    store.dispatch(actionGameplayUIShow())
    store.subscribe(savingListener(this.levelMap, store.getState().menuState))

    this.game.kineticScrolling.start()
  }

  update () {
    const {camera, cursors: {up, down, left, right}, time: {elapsed}} = this.game

    if (
      up.isDown
      || down.isDown
      || left.isDown
      || right.isDown
    ) {
      const scrollSpeed = CAMERA_SPEED * elapsed

      if (up.isDown) {
        camera.y -= scrollSpeed
      } else if (down.isDown) {
        camera.y += scrollSpeed
      }

      if (left.isDown) {
        camera.x -= scrollSpeed
      } else if (right.isDown) {
        camera.x += scrollSpeed
      }
    }
  }
}
