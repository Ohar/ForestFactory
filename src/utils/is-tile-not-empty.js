import EMPTY_TILE_INDEX from '@/constants/empty_tile_index'

function isTileNotEmpty (tile) {
	return tile.index !== EMPTY_TILE_INDEX
}

export default isTileNotEmpty
