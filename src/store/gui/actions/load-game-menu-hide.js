import { SET_LOAD_GAME_MENU_VISIBILITY } from '@/store/gui/action-types'

export default function actionLoadGameMenuHide () {
  return dispatch => {
    dispatch({
      type               : SET_LOAD_GAME_MENU_VISIBILITY,
      loadGameMenuVisible: false,
    })
  }
}
