export default function createCommandFunctionEnd (program) {
	return () => {
		if (program.returnToLabelsStack.length) {
			const labelName = program.returnToLabelsStack.pop(),
				  nextStage = program.labels[labelName]
			
			program.stage = nextStage
		}
		
		return program.run()
	}
}
