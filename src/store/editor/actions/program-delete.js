import {PROGRAM_DELETE} from '@/store/editor/action-types'

export default function actionProgramDelete (programId) {
    return dispatch => {
      dispatch({
        type: PROGRAM_DELETE,
        programId,
      })
    }
}
