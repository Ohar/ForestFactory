#!/usr/bin/env bash

# Copy builds to server
copyBuilds () {
    # Save script directory path
    # https://stackoverflow.com/a/24112741/2059884
    local -r PARENT_PATH=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
    cd "$PARENT_PATH"

    source './generate-file-name.sh'

    local -r DEPLOY_FOLDER=`node -p 'require("./../config.deploy.json")["path"]'`
    local -r SERVER=`node -p 'require("./../config.deploy.json")["server"]'`
    local -r USERNAME=`node -p 'require("./../config.deploy.json")["user"]'`

    local -r BUILD_FOLDER=`node -p 'require("./../config.build.json")["buildFolder"]'`
    IFS=',' read -a PLATFORM_LIST <<< `node -p 'require("./../config.build.json")["platform"].join(",")'`

    local -r URL=$USERNAME@$SERVER

    copyToServer () {
        local -r PLATFORM=$1
        local -r FILE_NAME=`generateFileName $PLATFORM`
        echo "Copying $FILE_NAME…"
        scp ./../$BUILD_FOLDER/$FILE_NAME $URL:$DEPLOY_FOLDER
    }

    echo "Copying builds started"

    for PLATFORM in "${PLATFORM_LIST[@]}"
    do
        copyToServer $PLATFORM
    done

    echo "Copying builds finished"
}

copyBuilds
