#!/usr/bin/env bash

generateFileName () {
    # Save script directory path
    # https://stackoverflow.com/a/24112741/2059884
    local -r PARENT_PATH=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
    cd "$PARENT_PATH"

    local -r PLATFORM=$1
    echo `node -p "require('./generate-file-name.js')('$PLATFORM')"`
}
