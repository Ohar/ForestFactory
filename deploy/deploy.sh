#!/bin/bash

deploy () {
    # Save script directory path
    # https://stackoverflow.com/a/24112741/2059884
    local -r PARENT_PATH=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
    cd "$PARENT_PATH"

    echo 'Deploy started'
    source './copyBuilds.sh'
    source './generateReleasePage.sh'
    echo 'Deploy finished'
}

deploy
