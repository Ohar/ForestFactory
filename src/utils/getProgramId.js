export default function getProgramId (robotsPrograms, robot) {
  const robotProgram = robot
    ? robotsPrograms.find(e => e.robotId === robot.ffId)
    : null

  return robotProgram
    ? robotProgram.programId
    : null
}
