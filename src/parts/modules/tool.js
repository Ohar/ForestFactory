import { TOOL_MODULE_TYPE } from '@/constants/moduleTypes'
import Module from '@/parts/modules/module'
import convertTileToSprite from '@/utils/convert-tile-to-sprite'
import isTile from '@/utils/is-tile'

export default class Tool extends Module {
  constructor ({id, moduleType = TOOL_MODULE_TYPE, owner, ownerId, range}) {
    super({id, moduleType, owner, ownerId})

    this.range = range
  }

  getTargetByFFId (ffId) {
    const grabTarget = this.owner.game.ffVarList.get(ffId)

    return isTile(grabTarget)
           ? convertTileToSprite(grabTarget, ffId, this.owner.game)
           : grabTarget
  }
}
