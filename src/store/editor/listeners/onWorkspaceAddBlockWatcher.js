import { isUndefined } from 'lodash'
import uid from 'uid'

// TODO: use Symbol
const listenerList = '_listenerList'
const listenerLength = '_listenerLength'

function checkBlockValue (value) {
  return block => block.inputList.some(
    input => input.fieldRow.some(
      row => row.value_ === value,
    ),
  )
}

function checkBlockChild (child) {
  return block => block.childBlocks_.some(
    checkBlockValue(child.value),
  )
}

const onWorkspaceAddBlockWatcher = {
  get hasListeners () {
    return Boolean(this[listenerLength])
  },
  [listenerLength]: 0,
  [listenerList]: {},
  checker (blockList) {
    Object
      .keys(this[listenerList])
      .forEach(
        id => {
          const {block: {child, type, value}, callback} = this[listenerList][id]

          let isValid = false

          if (isUndefined(child) && isUndefined(value)) {
            // Значение не важно, нужно проверить только наличие блока такого типа
            isValid = blockList.some(e => e.type === type)
          } else
          if (!isUndefined(child) && !isUndefined(value)) {
            // Нужно проверить как тип и значение, так и детей блока
            // Нужно ли это?
          } else
          if (!isUndefined(value)) {
            // Нужно проверить тип и значение блока
            isValid = blockList
              .filter(e => e.type === type)
              .some(checkBlockValue(value))
          } else
          if (!isUndefined(child)) {
            // Нужно проверить тип и детей блока
            isValid = blockList
              .filter(e => e.type === type)
              .some(checkBlockChild(child))
          }

          if (isValid) {
            callback()
            delete this[listenerList][id]
            this[listenerLength]--
          }
        },
      )
  },
  addListener (data) {
    const id = uid()

    this[listenerList][id] = {
      ...data,
      id,
    }

    this[listenerLength]++
  },
}

export default onWorkspaceAddBlockWatcher
