import * as actionTypes from '@/store/menu/action-types'
import DEFAULT_STATE from '@/store/menu/default_state'

export default function menuReducer (state = DEFAULT_STATE, action) {
	switch (action.type) {
    case actionTypes.SAVE_GAME_REQUEST:
      return {
        ...state,
        isGameSaving: true,
      }

    case actionTypes.SAVE_GAME_SUCCESS:
    case actionTypes.SAVE_GAME_FAILURE:
      return {
        ...state,
        isGameSaving: false,
      }

    case actionTypes.DELETE_GAME_REQUEST:
      return {
        ...state,
        isGameDeleting: true,
        deletingLevelId: action.deletingLevelId,
        deletingTimeout: action.deletingTimeout,
      }

    case actionTypes.DELETE_GAME_SUCCESS:
    case actionTypes.DELETE_GAME_FAILURE:
      return {
        ...state,
        isGameDeleting: false,
        deletingLevelId: DEFAULT_STATE.deletingLevelId,
        deletingTimeout: DEFAULT_STATE.deletingTimeout,
      }

    case actionTypes.LOAD_GAME_REQUEST:
      return {
        ...state,
        isGameLoading: true,
      }

    case actionTypes.LOAD_GAME_SUCCESS:
    case actionTypes.LOAD_GAME_FAILURE:
      return {
        ...state,
        isGameLoading: false,
      }

    case actionTypes.MENU_SHOW:
      return {
        ...state,
        isMenuOpen: true,
      }

    case actionTypes.MENU_HIDE:
      return {
        ...state,
        isMenuOpen: false,
      }

    case actionTypes.MENU_TOGGLE:
      return {
        ...state,
        isMenuOpen: !state.isMenuOpen,
      }

		default:
			return state
	}
}
