export default function convertMemoryValToBoolean (val) {
	switch (val) {
		case 'true':
			return true
		
		case 'false':
			return false
		
		default:
			return Boolean(val)
	}
}
