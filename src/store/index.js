import editorReducer from '@/store/editor/reducer'
import guiReducer from '@/store/gui/reducer'
import menuReducer from '@/store/menu/reducer'
import optionsReducer from '@/store/options/reducer'
import plotReducer from '@/store/plot/reducer'
import tabReducer from '@/store/tab/reducer'
import { applyMiddleware, combineReducers, createStore } from 'redux'
import thunk from 'redux-thunk'

const reducers = combineReducers({
  editorState : editorReducer,
  guiState    : guiReducer,
  menuState   : menuReducer,
  optionsState: optionsReducer,
  plotState   : plotReducer,
  tabState    : tabReducer,
})

const store = createStore(
  reducers,
  applyMiddleware(thunk),
)

export default store
