export default class IRNode {
	constructor (params = {id: null, code: ''}) {
		this.id   = params.id
		this.code = params.code
	}
}
