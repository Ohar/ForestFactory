import ModuleTile from '@/gui/components/ModuleTile'
import actionModuleSetSelected from '@/store/editor/actions/module-set-selected'
import sortByModuleType from '@/utils/sortByModuleType'
import React from 'react'
import connect from 'react-redux/es/connect/connect'
import { bindActionCreators } from 'redux'
import { MODULE_LIST_EMPTY } from '@/l10n/keys'
import './styles.less'

function ModulePicker ({l10n, moduleSetSelected, moduleList}) {
  return (
    <section className='ModulePicker'>
      {
        moduleList.length
        ? moduleList
          .sort(sortByModuleType)
          .map(
            module => (
              <ModuleTile
                key={module.id}
                module={module}
              />
            ),
          )
        : l10n[MODULE_LIST_EMPTY]
      }
    </section>
  )
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators(
    {
      moduleSetSelected: actionModuleSetSelected,
    },
    dispatch,
  )
}

function mapStateToProps (state, ownProps) {
  return {
    ...ownProps,
    moduleList: state.editorState.moduleList,
    l10n: state.optionsState.l10n,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ModulePicker)
