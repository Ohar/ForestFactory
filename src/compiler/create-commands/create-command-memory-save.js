import getRobotById from '@/utils/getRobotById'

function createCommandMemorySave (program, params) {
	const [name, ...savingProps] = params

	return () => {
    const robot = getRobotById(program.robotId)
		const val = getSavingVal(savingProps, robot.modules.memory)

		robot.modules.memory.save(name, val)

		return true
	}
}

function getSavingVal (props, memory) {
	if (props.length === 1) {
		return props[0]
	} else {
		const operator = props[1],
		      var1     = memory.load(props[0]),
		      var2     = memory.load(props[2])

		switch (operator) {
			case '+': {
				const simple  = var1 + var2,
				      numeric = Number(var1) + Number(var2)

				return simple === numeric
					? simple
					: numeric
			}

			case '-':
				return var1 -  var2
			case '*':
				return var1 *  var2
			case '/':
				return var1 /  var2
			case '<':
				return var1 <  var2
			case '<=':
				return var1 <= var2
			case '>':
				return var1 >  var2
			case '>=':
				return var1 >= var2
			case '==':
				return var1 == var2
			case '!=':
				return var1 != var2
			default:
				throw new Error('Unknown operator', operator)
		}
	}
}

export default createCommandMemorySave
