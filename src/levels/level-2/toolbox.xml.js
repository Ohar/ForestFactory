const toolbox = `
<xml id="toolbox" style="display: none">
    <category name="Робот" colour="0">
        <category name="Зрение" colour="300">
            <!--<block type="FF_Seek"/>-->
            <block type="FF_SeekNearest"/>
        </category>
        <category name="Шасси" colour="0">
            <block type="FF_MoveToObj"/>
            <!--<block type="FF_MoveToXY">-->
                <!--<value name="X">-->
                    <!--<shadow type="math_number">-->
                        <!--<field name="NUM">0</field>-->
                    <!--</shadow>-->
                <!--</value>-->
                <!--<value name="Y">-->
                    <!--<shadow type="math_number">-->
                        <!--<field name="NUM">0</field>-->
                    <!--</shadow>-->
                <!--</value>-->
            <!--</block>-->
            <block type="FF_MoveByXY">
                <value name="X">
                    <shadow type="math_number">
                        <field name="NUM">0</field>
                    </shadow>
                </value>
                <value name="Y">
                    <shadow type="math_number">
                        <field name="NUM">0</field>
                    </shadow>
                </value>
            </block>
        </category>
        <!--<category name="Энергия" colour="50"/>-->
        <!--<category name="Память" colour="#A65C81" custom="VARIABLE"/>-->
        <!--<category name="Программа" colour="150"/>-->
        <!--<category name="Контейнер" colour="200"/>-->
        <!--<category name="Инструменты" colour="250">-->
            <!--<category name="Хватало" colour="250">-->
                <!--<block type="FF_Grab"/>-->
                <!--<block type="FF_PutInto"/>-->
                <!--<block type="FF_Release"/>-->
            <!--</category>-->
            <!--<category name="Цепная пила" colour="220">-->
                <!--<block type="FF_Saw"/>-->
            <!--</category>-->
        <!--</category>-->
    </category>
    <!--<sep/>-->
    <!--<category name="Логика" colour="#5C81A6">-->
        <!--<block type="controls_if"/>-->
        <!--<block type="logic_compare">-->
            <!--<field name="OP">EQ</field>-->
        <!--</block>-->
        <!--<block type="logic_operation">-->
            <!--<field name="OP">AND</field>-->
        <!--</block>-->
        <!--<block type="logic_negate"/>-->
        <!--<block type="logic_boolean">-->
            <!--<field name="BOOL">TRUE</field>-->
        <!--</block>-->
        <!--<block type="logic_null"/>-->
        <!--&lt;!&ndash;<block type="logic_ternary"/>&ndash;&gt;-->
    <!--</category>-->
    <!--<category name="Циклы" colour="#5CA65C">-->
        <!--<block type="controls_repeat_ext">-->
            <!--<value name="TIMES">-->
                <!--<shadow type="math_number">-->
                    <!--<field name="NUM">10</field>-->
                <!--</shadow>-->
            <!--</value>-->
        <!--</block>-->
        <!--<block type="controls_whileUntil">-->
            <!--<field name="MODE">WHILE</field>-->
        <!--</block>-->
        <!--<block type="controls_for">-->
            <!--<field name="VAR">i</field>-->
            <!--<value name="FROM">-->
                <!--<shadow type="math_number">-->
                    <!--<field name="NUM">1</field>-->
                <!--</shadow>-->
            <!--</value>-->
            <!--<value name="TO">-->
                <!--<shadow type="math_number">-->
                    <!--<field name="NUM">10</field>-->
                <!--</shadow>-->
            <!--</value>-->
            <!--<value name="BY">-->
                <!--<shadow type="math_number">-->
                    <!--<field name="NUM">1</field>-->
                <!--</shadow>-->
            <!--</value>-->
        <!--</block>-->
        <!--<block type="controls_forEach">-->
            <!--<field name="VAR">j</field>-->
        <!--</block>-->
        <!--<block type="controls_flow_statements">-->
            <!--<field name="FLOW">BREAK</field>-->
        <!--</block>-->
    <!--</category>-->
    <!--<category name="Математика" colour="#5C68A6">-->
        <!--<block type="math_number">-->
            <!--<field name="NUM">0</field>-->
        <!--</block>-->
        <!--<block type="math_arithmetic">-->
            <!--<field name="OP">ADD</field>-->
            <!--<value name="A">-->
                <!--<shadow type="math_number">-->
                    <!--<field name="NUM">1</field>-->
                <!--</shadow>-->
            <!--</value>-->
            <!--<value name="B">-->
                <!--<shadow type="math_number">-->
                    <!--<field name="NUM">1</field>-->
                <!--</shadow>-->
            <!--</value>-->
        <!--</block>-->
        <!--<block type="math_single">-->
            <!--<field name="OP">ROOT</field>-->
            <!--<value name="NUM">-->
                <!--<shadow type="math_number">-->
                    <!--<field name="NUM">9</field>-->
                <!--</shadow>-->
            <!--</value>-->
        <!--</block>-->
        <!--<block type="math_trig">-->
            <!--<field name="OP">SIN</field>-->
            <!--<value name="NUM">-->
                <!--<shadow type="math_number">-->
                    <!--<field name="NUM">45</field>-->
                <!--</shadow>-->
            <!--</value>-->
        <!--</block>-->
        <!--<block type="math_constant">-->
            <!--<field name="CONSTANT">PI</field>-->
        <!--</block>-->
        <!--<block type="math_number_property">-->
            <!--<mutation divisor_input="false"/>-->
            <!--<field name="PROPERTY">EVEN</field>-->
            <!--<value name="NUMBER_TO_CHECK">-->
                <!--<shadow type="math_number">-->
                    <!--<field name="NUM">0</field>-->
                <!--</shadow>-->
            <!--</value>-->
        <!--</block>-->
        <!--<block type="math_round">-->
            <!--<field name="OP">ROUND</field>-->
            <!--<value name="NUM">-->
                <!--<shadow type="math_number">-->
                    <!--<field name="NUM">3.1</field>-->
                <!--</shadow>-->
            <!--</value>-->
        <!--</block>-->
        <!--<block type="math_on_list">-->
            <!--<mutation op="SUM"/>-->
            <!--<field name="OP">SUM</field>-->
        <!--</block>-->
        <!--<block type="math_modulo">-->
            <!--<value name="DIVIDEND">-->
                <!--<shadow type="math_number">-->
                    <!--<field name="NUM">64</field>-->
                <!--</shadow>-->
            <!--</value>-->
            <!--<value name="DIVISOR">-->
                <!--<shadow type="math_number">-->
                    <!--<field name="NUM">10</field>-->
                <!--</shadow>-->
            <!--</value>-->
        <!--</block>-->
        <!--<block type="math_constrain">-->
            <!--<value name="VALUE">-->
                <!--<shadow type="math_number">-->
                    <!--<field name="NUM">50</field>-->
                <!--</shadow>-->
            <!--</value>-->
            <!--<value name="LOW">-->
                <!--<shadow type="math_number">-->
                    <!--<field name="NUM">1</field>-->
                <!--</shadow>-->
            <!--</value>-->
            <!--<value name="HIGH">-->
                <!--<shadow type="math_number">-->
                    <!--<field name="NUM">100</field>-->
                <!--</shadow>-->
            <!--</value>-->
        <!--</block>-->
        <!--<block type="math_random_int">-->
            <!--<value name="FROM">-->
                <!--<shadow type="math_number">-->
                    <!--<field name="NUM">1</field>-->
                <!--</shadow>-->
            <!--</value>-->
            <!--<value name="TO">-->
                <!--<shadow type="math_number">-->
                    <!--<field name="NUM">100</field>-->
                <!--</shadow>-->
            <!--</value>-->
        <!--</block>-->
        <!--<block type="math_random_float"/>-->
    <!--</category>-->
    <!--<category name="Списки" colour="#745CA6">-->
        <!--<block type="lists_create_with">-->
            <!--<mutation items="0"/>-->
        <!--</block>-->
        <!--<block type="lists_create_with">-->
            <!--<mutation items="3"/>-->
        <!--</block>-->
        <!--<block type="lists_repeat">-->
            <!--<value name="NUM">-->
                <!--<shadow type="math_number">-->
                    <!--<field name="NUM">5</field>-->
                <!--</shadow>-->
            <!--</value>-->
        <!--</block>-->
        <!--<block type="lists_length"/>-->
        <!--<block type="lists_isEmpty"/>-->
        <!--<block type="lists_indexOf">-->
            <!--<field name="END">FIRST</field>-->
            <!--<value name="VALUE">-->
                <!--<block type="variables_get">-->
                    <!--<field name="VAR">list</field>-->
                <!--</block>-->
            <!--</value>-->
        <!--</block>-->
        <!--<block type="lists_getIndex">-->
            <!--<mutation statement="false" at="true"/>-->
            <!--<field name="MODE">GET</field>-->
            <!--<field name="WHERE">FROM_START</field>-->
            <!--<value name="VALUE">-->
                <!--<block type="variables_get">-->
                    <!--<field name="VAR">list</field>-->
                <!--</block>-->
            <!--</value>-->
        <!--</block>-->
        <!--<block type="lists_setIndex">-->
            <!--<mutation at="true"/>-->
            <!--<field name="MODE">SET</field>-->
            <!--<field name="WHERE">FROM_START</field>-->
            <!--<value name="LIST">-->
                <!--<block type="variables_get">-->
                    <!--<field name="VAR">list</field>-->
                <!--</block>-->
            <!--</value>-->
        <!--</block>-->
        <!--<block type="lists_getSublist">-->
            <!--<mutation at1="true" at2="true"/>-->
            <!--<field name="WHERE1">FROM_START</field>-->
            <!--<field name="WHERE2">FROM_START</field>-->
            <!--<value name="LIST">-->
                <!--<block type="variables_get">-->
                    <!--<field name="VAR">list</field>-->
                <!--</block>-->
            <!--</value>-->
        <!--</block>-->
        <!--<block type="lists_split">-->
            <!--<mutation mode="SPLIT"/>-->
            <!--<field name="MODE">SPLIT</field>-->
            <!--<value name="DELIM">-->
                <!--<shadow type="text">-->
                    <!--<field name="TEXT">,</field>-->
                <!--</shadow>-->
            <!--</value>-->
        <!--</block>-->
        <!--<block type="lists_sort">-->
            <!--<field name="TYPE">NUMERIC</field>-->
            <!--<field name="DIRECTION">1</field>-->
        <!--</block>-->
    <!--</category>-->
    <!--<sep/>-->
    <!--<category name="Функции" colour="#9A5CA6" custom="PROCEDURE"/>-->
</xml>
`

module.exports = toolbox
