import {
  BATTERY_MODULE_TYPE, CHAINSAW_MODULE_TYPE, CHASSIS_MODULE_TYPE, DEFAULT_MODULE_TYPE,
  GRIPPER_MODULE_TYPE, MEMORY_MODULE_TYPE, STORAGE_MODULE_TYPE, TOOL_MODULE_TYPE, VISION_MODULE_TYPE,
} from '@/constants/moduleTypes'
import Battery from '@/parts/modules/battery'
import Chainsaw from '@/parts/modules/chainsaw'
import Chassis from '@/parts/modules/chassis'
import Gripper from '@/parts/modules/gripper'
import Memory from '@/parts/modules/memory'
import Module from '@/parts/modules/module'
import Storage from '@/parts/modules/storage'
import Tool from '@/parts/modules/tool'
import Vision from '@/parts/modules/vision'

const moduleClassByType = {
  [BATTERY_MODULE_TYPE]: Battery,
  [CHAINSAW_MODULE_TYPE]: Chainsaw,
  [CHASSIS_MODULE_TYPE]: Chassis,
  [GRIPPER_MODULE_TYPE]: Gripper,
  [MEMORY_MODULE_TYPE]: Memory,
  [DEFAULT_MODULE_TYPE]: Module,
  [STORAGE_MODULE_TYPE]: Storage,
  [TOOL_MODULE_TYPE]: Tool,
  [VISION_MODULE_TYPE]: Vision,
}

export default moduleClassByType
