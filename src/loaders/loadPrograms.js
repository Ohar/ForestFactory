import { LOAD_PROGRAMS } from '@/store/editor/action-types'
import Program from '@/parts/program'
import store from '@/store'

export default function loadPrograms (levelPrograms) {
  const programs = levelPrograms.map(e => new Program(e))

  return new Promise(
    resolve => {
      setTimeout(
        () => {
          store.dispatch({
            type: LOAD_PROGRAMS,
            programs,
          })

          resolve()
        },
        200,
      )
    },
  )
}
