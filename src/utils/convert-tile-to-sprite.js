import Pob from '@/sprites/pobs/pob'
import clearTile from '@/utils/clear-tile'
import getSpriteClassByTileIndex from '@/utils/get-sprite-class-by-tile-index'

function convertTileToSprite (tile, ffId, game) {
	const {key, frame} = getSpriteClassByTileIndex(tile.index),
	      options      = {
		      game,
		      x: tile.worldX,
		      y: tile.worldY,
		      key,
		      frame,
	      }

	const tileMap      = game.state.states.gameState.levelMap,
	      tileMapLayer = game.state.states.gameState.layers[tile.layer.name]

	// Заменить тайл на пустой
	clearTile(tile, tileMap, tileMapLayer)

	return new Pob(options, ffId)
}

export default convertTileToSprite
