import { PROGRAM_SET_VISIBILITY } from '@/store/editor/action-types'

export default function actionProgramShowAtList (programId) {
  return dispatch => {
    dispatch({
      type: PROGRAM_SET_VISIBILITY,
      showAtList: true,
      programId,
    })
  }
}
