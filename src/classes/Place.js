import 'p2'
import Phaser from 'phaser'
import 'pixi'

export default class Place extends Phaser.Rectangle {
  constructor ({game, x, y, width, height, name}) {
    super(x, y, width, height)

    this.name = name

    game.world.places.push(this)

  }
}
