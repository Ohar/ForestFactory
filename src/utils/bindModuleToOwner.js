import moduleClassByType from '@/constants/moduleClassByType'
import store from '@/store'
import { BIND_MODULE_TO_ROBOT } from '@/store/editor/action-types'
import getModuleById from '@/utils/getModuleById'

export default function bindModuleToOwner (owner) {
  return ({moduleId, moduleType}) => {
    if (moduleId) {
      store.dispatch({
        type    : BIND_MODULE_TO_ROBOT,
        ownerId : owner.ffId,
        moduleId: moduleId,
      })

      owner.modules[moduleType] = getModuleById(moduleId)
      owner.modules[moduleType].reconnectToOwner()
    } else if (moduleClassByType[moduleType]) {
      owner.modules[moduleType] = new (moduleClassByType[moduleType])({owner, id: moduleId})
    } else {
      console.error('Unknown module type', moduleType)
    }
  }
}
