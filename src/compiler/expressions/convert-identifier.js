import IRNode from '@/classes/ir-node'
import IR_COMMANDS_LIST from '@/constants/ir_commands_list'
import getFunctionName from '@/utils/get-function-name'
import uid from 'uid'

export default function convertIdentifier (node, program, loopLabels) {
  const functionName = getFunctionName(node.name),
        calleeId     = program.functions[functionName]

  let code = ''

  // Пока что без аргументов
  if (calleeId) {
    const labelAfterCall = uid(20)

    code = `
functionCall ${calleeId} ${labelAfterCall}
label ${labelAfterCall}
`
  } else if (IR_COMMANDS_LIST.find(e => e === node.name)) {
    // Print only Identifiers from command list
    code = node.name
  }

  return new IRNode({
    id: node.name,
    code,
  })
}
