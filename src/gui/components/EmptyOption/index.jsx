// Fix Firefox bug
// Prevent Firefox to show selected option when no option is selected in fact
// https://bugzilla.mozilla.org/show_bug.cgi?id=46845

import { noop } from 'lodash'
import React, { PropTypes } from 'react'
import EMPTY_OPTION from '@/constants/EMPTY_OPTION'
import './styles.less'

const EmptyOption = ({value}) => (
  <option
    className='EmptyOption'
    value={value}
  />
)

EmptyOption.propTypes = {
  value: PropTypes.string,
}

EmptyOption.defaultProps = {
  value: EMPTY_OPTION.value,
}

export default EmptyOption
