// IndexedDB: получить все записи

import dbConnectionPromise from '@/singletons/db/db-connection-promise'

export default function getAllItems (storeName) {
	return () => {
    return dbConnectionPromise
      .then(
        dbConnection => {
          const store = dbConnection.transaction(storeName).objectStore(storeName)

          return new Promise(
            resolve => {
              const result = []

              store.openCursor().onsuccess = event => {
                const cursor = event.target.result

                if (cursor) {
                  result.push(cursor.value)
                  cursor.continue()
                } else {
                  resolve(result)
                }
              }

              store.openCursor().onerror = event => {
                console.error('Fail getAllItems', event.target.errorCode)
                resolve([])
              }
            }
          )
        }
      )
	}
}
