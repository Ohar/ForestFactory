import Blockly from '@/blockly/blockly'
import { CHAINSAW_MODULE_TYPE } from '@/constants/moduleTypes'
import store from '@/store'
import { BLOCKLY_CHAINSAW_TEXT } from '@/l10n/keys'

const BJS = Blockly.JavaScript

const BlockSaw = {
	name      : 'FF_Saw',
	generator : block => {
		const obj = BJS.valueToCode(block, 'obj', BJS.ORDER_ATOMIC)

		return `${CHAINSAW_MODULE_TYPE}.saw(${obj})\n`
	},
	definition: {
		init () {
      const {optionsState: {l10n}} = store.getState()

      this.appendValueInput('obj')
				.setCheck('Sprite')
				.appendField(l10n[BLOCKLY_CHAINSAW_TEXT]);
			this.setInputsInline(true);
			this.setPreviousStatement(true, null);
			this.setNextStatement(true, null);
			this.setColour(250);
		}
	}
}

export default BlockSaw
