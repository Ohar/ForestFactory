import hideStartLevelScreenListener from '@/store/gui/listeners/hideStartLevelScreenListener'
import actionStartLevelScreenShow from '@/store/gui/actions/start-level-screen-show'
import store from '@/store'
import loadLevel from '@/loaders/loadLevel'
import dbConnectionPromise from '@/singletons/db/db-connection-promise'
import Phaser from 'phaser'

export default class BootState extends Phaser.State {
  constructor (...args) {
    super(...args)

    this.nextState                             = this.nextState.bind(this)
    this.unSubScriber_StartLevelScreenListener = null
  }

  init () {
    this.stage.backgroundColor = '#fff'
    this.ready                 = false
  }

  preload () {
    dbConnectionPromise
      .then(() => loadLevel(this.game))
      .then(
        loadedLevel => {
          // Show Start Level Screen
          store.dispatch(actionStartLevelScreenShow(loadedLevel))

          // Subscribe to Start Level Screen hiding
          this.unSubScriber_StartLevelScreenListener = store.subscribe(
            hideStartLevelScreenListener(this.nextState(loadedLevel), store.getState().guiState),
          )
        },
      )
      .catch(
        err => {
          console.error('Fail on loading', err)
        },
      )
  }

  nextState (loadedLevel) {
    return () => {
      this.game.currentLevel = loadedLevel
      this.ready             = true

      if (this.unSubScriber_StartLevelScreenListener) {
        this.unSubScriber_StartLevelScreenListener()
        this.unSubScriber_StartLevelScreenListener = null
      }
    }
  }

  render () {
    if (this.ready) {
      this.state.start('gameState')
    }
  }
}
