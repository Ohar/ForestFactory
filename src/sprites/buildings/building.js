import FFSprite from '@/classes/ff-sprite'

export default class Building extends FFSprite {
	constructor ({game, x, y, key, frame, ffId}) {
		super({game, x, y, key, frame}, ffId)

		this.game = game
		this.game.world.getByName('buildings').add(this)
	}
}
