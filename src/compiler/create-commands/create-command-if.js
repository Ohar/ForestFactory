import getLabelName from '@/utils/get-label-name'
import convertMemoryValToBoolean from '@/utils/convert-memory-val-to-boolean'
import getRobotById from '@/utils/getRobotById'

function createCommandIf (program, params) {
	const condition   = params[0],
	      labelNumber = params[1],
	      isNegative  = condition[0] === '!',
	      memoryName  = condition.replace(/^!/, '')

	return () => {
    const robot        = getRobotById(program.robotId)
		const memoryVal    = convertMemoryValToBoolean(robot.modules.memory.load(memoryName)),
		      conditionVal = isNegative ? !memoryVal : memoryVal

		if (conditionVal) {
			const labelName = getLabelName(labelNumber),
			      nextStage = program.labels[labelName]

			program.stage = nextStage
			return program.run()
		} else {
			return true;
		}
	}
}

export default createCommandIf
