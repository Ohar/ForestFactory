import { SET_MAIN_MENU_VISIBILITY } from '@/store/gui/action-types'

export default function actionMainMenuShow () {
  return dispatch => {
    dispatch({
      type           : SET_MAIN_MENU_VISIBILITY,
      mainMenuVisible: true,
    })
  }
}
