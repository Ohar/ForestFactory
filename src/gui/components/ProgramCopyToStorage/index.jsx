import signalActivateRepositoryTab from '@/store/tab/actions/activate-repository-tab'
import generateEditorBtnTitle from '@/utils/generateEditorBtnTitle'
import getRobotProgram from '@/utils/getRobotProgram'
import { noop } from 'lodash'
import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  PROGRAM_COPY_TO_STORAGE_BTN_TEXT,
  PROGRAM_COPY_TO_STORAGE_NEW_NAME,
  PROGRAM_COPY_TO_STORAGE_TEMPLATE_COMMON,
  PROGRAM_COPY_TO_STORAGE_TEMPLATE_DISABLED,
} from '@/l10n/keys'

class ProgramCopyToStorage extends Component {
  constructor (...args) {
    super(...args)

    this.onClick = this.onClick.bind(this)
  }

  onClick () {
    const {l10n, robot, activateRepository} = this.props
    const program = getRobotProgram(robot)

    program.createCopy(l10n[PROGRAM_COPY_TO_STORAGE_NEW_NAME](program.name))
    activateRepository()
  }

  render () {
    const {l10n, isRepositoryEnabled, robot} = this.props

    if (isRepositoryEnabled) {
      const program = getRobotProgram(robot)

      const title = generateEditorBtnTitle({
        program,
        robot,
        isDisabled      : !program,
        templateDisabled: l10n[PROGRAM_COPY_TO_STORAGE_TEMPLATE_DISABLED],
        templateCommon  : l10n[PROGRAM_COPY_TO_STORAGE_TEMPLATE_COMMON],
      })

      return (
        <button
          onClick={this.onClick}
          title={title}
          disabled={!program}
        >
          {l10n[PROGRAM_COPY_TO_STORAGE_BTN_TEXT]}
        </button>
      )
    } else {
      return null
    }
  }
}

function mapStateToProps (state, ownProps) {
  return {
    ...ownProps,
    robot              : state.editorState.robot,
    robotsPrograms     : state.editorState.robotsPrograms,
    isRepositoryEnabled: state.guiState.isRepositoryEnabled,
    l10n               : state.optionsState.l10n,
  }
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators({
    activateRepository: signalActivateRepositoryTab,
  }, dispatch)
}

ProgramCopyToStorage.propTypes = {
  programShow   : PropTypes.func.isRequired,
  robot         : PropTypes.object,
  robotsPrograms: PropTypes.array.isRequired,
}

ProgramCopyToStorage.defaultProps = {
  programShow   : noop,
  robot         : {},
  robotsPrograms: [],
}

export default connect(mapStateToProps, mapDispatchToProps)(ProgramCopyToStorage)
