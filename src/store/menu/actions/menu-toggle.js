import {MENU_TOGGLE} from '@/store/menu/action-types'

export default function actionMenuToggle () {
	return dispatch => {
		dispatch({type: MENU_TOGGLE})
	}
}
