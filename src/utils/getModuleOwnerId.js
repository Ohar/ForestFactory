import store from '@/store'

export default function getModuleOwnerId (moduleIdToSearch) {
  const {editorState: {ownerModules}} = store.getState()
  const ownerToModule = ownerModules.find(({moduleId}) => moduleId === moduleIdToSearch)

  return ownerToModule
         ? ownerToModule.ownerId
         : null
}
