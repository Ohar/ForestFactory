import {SET_TAB_LIST_FULLSCREEN} from '@/store/tab/action-types'

export default function signalTabListFullScreenOff () {
	return dispatch => {
		dispatch(
			{
				type      : SET_TAB_LIST_FULLSCREEN,
				fullScreen: false,
			}
		)
	}
}
