import { SET_TAB_LIST_VISIBILITY } from '@/store/tab/action-types'

export default function signalTabListHide () {
  return dispatch => {
    dispatch(
      {
        type: SET_TAB_LIST_VISIBILITY,
        tabListVisibility: false,
      },
    )
  }
}
