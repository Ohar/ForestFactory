import store from '@/store'
import getProgramId from '@/utils/getProgramId'
import getProgramById from '@/utils/getProgramById'

export default function getRobotProgram (robot) {
  const {editorState: {robotsPrograms}} = store.getState()
  const programId = getProgramId(robotsPrograms, robot)

  return getProgramById(programId)
}
