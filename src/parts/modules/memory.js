import { MEMORY_MODULE_TYPE } from '@/constants/moduleTypes'
import Module from '@/parts/modules/module'

const DEFAULT_SIZE = 1000000

export default class Memory extends Module {
  constructor ({owner, ownerId, id, size = DEFAULT_SIZE, storage = {}, targetToMove = null}) {
    super({id, moduleType: MEMORY_MODULE_TYPE, owner, ownerId})

    this._storage   = storage
    this.size       = size
    this.targetToMove = targetToMove
  }

  save (key, val) {
    const stringified          = JSON.stringify(val)
    this._storage[String(key)] = stringified
    this.size -= stringified.length
  }

  load (key) {
    if (this.has(key)) {
      try {
        return this.load(JSON.parse(this._storage[String(key)]))
      }
      catch (err) {
        throw new Error('Memory load error', err)
      }
    } else {
      return key
    }
  }

  has (key) {
    return String(key) in this._storage
  }

  delete (key) {
    if (this.has(key)) {
      const val = this._storage[String(key)]
      this.size += val.length
      delete this._storage[String(key)]
      return true
    } else {
      return false
    }
  }

  clear () {
    this._storage = {}
  }
}
