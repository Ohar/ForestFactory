const EDITOR_DEFAULT_STATE = {
  robot             : null,
  blockId           : null,
  ir                : '',
  toolbox           : '',
  programs          : [],
  selectedModuleId  : null,
  selectedProgramId : null,
  robotList         : [],
  moduleList        : [],
  robotsPrograms    : [],
  ownerModules      : [],
  levelSavedData    : null,
  levelLoadedData   : null,
  isWorkspaceVisible: false,
}

export default EDITOR_DEFAULT_STATE
