import actionMainMenuShow from '@/store/gui/actions/main-menu-show'
import signalLoadGame from '@/store/menu/signals/game-load'
import classNames from 'classnames'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import getLocalizedString from '@/l10n/getLocalizedString'
import {
  WINSCREEN_VICTORY_BTN_NEXT,
  WINSCREEN_VICTORY_BTN_TO_MAIN_MANU,
  WINSCREEN_VICTORY_HEADER,
  WINSCREEN_VICTORY_TEXT,
} from '@/l10n/keys'
import './styles.less'

class WinScreen extends Component {
  constructor (...args) {
    super(...args)

    this.loadNextLevel = this.loadNextLevel.bind(this)
  }

  loadNextLevel () {
    const {loadLevel, nextLvlId} = this.props

    loadLevel(nextLvlId)
  }

  render () {
    const {l10n, levelName, nextLvlId, showMainMenu, visible} = this.props
    const localizedLevelName = getLocalizedString(levelName)

    return (
      <section className={classNames('WinScreen', visible ? '' : 'WinScreen-hidden')}>
        <header className='WinScreen_header'>
          {l10n[WINSCREEN_VICTORY_HEADER]}
        </header>
        <p className='WinScreen_text'>
          {l10n[WINSCREEN_VICTORY_TEXT](localizedLevelName)}
        </p>
        {
          nextLvlId
          ? (
            <button
              className='WinScreen_nextBtn'
              disabled={!visible}
              onClick={this.loadNextLevel}
            >
              {l10n[WINSCREEN_VICTORY_BTN_NEXT]}
            </button>
          )
          : (
            <button
              className='WinScreen_nextBtn'
              disabled={!visible}
              onClick={showMainMenu}
            >
              {l10n[WINSCREEN_VICTORY_BTN_TO_MAIN_MANU]}
            </button>
          )
        }

      </section>
    )
  }
}

function mapStateToProps (state, ownProps) {
  return {
    ...ownProps,
    visible  : state.guiState.winScreenVisible,
    nextLvlId: state.guiState.nextLvlId,
    levelName: state.guiState.levelName,
    l10n     : state.optionsState.l10n,
  }
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators(
    {
      loadLevel   : signalLoadGame,
      showMainMenu: actionMainMenuShow,
    },
    dispatch,
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(WinScreen)
