import {
  BATTERY_MODULE_TYPE,
  CHAINSAW_MODULE_TYPE,
  CHASSIS_MODULE_TYPE,
  GRIPPER_MODULE_TYPE,
  MEMORY_MODULE_TYPE,
  STORAGE_MODULE_TYPE,
  VISION_MODULE_TYPE,
} from '@/constants/moduleTypes'
import { PROGRAM_DELETE, ROBOT_ADD, SET_EDITOR_ROBOT } from '@/store/editor/action-types'
import signalSetProgramToRobot from '@/store/editor/signals/set-program-to-robot'
import store from '@/store'
import ActiveMobCircle from '@/sprites/ActiveMobCircle'
import Mob from '@/sprites/mobs/mob'
import bindModuleToOwner from '@/utils/bindModuleToOwner'
import { ROBOT_DEFAULT_NAME } from '@/l10n/keys'

export default class Robot extends Mob {
	constructor (
		{
			game, x, y, programId = null, ffId, batteryId, chainsawId, chassisId, gripperId, memoryId, storageId, visionId,
			questDoneString, name
		}
	) {
		super(game, x, y, 'robot', ffId, questDoneString)

    this.updateQuest = this.updateQuest.bind(this)

		this.game.world
			.getByName('robots')
			.add(this)

    const {optionsState: {l10n}} = store.getState()

    this.inputEnabled = true
    this.modules      = {}
    this.name         = name || `${l10n[ROBOT_DEFAULT_NAME]} ${game.robotIdCounter++}`
    this.programId    = programId
    this.program      = null
    this.inFocus      = false
    this.activeCircle = new ActiveMobCircle(this)

    store.dispatch({
      type : ROBOT_ADD,
      robot: this,
    })

    this.animations.add('look_up',    [0], 1, true)
    this.animations.add('look_left',  [2], 1, true)
    this.animations.add('look_down',  [1], 1, true)
    this.animations.add('look_right', [3], 1, true)
    this.animations.add('idle',       [1], 1, true)

    this.onHover        = this.onHover.bind(this)
    this.onBlur         = this.onBlur.bind(this)
    this.getPlayerFocus = this.getPlayerFocus.bind(this)

    this.events.onInputUp.add(this.getPlayerFocus)
    this.events.onInputOver.add(this.onHover)
    this.events.onInputOut.add(this.onBlur)

    if (this.programId) {
      store.dispatch(signalSetProgramToRobot(this, this.programId, false))
    }

    const bindModules = bindModuleToOwner(this) // TODO: Модули нужно не только ставить, но и снимать
    const moduleList = [
      {
        moduleId: batteryId,
        moduleType: BATTERY_MODULE_TYPE,
      },
      {
        moduleId: chainsawId,
        moduleType: CHAINSAW_MODULE_TYPE,
      },
      {
        moduleId: chassisId,
        moduleType: CHASSIS_MODULE_TYPE,
      },
      {
        moduleId: gripperId,
        moduleType: GRIPPER_MODULE_TYPE,
      },
      {
        moduleId: memoryId,
        moduleType: MEMORY_MODULE_TYPE,
      },
      {
        moduleId: storageId,
        moduleType: STORAGE_MODULE_TYPE,
      },
      {
        moduleId: visionId,
        moduleType: VISION_MODULE_TYPE,
      },
    ]

    moduleList.forEach(bindModules)
	}

	onHover () {
    this.activeCircle.show()

    if (this.modules.vision) {
      this.modules.vision.showFov()
    }
	}

	onBlur () {
    if (!this.inFocus) {
      this.activeCircle.hide()
    }
    if (this.modules.vision) {
      this.modules.vision.hideFov()
    }
	}

	getPlayerFocus () {
    this.game.world
      .children
      .find(
      	({name}) => name === 'robots'
      )
      .children.forEach(
	      robot => robot.activeCircle.hide()
	    )

		this.game.camera.focusOn(this)
		this.activeCircle.show()
    this.inFocus = true

    store.dispatch({
      type : SET_EDITOR_ROBOT,
      robot: this,
    })
	}

	setProgram (program) {
		if (program) {
			if (this.program) {
				this.program.stop()

				if (!this.program.showAtList) {
					store.dispatch({
						type     : PROGRAM_DELETE,
						programId: this.program.id,
					})
				}
			}

      this.program   = program
      this.programId = this.program.id

      this.program.init(this)
		}
	}

	update () {
		if (this.program) {
			this.program.run()
		}

		this.updateAnimation()
    this.updateQuest()
	}
}
