const IR_COMMANDS_LIST = [
  'chassis.moveByXY',
  'chassis.moveToObj',
  'chassis.moveToXY',
  'functionCall',
  'functionEnd',
  'goto',
  'gripper.grab',
  'gripper.putInto',
  'gripper.release',
  'chainsaw.saw',
  'highlightBlock',
  'label',
  'if',
  'memory.save',
  'vision.seek',
  'vision.seekNearest',
]

export default IR_COMMANDS_LIST
