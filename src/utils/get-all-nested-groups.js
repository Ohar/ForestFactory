import Phaser from 'phaser'

function getAllNestedGroups (parentGroup) {
	return parentGroup instanceof Phaser.Group
		? [parentGroup, ...parentGroup.children.map(getAllNestedGroups)]
		: []
}

export default getAllNestedGroups
