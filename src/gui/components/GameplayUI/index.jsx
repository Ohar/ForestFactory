import TabList from '@/gui/components/TabList'
import React from 'react';

import QuestInfo from '@/gui/components/QuestInfo'
import RobotPanel from '@/gui/components/RobotPanel';
import GameMenu from '@/gui/components/GameMenu';
import { connect } from 'react-redux'
import classNames from 'classnames'
import './styles.less';

function GameplayUI ({visible}) {
	return (
		<section className={classNames('GameplayUI', visible ? '' : 'GameplayUI-hidden')}>
      <TabList/>
      <QuestInfo/>
			<RobotPanel/>
      <GameMenu/>
		</section>
	)
}

function mapStateToProps (state, ownProps) {
  return {
    ...ownProps,
    visible: state.guiState.gameplayUIVisible,
  }
}

export default connect(mapStateToProps)(GameplayUI)
