import IRNode from '@/classes/ir-node'
import convertAstNodeToIR from '@/compiler/converters/convert-ast-node-to-ir'
import uid from 'uid'

export default function convertIfStatement (node, program, loopLabels) {
	const condition  = convertAstNodeToIR(node.test, program, loopLabels),
		  consequent = convertAstNodeToIR(node.consequent, program, loopLabels),
		  alternate  = convertAstNodeToIR(node.alternate, program, loopLabels)

	const labelTrue   = uid(20),
		  labelFalse  = uid(20),
		  labelFinish = uid(20)

	const code = node.alternate
	  ? `
${condition.code}
if ${condition.id} ${labelTrue}
if !${condition.id} ${labelFalse}
label ${labelTrue}
${consequent.code}
goto ${labelFinish}
label ${labelFalse}
${alternate.code}
goto ${labelFinish}
label ${labelFinish}
`
	  : `
${condition.code}
if ${condition.id} ${labelTrue}
goto ${labelFinish}
label ${labelTrue}
${consequent.code}
label ${labelFinish}
`

	return new IRNode({
		id  : null,
		code,
	})
}
