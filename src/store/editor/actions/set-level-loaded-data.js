import { SET_LEVEL_LOADED_DATA } from '@/store/editor/action-types'

export default function actionSetLevelLoadedData (levelLoadedData) {
  return dispatch => {
    dispatch(
      {
        type: SET_LEVEL_LOADED_DATA,
        levelLoadedData,
      },
    )
  }
}
