import IRNode from '@/classes/ir-node'
import convertAstNodeToIR from '@/compiler/converters/convert-ast-node-to-ir'
import uid from 'uid'

export default function convertForStatement (node, program, upperLoopLabels) {

	const labelStart = uid(20),
		  labelBody  = uid(20),
		  labelEnd   = uid(20)

	const loopLabels = {
		nextIteration: labelBody,
		endLoop      : labelEnd,
	}
	const init   = convertAstNodeToIR(node.init, program, upperLoopLabels),
	      test   = convertAstNodeToIR(node.test, program, upperLoopLabels),
	      update = convertAstNodeToIR(node.update, program, upperLoopLabels),
	      body   = convertAstNodeToIR(node.body, program, loopLabels)

	return new IRNode({
		id  : null,
		code: `
${init.code}
${test.code}
label ${labelStart}
if ${test.id} ${labelBody}
if !${test.id} ${labelEnd}
label ${labelBody}
${body.code}
${update.code}
goto ${labelStart}
label ${labelEnd}
`,
	})
}
