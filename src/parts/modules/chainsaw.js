import { CHAINSAW_MODULE_TYPE } from '@/constants/moduleTypes'
import Tool from '@/parts/modules/tool'

const SAW_RANGE = 10

export default class Chainsaw extends Tool {
  constructor ({id, owner, ownerId}) {
    super({
      id,
      moduleType: CHAINSAW_MODULE_TYPE,
      range: SAW_RANGE,
      owner,
      ownerId,
    })
  }

  saw (ffId) {
    const target = this.getTargetByFFId(ffId)

    let result   = false,
        finished = false

    if (target) {
      if (target.health) {
        target.damage(1)
      }

      if (!target.health) {
        finished = true
      }

      result = true
    } else {
      result = false
    }

    return {
      finished,
      result,
      resultForSave: result,
    }
  }
}
