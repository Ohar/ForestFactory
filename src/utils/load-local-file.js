import fs from 'mz/fs';

export default function loadLocalFile (path, fileType = 'text') {
	return __WEB__
		? fetch(path)
			.then(res => res[fileType]())
		: fs
			.readFile(
				// Исправление относительных путей в электроне
				(__PACKAGE__ ? './resources/app/' : '') +
				(__PACKAGE__ ? path.replace('./', '') : path)
			)
			.then(
				buffer => {
					const text = new Buffer(buffer).toString()

					switch (fileType) {
						case 'text':
							return text

						case 'json':
							return JSON.parse(text)
					}
				}
			)
}
