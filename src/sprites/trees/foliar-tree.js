import BaseTree from '@/sprites/trees/base-tree';

export default class FoliarTree extends BaseTree {
	constructor ({game, x, y, key, frame, ffId}) {
		const treetopFrame = Math.random() > .5 ? 0 : 1
		super({game, x, y, key, frame, ffId}, treetopFrame)
	}
}
