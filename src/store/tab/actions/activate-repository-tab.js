import { SET_ACTIVE_TAB } from '@/store/tab/action-types'
import tabIdList from '@/constants/tabIdList'

export default function signalActivateRepositoryTab () {
  return dispatch => {
    dispatch(
      {
        type: SET_ACTIVE_TAB,
        activeTabId: tabIdList.repository,
      },
    )
  }
}
