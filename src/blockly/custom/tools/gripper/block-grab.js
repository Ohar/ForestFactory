import Blockly from '@/blockly/blockly'
import { GRIPPER_MODULE_TYPE } from '@/constants/moduleTypes'
import store from '@/store'
import { BLOCKLY_GRAB_TEXT } from '@/l10n/keys'

const BJS = Blockly.JavaScript

const BlockGrab = {
	name      : 'FF_Grab',
	generator : block => {
		const obj = BJS.valueToCode(block, 'obj', BJS.ORDER_ATOMIC)

		return `${GRIPPER_MODULE_TYPE}.grab(${obj})\n`
	},
	definition: {
		init () {
      const {optionsState: {l10n}} = store.getState()

      this.appendValueInput('obj')
				.setCheck('Sprite')
				.appendField(l10n[BLOCKLY_GRAB_TEXT]);
			this.setInputsInline(true);
			this.setPreviousStatement(true, null);
			this.setNextStatement(true, null);
			this.setColour(250);
		}
	}
}

export default BlockGrab
