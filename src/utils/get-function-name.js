import FUNCTION_NAME_PREFIX from '@/constants/function_name_prefix'

export default function getFunctionName (nodeName) {
	return `${FUNCTION_NAME_PREFIX}${nodeName}`
}
