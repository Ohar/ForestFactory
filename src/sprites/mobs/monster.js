import Mob from '@/sprites/mobs/mob'
import rnd from '@/utils/rnd'

class Monster extends Mob {
	constructor (game, x, y, key) {
		super(game, x, y, key)

		this.animations.add('look_up',    [0,  1,  2], 6, true)
		this.animations.add('look_left',  [3,  4,  5], 6, true)
		this.animations.add('look_down',  [6,  7,  8], 6, true)
		this.animations.add('look_right', [9, 10, 11], 6, true)
		this.animations.add('idle',       [6,  7,  8], 1, true)
		
		this.randomizeVelocity()
	}
	
	randomizeVelocity () {
		let velocity = {
			x: rnd.integerInRange(-50, 50),
			y: rnd.integerInRange(-50, 50),
		}
		
		this.body.velocity.setTo(velocity.x, velocity.y)
	}
	
	update () {
		if (Math.random() > .99) {
			this.randomizeVelocity()
		}
		
		this.updateAnimation()
	}
	
}

export default Monster
