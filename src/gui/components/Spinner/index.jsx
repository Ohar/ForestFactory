import React from 'react'
import classNames from 'classnames'
import './styles.less'

function Spinner ({visible}) {
  return (
    <span className={classNames('Spinner', {
      'Spinner-visible': visible,
    })}>☸</span>
  )
}

Spinner.defaultProps = {
  visible: true,
}

export default  Spinner
