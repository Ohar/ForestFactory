export default function sortByXY (a, b) {
  if (a.y > b.y) {
    return 1
  } else if (a.y < b.y) {
    return -1
  } else if (a.x > b.x) {
    return 1
  } else if (a.x < b.x) {
    return -1
  } else {
    return 0
  }
}
