import { SHOW_START_LEVEL_SCREEN } from '@/store/gui/action-types'

export default function actionStartLevelScreenShow ({name: levelName, nextLvlId}) {
  return dispatch => {
    dispatch({
      levelName,
      nextLvlId,
      type: SHOW_START_LEVEL_SCREEN,
    })
  }
}
