import actionOptionsShow from '@/store/gui/actions/options-show'
import { last, sortBy } from 'lodash'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import actionLoadGameMenuShow from '@/store/gui/actions/load-game-menu-show'
import actionMainMenuHide  from '@/store/gui/actions/main-menu-hide'
import signalLoadGame from '@/store/menu/signals/game-load'
import Spinner from '@/gui/components/Spinner'
import Version from '@/gui/components/Version'
import dbApi from '@/singletons/db/api'
import {
  MAIN_MENU_EXIT_TEXT,
  MAIN_MENU_LOAD_GAME_TEXT,
  MAIN_MENU_LOAD_GAME_TITLE,
  MAIN_MENU_LOAD_LAST_SAVE, MAIN_MENU_LOGO_ALT, MAIN_MENU_OPTIONS_TEXT, MAIN_MENU_OPTIONS_TITLE,
  MAIN_MENU_START_NEW_GAME_TEXT,
  MAIN_MENU_START_NEW_GAME_TITLE, MAIN_MENU_TITLE_TEXT, NOT_READY_YET,
} from '@/l10n/keys'
import robotFront from 'root/resources/images/robot/original/front.js'
import './styles.less'

class MainMenu extends Component {
  constructor (...args) {
    super(...args)

    this.state = {
      savedGameList: [],
    }

    this.startNewGame     = this.startNewGame.bind(this)
    this.resumeGame       = this.resumeGame.bind(this)
    this.getSavedGameList = this.getSavedGameList.bind(this)
  }

  startNewGame () {
    this.props.loadGame()
  }

  resumeGame () {
    const {savedGameList} = this.state
    const lastSavedGame   = last(sortBy(savedGameList, 'date'))

    this.props.loadGame(lastSavedGame.id)
  }

  componentWillMount () {
    this.getSavedGameList()

    if (__DEV__) {
      this.startNewGame()
    }
  }

  componentWillReceiveProps ({isGameDeleting, isGameSaving}) {
    if (
      !isGameDeleting && isGameDeleting !== this.props.isGameDeleting
      || !isGameSaving && isGameSaving !== this.props.isGameSaving
    ) {
      this.getSavedGameList()
    }
  }

  getSavedGameList () {
    dbApi
      .getAllLevel()
      .then(
        savedGameList => {
          this.setState({savedGameList})
        },
      )
  }

  renderElements () {
    const {isGameLoading, l10n, showLoadGameMenu, showOptions} = this.props
    const {savedGameList}                                      = this.state

    return (
      <section className='MainMenu'>
        <header className='MainMenu_title'>
          {l10n[MAIN_MENU_TITLE_TEXT]}
          <Version/>
        </header>
        <img
          className='MainMenu_logo'
          src={robotFront}
          alt={l10n[MAIN_MENU_LOGO_ALT]}
        />
        <ul className='MainMenu_list'>
          {
            savedGameList.length
            ? (
              <li className='MainMenu_item'>
                <button
                  className='MainMenu_button'
                  onClick={this.resumeGame}
                  title={l10n[MAIN_MENU_LOAD_LAST_SAVE]}
                >
                  {l10n[MAIN_MENU_LOAD_LAST_SAVE]}
                </button>
              </li>
            )
            : null
          }

          <li className='MainMenu_item'>
            <button
              className='MainMenu_button'
              onClick={this.startNewGame}
              title={l10n[MAIN_MENU_START_NEW_GAME_TITLE]}
            >
              {l10n[MAIN_MENU_START_NEW_GAME_TEXT]}
            </button>
          </li>
          <li className='MainMenu_item'>
            <button
              className='MainMenu_button'
              disabled={isGameLoading || !savedGameList.length}
              onClick={showLoadGameMenu}
              title={l10n[MAIN_MENU_LOAD_GAME_TITLE]}
            >
              <Spinner visible={isGameLoading}/>
              {l10n[MAIN_MENU_LOAD_GAME_TEXT]}
            </button>
          </li>
          <li className='MainMenu_item'>
            <button
              className='MainMenu_button'
              title={l10n[MAIN_MENU_OPTIONS_TITLE]}
              onClick={showOptions}
            >
              {l10n[MAIN_MENU_OPTIONS_TEXT]}
            </button>
          </li>
          {
            __WEB__
            ? null
            : (
              <li className='MainMenu_item'>
                <button
                  className='MainMenu_button'
                  title={l10n[NOT_READY_YET]}
                  disabled={true}
                >
                  {l10n[MAIN_MENU_EXIT_TEXT]}
                </button>
              </li>
            )
          }
        </ul>
      </section>
    )
  }

  render () {
    const {visible} = this.props

    return visible
           ? this.renderElements()
           : null
  }
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators(
    {
      hide            : actionMainMenuHide ,
      showLoadGameMenu: actionLoadGameMenuShow,
      loadGame        : signalLoadGame,
      showOptions     : actionOptionsShow,
    },
    dispatch,
  )
}

function mapStateToProps (state, ownProps) {
  return {
    ...ownProps,
    visible       : state.guiState.mainMenuVisible,
    isGameLoading : state.menuState.isGameLoading,
    isGameDeleting: state.menuState.isGameDeleting,
    isGameSaving: state.menuState.isGameSaving,
    l10n           : state.optionsState.l10n,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MainMenu)
