import FFSprite from '@/classes/ff-sprite'

class Mob extends FFSprite {
	constructor (game, x, y, key, ffId, questDoneString) {
		const frame = 2

		super({game, x, y, key, frame, questDoneString}, ffId)

    this.updateQuest = this.updateQuest.bind(this)

		this.game = game

		this.animations.add('look_up',    [0], 1, true)
		this.animations.add('look_left',  [0], 1, true)
		this.animations.add('look_down',  [0], 1, true)
		this.animations.add('look_right', [0], 1, true)
		this.animations.add('idle',       [0], 1, true)

		this.animations.play('idle')
	}

	update () {
		this.updateAnimation()
    this.updateQuest()
  }

	updateAnimation () {
		let abs = {
			x: Math.abs(this.body.velocity.x),
			y: Math.abs(this.body.velocity.y),
		}

		if (abs.x > abs.y) {
			if (this.body.velocity.x > 0) {
				this.animations.play('look_right')
			} else if (this.body.velocity.x < 0) {
				this.animations.play('look_left')
			}
		} else if (abs.x < abs.y) {
			if (this.body.velocity.y > 0) {
				this.animations.play('look_down')
			} else if (this.body.velocity.y < 0) {
				this.animations.play('look_up')
			}
		} else  {
			this.animations.play('idle')
		}
	}

}

export default Mob
