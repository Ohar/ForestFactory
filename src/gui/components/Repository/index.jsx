import EmptyOption from '@/gui/components/EmptyOption'
import ProgramCopyBtn from '@/gui/components/ProgramCopyBtn'
import ProgramDeleteBtn from '@/gui/components/ProgramDeleteBtn'
import ProgramLoadBtn from '@/gui/components/ProgramLoadBtn'
import ProgramRenameBtn from '@/gui/components/ProgramRenameBtn'
import actionProgramSetSelected from '@/store/editor/actions/program-set-selected'
import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { REPOSITORY_EMPTY, REPOSITORY_HEADER } from '@/l10n/keys'
import './styles.less'

const MULTIPLE_ROWS_COUNT = 7

function Repository (
  {
    isProgramCopyBtnEnabled,
    isProgramDeleteBtnEnabled,
    isProgramRenameBtnEnabled,
    l10n,
    programs,
    selectProgram,
    selectedProgramId,
  }
    ) {
	const programsToShow = programs.filter(e => e.showAtList)

	return <section className='Repository'>
    <header className='Repository_header'>
      {l10n[REPOSITORY_HEADER]}
    </header>
    <section className='Repository_content'>
      <select
        className='Repository_select'
        onChange={e => selectProgram(e.target.value)}
        value={selectedProgramId || ''}
        size={MULTIPLE_ROWS_COUNT}
      >
        <EmptyOption/>
        {
          programsToShow.length
          ? programsToShow.map(
            ({id, name}) => (
              <option
                className='Repository_option'
                key={id}
                value={id}
              >
                {name}
              </option>
            )
          )
          : <option disabled>{l10n[REPOSITORY_EMPTY]}</option>
        }
      </select>
      <div className='Repository_controlList'>
        {isProgramDeleteBtnEnabled && <ProgramDeleteBtn/>}
        <ProgramLoadBtn/>
        {isProgramRenameBtnEnabled && <ProgramRenameBtn/>}
        {isProgramCopyBtnEnabled && <ProgramCopyBtn/>}
      </div>
    </section>
  </section>
}

function mapStateToProps (state, ownProps) {
  return {
    ...ownProps,
    programs                 : state.editorState.programs,
    selectedProgramId        : state.editorState.selectedProgramId,
    isProgramCopyBtnEnabled  : state.guiState.isProgramCopyBtnEnabled,
    isProgramDeleteBtnEnabled: state.guiState.isProgramDeleteBtnEnabled,
    isProgramRenameBtnEnabled: state.guiState.isProgramRenameBtnEnabled,
    l10n                     : state.optionsState.l10n,
  }
}

function mapDispatchToProps (dispatch) {
	return bindActionCreators({selectProgram: actionProgramSetSelected}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Repository)
