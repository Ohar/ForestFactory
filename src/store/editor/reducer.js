import { SET_SELECTED_MODULE } from '@/store/editor/action-types'
import * as actionTypes from '@/store/editor/action-types'
import DEFAULT_STATE from '@/store/editor/default_state'
import Program from '@/parts/program'

export default function editorReducer (state = DEFAULT_STATE, action) {
	switch (action.type) {
		case actionTypes.SET_EDITOR_ROBOT:
			return {
				...state,
				robot: action.robot,
			}

		case actionTypes.SET_EDITOR_LISTING_IR:
			return {
				...state,
				ir: action.ir,
			}

		case actionTypes.EDITOR_HIGHLIGHT_COMMAND:
			return {
				...state,
				blockId: action.blockId,
			}

		case actionTypes.LOAD_PROGRAMS:
			return {
				...state,
				programs: action.programs,
			}

		case actionTypes.PROGRAM_ADD:
			return {
				...state,
				programs: [
					...state.programs,
					action.program
				],
			}

		case actionTypes.PROGRAM_DELETE:
			return {
				...state,
        selectedProgramId: DEFAULT_STATE.selectedProgramId,
				programs: state.programs.filter(
					e => e.id !== action.programId
				),
        robotsPrograms: state.robotsPrograms.filter(
          e => e.programId !== action.programId
        ),
			}

		case actionTypes.PROGRAM_RENAME:
			return {
				...state,
				programs: state.programs.map(
					program => program.id === action.programId
						? new Program({
              ...program,
              name: action.name,
            }, false)
						: program
				),
			}

		case actionTypes.PROGRAM_SET_VISIBILITY:
			return {
				...state,
				programs: state.programs.map(
					program => program.id === action.programId
						? new Program({
              ...program,
              showAtList: action.showAtList,
            }, false)
						: program
				),
			}

    case actionTypes.SET_SELECTED_MODULE:
      return {
        ...state,
        selectedModuleId: action.selectedModuleId,
      }

		case actionTypes.SET_SELECTED_PROGRAM:
			return {
				...state,
				selectedProgramId: action.selectedProgramId,
			}

		case actionTypes.UPDATE_WORKSPACE_VISIBILITY:
			return {
				...state,
        isWorkspaceVisible: action.isWorkspaceVisible,
			}

		case actionTypes.BIND_PROGRAM_TO_ROBOT:
      return {
        ...state,
        robotsPrograms: [
	        ...state.robotsPrograms.filter(e => e.robotId !== action.robotId),
          {
            robotId  : action.robotId,
            programId: action.programId,
          }
        ]
      }

    case actionTypes.ROBOT_ADD:
      return {
        ...state,
        robotsPrograms: [
          ...state.robotsPrograms.filter(e => e.robotId !== action.robot.ffId),
          {
            robotId  : action.robot.ffId,
            programId: action.robot.program && action.robot.program.id,
          }
        ],
        robotList: [
          ...state.robotList,
          action.robot,
        ],
      }

    case actionTypes.SAVE_GAME_REQUEST:
      return {
        ...state,
        isGameSaving: true,
      }

    case actionTypes.SAVE_GAME_SUCCESS:
    case actionTypes.SAVE_GAME_FAILURE:
      return {
        ...state,
        isGameSaving: false,
      }

    case actionTypes.LOAD_GAME_REQUEST:
      return {
        ...state,
        isGameLoading: true,
      }

    case actionTypes.LOAD_GAME_SUCCESS:
    case actionTypes.LOAD_GAME_FAILURE:
      return {
        ...state,
        isGameLoading: false,
      }

    case actionTypes.SET_LEVEL_SAVED_DATA:
      return {
        ...state,
        levelSavedData: action.levelSavedData,
      }

    case actionTypes.SET_LEVEL_LOADED_DATA:
      return {
        ...state,
        levelLoadedData: action.levelLoadedData,
      }

    case actionTypes.MODULE_ADD:
      return {
        ...state,
        ownerModules: [
          ...state.ownerModules.filter(e => e.moduleId !== action.moduleId),
          {
            ownerId : action.ownerId,
            moduleId: action.moduleId,
          }
        ],
        moduleList: [
          ...state.moduleList,
          action.module,
        ],
      }

    case actionTypes.BIND_MODULE_TO_ROBOT:
      return {
        ...state,
        ownerModules: [
          ...state.ownerModules.filter(e => e.moduleId !== action.moduleId),
          {
            ownerId : action.ownerId,
            moduleId: action.moduleId,
          }
        ],
      }

    case actionTypes.LOAD_MODULES:
      return {
        ...state,
        moduleList: action.moduleList,
      }

    case actionTypes.SET_TOOLBOX:
      return {
        ...state,
        toolbox: action.toolbox,
      }

    case actionTypes.CLEAR_GAME_DATA:
      return {
        ...state,
        blockId           : DEFAULT_STATE.blockId,
        ir                : DEFAULT_STATE.ir,
        isWorkspaceVisible: DEFAULT_STATE.isWorkspaceVisible,
        levelLoadedData   : DEFAULT_STATE.levelLoadedData,
        levelSavedData    : DEFAULT_STATE.levelSavedData,
        moduleList        : DEFAULT_STATE.moduleList,
        ownerModules      : DEFAULT_STATE.ownerModules,
        programs          : DEFAULT_STATE.programs,
        robot             : DEFAULT_STATE.robot,
        robotList         : DEFAULT_STATE.robotList,
        robotsPrograms    : DEFAULT_STATE.robotsPrograms,
        selectedProgramId : DEFAULT_STATE.selectedProgramId,
        toolbox           : DEFAULT_STATE.toolbox,
      }

		default:
			return state
	}
}
