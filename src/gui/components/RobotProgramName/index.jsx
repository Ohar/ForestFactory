import EMPTY_OPTION from '@/constants/EMPTY_OPTION'
import signalSetProgramToRobot from '@/store/editor/signals/set-program-to-robot'
import getProgramById from '@/utils/getProgramById'
import getProgramId from '@/utils/getProgramId'
import getProgramOriginId from '@/utils/getProgramOriginId'
import { noop } from 'lodash'
import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import './styles.less'
import { ROBOT_NO_PROGRAM } from '@/l10n/keys'

function RobotProgramName ({l10n, programs, robot, robotsPrograms}) {
  const programId = getProgramId(robotsPrograms, robot)
  const originId  = getProgramOriginId(programId)
  const original  = programs.find(e => e.showAtList && e.id === originId)
  const chosen    = original || getProgramById(programId)

  return (
    <section
      className='RobotProgramName'
      title={
        chosen
        ? chosen.name
        : l10n[ROBOT_NO_PROGRAM]
      }
    >
      {
        chosen ?
        chosen.name :
        EMPTY_OPTION.text
      }
    </section>
  )
}

function mapStateToProps (state, ownProps) {
  return {
    ...ownProps,
    programs      : state.editorState.programs,
    robotsPrograms: state.editorState.robotsPrograms,
    l10n             : state.optionsState.l10n,
  }
}

RobotProgramName.propTypes = {
  programs      : PropTypes.array.isRequired,
  robot         : PropTypes.object.isRequired,
  robotsPrograms: PropTypes.array.isRequired,
}

RobotProgramName.defaultProps = {
  programs      : [],
  robot         : {},
  robotsPrograms: [],
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators({setProgramToRobot: signalSetProgramToRobot}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(RobotProgramName)
