import classNames from 'classnames'
import React from 'react'
import { connect } from 'react-redux'
import './styles.less'

function Tab ({activeTabId, children, tabId}) {
  const isActive = tabId === activeTabId

  return (
    <section className={classNames('Tab', {
      'Tab-hidden': !isActive,
    })}>
      {children}
    </section>
  )
}

function mapStateToProps (state, ownProps) {
  return {
    ...ownProps,
    activeTabId: state.tabState.activeTabId,
  }
}

export default connect(mapStateToProps)(Tab)
