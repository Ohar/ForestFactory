import { sortBy } from 'lodash'
import store from '@/store'
import { SAVE_GAME_FAILURE, SAVE_GAME_SUCCESS } from '@/store/menu/action-types'
import actionSetLevelSavedData from '@/store/editor/actions/set-level-saved-data'
import dbApi from '@/singletons/db/api'
import normalizeLevelObj from '@/utils/normalizeLevelObj'

const MIN_SAVE_TIME = 500

export default function saveLevel (level) {
  if (__DEV__) {
    store.dispatch(actionSetLevelSavedData(normalizeLevelObj(level)))
  }

  const saveToDb = dbApi.setLevel(level)

  const timer = new Promise(resolve => {
    setTimeout(resolve, MIN_SAVE_TIME)
  })

  Promise
    .all([saveToDb, timer])
    .then(
      () => {
        store.dispatch({type: SAVE_GAME_SUCCESS})
      },
    )
    .catch(
      error => {
        store.dispatch({
          type: SAVE_GAME_FAILURE,
          error,
        })
      },
    )
}
