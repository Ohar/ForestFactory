import getModuleOwnerId from '@/utils/getModuleOwnerId'
import getRobotById from '@/utils/getRobotById'

export default function getModuleOwner (moduleId) {
  const ownerId = getModuleOwnerId(moduleId)

  return getRobotById(ownerId)
}
