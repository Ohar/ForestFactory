import actionSetRepositoryAvailability from '@/store/gui/actions/set-repository-availability'
import actionSetEditorAvailability from '@/store/gui/actions/set-editor-availability'
import actionSetModulesAvailability from '@/store/gui/actions/set-modules-availability'
import actionSetProgramDeleteBtnAvailability from '@/store/gui/actions/set-ProgramDeleteBtn-availability'
import actionSetProgramRenameBtnAvailability from '@/store/gui/actions/set-ProgramRenameBtn-availability'
import actionSetProgramCopyBtnAvailability from '@/store/gui/actions/set-ProgramCopyBtn-availability'
import store from '@/store'
import levelDefaultOptions from '@/constants/levelDefaultOptions'

export default function loadOptions (loadedOptions = {}) {
  return new Promise(
    resolve => {
      const options = {
        ...levelDefaultOptions,
        ...loadedOptions,
      }

      store.dispatch(actionSetRepositoryAvailability(options.isRepositoryEnabled))
      store.dispatch(actionSetEditorAvailability(options.isEditorEnabled))
      store.dispatch(actionSetModulesAvailability(options.isModulesEnabled))
      store.dispatch(actionSetProgramDeleteBtnAvailability(options.isProgramDeleteBtnEnabled))
      store.dispatch(actionSetProgramRenameBtnAvailability(options.isProgramRenameBtnEnabled))
      store.dispatch(actionSetProgramCopyBtnAvailability(options.isProgramCopyBtnEnabled))

      resolve()
    },
  )
}
