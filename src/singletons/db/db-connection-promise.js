// Подключение к IndexedDB

import DB_NAME from '@/singletons/db/consts/DB_NAME'
import STORED_OBJECTS from '@/singletons/db/consts/STORED_OBJECTS'

const dbConnectionPromise = new Promise(
	(resolve, reject) => {
		const dbRequest = window.indexedDB.open(DB_NAME)

		dbRequest.onsuccess = () => {
			resolve(dbRequest.result)
		}

		dbRequest.onerror = event => {
			reject(`IndexedDB error: ${event.request.errorCode}`)
		}

		dbRequest.onupgradeneeded = event => {
			const db = event.target.result;

      STORED_OBJECTS.forEach(
        storedObject => db.createObjectStore(storedObject, {keyPath: 'id'})
      )
		}
	}
)

export default dbConnectionPromise
