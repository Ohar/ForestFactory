import React from 'react'
import './styles.less'

// TODO: Close on ESC
function Popup ({children, close}) {
  return (
    <section className='Popup'>
      {children}
      <button
        className='Popup_close'
        onClick={close}
      >
        ×
      </button>
    </section>
  )
}

export default Popup
