import store from '@/store'
import signalSetQuestByIndex from '@/store/plot/actions/set-quest-by-index'
import showQuestInfo from '@/quest/showQuestInfo'
import showWinScreen from '@/quest/showWinScreen'

const TIME_TO_WAIT_BEFORE_QUEST_ENDS = 200

export default function checkQuestsDone (game) {
  setTimeout(
    () => {
      const {questList, questIndex} = store.getState().plotState
      const quest = questList[questIndex]
      const isDone = quest && quest.counter >= quest.goal

      if (isDone) {
        const nextQuestIndex = questIndex + 1
        const nextQuest = questList[nextQuestIndex]

        if (nextQuest) {
          // Set next quest
          store.dispatch(signalSetQuestByIndex(nextQuestIndex))
          setTimeout(
            () => {
              showQuestInfo(game)
            },
            TIME_TO_WAIT_BEFORE_QUEST_ENDS
          )
        } else {
          // Win level
          showWinScreen(game)
        }
      }
    },
    TIME_TO_WAIT_BEFORE_QUEST_ENDS
  )
}
