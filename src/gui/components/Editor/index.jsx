// Редактор кода

import Blockly from '@/blockly/blockly'
import loadBlocklyWorkspace from '@/blockly/load-blockly-workspace'
import Listing from '@/gui/components/Listing'
import ProgramAddBtn from '@/gui/components/ProgramAddBtn'
import ProgramControls from '@/gui/components/ProgramControls'
import ProgramCopyToStorage from '@/gui/components/ProgramCopyToStorage'
import Workspace from '@/gui/components/Workspace'
import store from '@/store'
import { SET_EDITOR_ROBOT } from '@/store/editor/action-types'
import onWorkspaceAddBlockWatcher from '@/store/editor/listeners/onWorkspaceAddBlockWatcher'
import signalUpdateWorkSpaceVisibility from '@/store/editor/signals/update-workspace-visibility'
import signalTabListFullScreenOff from '@/store/tab/actions/tab-list-fullScreen-off'
import signalTabListFullScreenOn from '@/store/tab/actions/tab-list-fullScreen-on'
import getProgramId from '@/utils/getProgramId'
import workspaceSingletone from '@/singletons/workspace'
import { isNumber } from 'lodash'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { showLevelData } from 'root/config.level'
import { EDITOR_HEADER, PROGRAM_NOT_SELECTED, ROBOT_NOT_SELECTED } from '@/l10n/keys'
import './styles.less'

class Editor extends Component {
  constructor () {
    super()

    workspaceSingletone.workspace = null

    this.updateCodeOutput   = this.updateCodeOutput.bind(this)
    this.onWorkSpaceChanged = this.onWorkSpaceChanged.bind(this)

    this.state = {
      xml: '',
      js : '',
    }
  }

  componentDidMount () {
    const {fullScreenOff, fullScreenOn, toolbox} = this.props

    if (toolbox) {
      // Костыль из “fullScreenOn + setTimeout + loadBlocklyWorkspace + fullScreenOff”
      // для того, чтобы workspace отрисовался на полный экран
      // иначе при изменении размера экрана его пришлось бы его пересоздавать
      fullScreenOn()

      setTimeout(() => {
        loadBlocklyWorkspace(toolbox)
          .then(
            workspace => {
              fullScreenOff()

              workspaceSingletone.workspace = workspace
              workspaceSingletone.workspace.addChangeListener(this.onWorkSpaceChanged)
            },
          )
      }, 0)
    }
  }

  onWorkSpaceChanged () {
    const {updateWorkSpaceVisibility} = this.props

    if (onWorkspaceAddBlockWatcher.hasListeners) {
      const blockList = workspaceSingletone.workspace.getAllBlocks()

      onWorkspaceAddBlockWatcher.checker(blockList)
    }

    updateWorkSpaceVisibility()

    this.updateCodeOutput()
  }

  getProgram (forcedTree, forcedRobot) {
    const {programs, robotsPrograms, robot} = this.props
    const tree                              = forcedTree || robotsPrograms
    const realRobot                         = forcedRobot || robot
    const programId                         = getProgramId(tree, realRobot)

    return programs.find(e => e.id === programId)
  }

  componentWillReceiveProps (nextProps) {
    if (
      nextProps.robot !== this.props.robot
      || nextProps.robotsPrograms !== this.props.robotsPrograms
    ) {
      const program = this.getProgram(nextProps.robotsPrograms, nextProps.robot)
      const xml     = program
                      ? program.xml
                      : ''

      this.updateXml(xml)
    }

    if (workspaceSingletone.workspace && nextProps.blockId !== this.props.blockId) {
      workspaceSingletone.workspace.highlightBlock(nextProps.blockId)
    }
  }

  updateXml (xml) {
    this.setState({xml})

    if (workspaceSingletone.workspace) {
      workspaceSingletone.workspace.clear()

      const dom = Blockly.Xml.textToDom(xml)

      Blockly.Xml.domToWorkspace(dom, workspaceSingletone.workspace)
    }
  }

  updateXmlAndJs (xml, js) {
    this.setState({xml, js})

    const program = this.getProgram()

    if (program) {
      program.js  = js
      program.xml = xml
    }
  }

  updateCodeOutput () {
    const xml = Blockly.Xml.domToPrettyText(Blockly.Xml.workspaceToDom(workspaceSingletone.workspace)),
          js  = Blockly.JavaScript.workspaceToCode(workspaceSingletone.workspace)

    this.updateXmlAndJs(xml, js)
  }

  clear () {
    this.updateXml('')

    this.props.dispatch({
      type : SET_EDITOR_ROBOT,
      robot: null,
    })

    if (workspaceSingletone.workspace && workspaceSingletone.workspace.clear) {
      workspaceSingletone.workspace.clear()
    }
  }

  render () {
    const {robot, robotList, l10n, levelSavedData, levelLoadedData} = this.props
    const {xml, js}                                                 = this.state

    const program = this.getProgram()

    const robotIndex = robot
                       ? robotList.findIndex(e => e === robot)
                       : null

    const robotName = isNumber(robotIndex)
                      ? robot.name
                      : l10n[ROBOT_NOT_SELECTED]

    const programName = program
                        ? program.name
                        : l10n[PROGRAM_NOT_SELECTED]

    return (
      <section className='Editor'>
        <header className='Editor_header'>{l10n[EDITOR_HEADER]}</header>

        {
          __DEV__ && showLevelData
          ? (
            <section className='Editor_levelData'>
              <textarea value={JSON.stringify(levelSavedData)} readOnly/>
              <textarea value={JSON.stringify(levelLoadedData)} readOnly/>
            </section>
          )
          : null
        }

        <section className='Editor_info'>
          <span className='Editor_info_name Editor_info_name-robot'>{robotName}</span>
          <span className='Editor_info_divider'>/</span>
          <span className='Editor_info_name Editor_info_name-program'>{programName}</span>
        </section>

        <section className="Editor_controls">
          <ProgramAddBtn/>
          <ProgramCopyToStorage/>
        </section>

        <ProgramControls robot={robot}/>
        <Workspace visible={Boolean(program)}/>

        {
          false && __DEV__
          ? <Listing xml={xml} js={js}/>
          : null
        }
      </section>
    )
  }
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators(
    {
      fullScreenOff            : signalTabListFullScreenOff,
      fullScreenOn             : signalTabListFullScreenOn,
      updateWorkSpaceVisibility: signalUpdateWorkSpaceVisibility,
    },
    dispatch,
  )
}

function mapStateToProps (state, ownProps) {
  return {
    ...ownProps,
    robot          : state.editorState.robot,
    programs       : state.editorState.programs,
    blockId        : state.editorState.blockId,
    fullScreen     : state.tabState.fullScreen,
    robotsPrograms : state.editorState.robotsPrograms,
    robotList      : state.editorState.robotList,
    levelSavedData : state.editorState.levelSavedData,
    levelLoadedData: state.editorState.levelLoadedData,
    toolbox        : state.editorState.toolbox,
    l10n           : state.optionsState.l10n,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Editor)
