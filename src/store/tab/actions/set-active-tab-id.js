import { SET_ACTIVE_TAB } from '@/store/tab/action-types'

export default function signalSetActiveTabId (activeTabId) {
  return dispatch => {
    dispatch(
      {
        type: SET_ACTIVE_TAB,
        activeTabId,
      },
    )
  }
}
