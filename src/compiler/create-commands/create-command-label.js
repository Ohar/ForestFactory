import getLabelName from '@/utils/get-label-name'

function createCommandLabel (program, params, commandsCount) {
	const labelName = getLabelName(params[0])

	program.addLabel(labelName, commandsCount + 1)

	return () => {
		program.stage++
		return program.run()
	}
}

export default createCommandLabel
