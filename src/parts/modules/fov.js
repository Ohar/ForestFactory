import { fontSize } from '@/constants/robot_font'
import Phaser from 'phaser'
import store from '@/store'
import { FOV_TEXT } from '@/l10n/keys'

const fovColor       = 0x333333
const fovColorHashed = '#333333'

const rangeTextGapKoef = .93
const textKoefList     = [
  {
    x    : 1,
    y    : 1,
    angle: -45,
  },
  {
    x    : -1,
    y    : 1,
    angle: 45,
  },
  {
    x    : 1,
    y    : -1,
    angle: 45,
  },
  {
    x    : -1,
    y    : -1,
    angle: -45,
  },
]

function calcTextXY (visionRange) {
  return Math.floor(Math.sqrt(2 * visionRange * visionRange * rangeTextGapKoef) / 2)
}

export default class Fov extends Phaser.Graphics {
  constructor (owner, range) {
    const game = owner.game

    super(game)

    const {optionsState: {l10n}} = store.getState()

    this.game       = game
    this.owner      = owner
    this.range      = range
    this.circle     = null
    this.textList   = []
    this.textCoord  = calcTextXY(this.range)
    this.textString = l10n[FOV_TEXT]

    this.hide = this.hide.bind(this)
    this.show = this.show.bind(this)
  }

  get diameter () {
    return this.range * 2
  }

  show () {
    this.circle = this.game.add.graphics()

    this.circle.lineStyle(3, fovColor, .3)
    this.circle.drawCircle(0, 0, this.range * 2)

    this.textList = textKoefList.map(
      ({x: xKoef, y: yKoef, angle}) => {
        const textNode = this.game.add.text(0, 0, this.textString, {fontSize, fill: fovColorHashed})

        textNode.anchor.set(.5)
        textNode.angle = angle
        textNode.x     = this.textCoord * xKoef
        textNode.y     = this.textCoord * yKoef

        return textNode
      },
    )

    this.owner.addChild(this.circle)

    this.textList.forEach(
      text => {
        this.owner.addChild(text)
      }
    )
  }

  hide () {
    this.owner.removeChild(this.circle)
    this.circle.destroy()

    this.textList.forEach(
      text => {
        this.owner.removeChild(text)
        text.destroy()
      }
    )

    this.textList = []
  }
}
