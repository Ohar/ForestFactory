import LocaleToggler from '@/gui/components/LocaleToggler'
import Popup from '@/gui/components/Popup'
import { OPTIONS_HEADER_TEXT } from '@/l10n/keys'
import actionOptionsHide from '@/store/gui/actions/options-hide'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import './styles.less'

class Options extends Component {
  render () {
    const {l10n, visible, optionsHide} = this.props

    return visible
           ? (
             <Popup
               className='Options'
               close={optionsHide}
             >
               <header className='Options_header'>
                 {l10n[OPTIONS_HEADER_TEXT]}
               </header>
               <div className='Options_body'>
                 <LocaleToggler/>
               </div>
             </Popup>
           )
           : null
  }
}

function mapStateToProps (state, ownProps) {
  return {
    ...ownProps,
    visible: state.guiState.optionsVisible,
    l10n   : state.optionsState.l10n,
  }
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators(
    {
      optionsHide: actionOptionsHide,
    },
    dispatch,
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(Options)
