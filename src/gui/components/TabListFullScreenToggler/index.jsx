import signalTabListFullScreenOff from '@/store/tab/actions/tab-list-fullScreen-off'
import signalTabListFullScreenOn from '@/store/tab/actions/tab-list-fullScreen-on'
import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { TAB_FULLSCREEN_BTN_NARROWER, TAB_FULLSCREEN_BTN_WIDER } from '@/l10n/keys'
import './styles.less'

function TabListFullScreenToggler ({l10n, on, off, fullScreen}) {
	const options = fullScreen
		? {
			text   : '🢂🢀',
			onClick: off,
			title  : l10n[TAB_FULLSCREEN_BTN_NARROWER],
		}
		: {
			text   : '🢀🢂',
			onClick: on,
			title  : l10n[TAB_FULLSCREEN_BTN_WIDER],
		}

	return (
		<button className='TabListFullScreenToggler'
		        onClick={options.onClick}
		        title={options.title}>
			{options.text}
		</button>
	)
}

function mapDispatchToProps (dispatch) {
	return bindActionCreators(
		{
			on : signalTabListFullScreenOn,
			off: signalTabListFullScreenOff,
		},
		dispatch
	)
}

function mapStateToProps (state, ownProps) {
	return {
    ...ownProps,
    fullScreen: state.tabState.fullScreen,
    l10n      : state.optionsState.l10n,
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TabListFullScreenToggler)
