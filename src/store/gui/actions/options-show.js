import { SET_OPTIONS_VISIBILITY } from '@/store/gui/action-types'

export default function actionOptionsShow () {
  return dispatch => {
    dispatch({
      type          : SET_OPTIONS_VISIBILITY,
      optionsVisible: true,
    })
  }
}
