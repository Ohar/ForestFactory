import signalStartLevelScreenScreenHide from '@/store/gui/actions/start-level-screen-hide'
import classNames from 'classnames'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import getLocalizedString from '@/l10n/getLocalizedString'
import { LVL_SCREEN_PRESS_ANY_KEY } from '@/l10n/keys'
import './styles.less'

class StartLevelScreen extends Component {
  constructor (...args) {
    super(...args)

    this.bindAnyKey = this.bindAnyKey.bind(this)
    this.onAnyKeyDown = this.onAnyKeyDown.bind(this)
    this.unbindAnyKey = this.unbindAnyKey.bind(this)
  }

  bindAnyKey () {
    document.addEventListener('click', this.onAnyKeyDown)
    document.addEventListener('keydown', this.onAnyKeyDown)
  }

  unbindAnyKey () {
    document.removeEventListener('click', this.onAnyKeyDown)
    document.removeEventListener('keydown', this.onAnyKeyDown)
  }

  onAnyKeyDown () {
    this.props.hide()
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.visible !== this.props.visible) {
      if (nextProps.visible) {
        this.bindAnyKey()
      } else {
        this.unbindAnyKey()
      }
    }
  }

  render () {
    const {l10n, levelName, visible} = this.props
    const localizedLevelName = getLocalizedString(levelName)

    return (
      <section className={classNames('StartLevelScreen', visible ? '' : 'StartLevelScreen-hidden')}>
        <header className='StartLevelScreen_header'>{localizedLevelName}</header>
        <p className='StartLevelScreen_text'>
          {l10n[LVL_SCREEN_PRESS_ANY_KEY]}
        </p>
      </section>
    )
  }
}

function mapStateToProps (state, ownProps) {
  return {
    ...ownProps,
    visible  : state.guiState.startLevelScreenVisible,
    l10n     : state.optionsState.l10n,
    levelName: state.guiState.levelName,
  }
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators(
    {
      hide: signalStartLevelScreenScreenHide,
    },
    dispatch,
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(StartLevelScreen)
