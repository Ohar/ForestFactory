import IRNode from '@/classes/ir-node'
import convertAstNodeToIR from '@/compiler/converters/convert-ast-node-to-ir'
import uid from 'uid'

export default function convertWhileStatement (node, program, upperLoopLabels) {
	const labelStart = uid(20),
		  labelBody  = uid(20),
		  labelEnd   = uid(20)

	const loopLabels = {
		nextIteration: labelBody,
		endLoop      : labelEnd,
	}

	const condition = convertAstNodeToIR(node.test, program, upperLoopLabels),
	      body      = convertAstNodeToIR(node.body, program, loopLabels)

	return new IRNode({
		id  : null,
		code: `
${condition.code}
label ${labelStart}
if ${condition.id} ${labelBody}
if !${condition.id} ${labelEnd}
label ${labelBody}
${body.code}
goto ${labelStart}
label ${labelEnd}
`,
	})
}
