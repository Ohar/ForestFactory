import classNames from 'classnames'
import { isNumber } from 'lodash'
import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import './styles.less'

function Workspace ({fullScreen, visible}) {
  return (
    <section
      id='BLOCKLY_WORKSPACE'
      className={
        classNames(
          'Workspace',
          {
            'Workspace-hidden'    : !visible,
            'Workspace-fullScreen': fullScreen,
          },
        )
      }
    />
  )
}

Workspace.propTypes = {
  visible: PropTypes.bool,
}

Workspace.defaultProps = {
  visible: false,
}

function mapStateToProps (state, ownProps) {
  return {
    ...ownProps,
    fullScreen: state.tabState.fullScreen,
  }
}

export default connect(mapStateToProps)(Workspace)

