import { STORAGE_MODULE_TYPE } from '@/constants/moduleTypes'
import { fontSize, fontStyle } from '@/constants/robot_font'
import Module from '@/parts/modules/module'
import noop from 'lodash/noop'

const DEFAULT_FULLNESS = 0
const DEFAULT_STORAGE_VOLUME = 100
const FONT_STYLE_ERROR = {
  ...fontStyle,
  fill: 'red',
}

export default class Storage extends Module {
  constructor (
    {
      changeFullnessCb = noop,
      content = [],
      fullnessCb = noop,
      hasText = false,
      id,
      owner,
      ownerId,
      volume = DEFAULT_STORAGE_VOLUME,
    },
  ) {
    super({id, moduleType: STORAGE_MODULE_TYPE, owner, ownerId})

    this.volume           = volume
    this.fullness         = DEFAULT_FULLNESS
    this.fullnessCb       = fullnessCb
    this.changeFullnessCb = changeFullnessCb
    this.content          = content // TODO: Хранить не объекты, а только id, иначе это невозможно сохранить в JSON

    if (hasText) {
      this.text = this.owner.game.add.text(0, 0, this.getText(), fontStyle)

      this.owner.addChild(this.text)
      this.text.anchor.set(.5)
      this.text.x = Math.floor(this.owner.width / 2)
      this.text.y = Math.floor(-fontSize / 2)
    } else {
      this.text = null
    }
  }

  get emptyVolume () {
    return this.volume - this.fullness
  }

  put (item) {
    if (this.ifWillFit(item)) {
      this.content.push(item)
      this.updateFullness()
    } else {
      if (this.text) {
        this.text.setStyle(FONT_STYLE_ERROR)

        setTimeout(
          () => {
            this.text.setStyle(fontStyle)
          },
          3000,
        )
      }

      throw new Error('No empty space', item)
    }
  }

  take (item) {
    if (this.content.includes(item)) {
      const taken = this.content.splice(this.content.indexOf(item), 1)
      this.updateFullness()
      return taken
    } else {
      throw new Error('No such item', item)
    }
  }

  getText () {
    return `${this.fullness} / ${this.volume}`
  }

  updateFullness () {
    const oldFullness = this.fullness

    this.fullness = this.content.reduce(
      (acc, curr) => acc + curr.volume,
      DEFAULT_FULLNESS,
    )

    if (this.text) {
      this.text.setText(this.getText())
    }

    this.changeFullnessCb(oldFullness)

    if (this.fullness === this.volume) {
      this.fullnessCb()
    }
  }

  ifWillFit (item) {
    return item.volume + this.fullness <= this.volume
  }

  ifWillFitPartly (item) {
    return item.isSeparable && this.fullness < this.volume
  }

  clear () {
    this.content = []
    this.updateFullness()
  }
}
