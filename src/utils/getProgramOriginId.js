import getProgramById from '@/utils/getProgramById'

export default function getProgramOriginId (programId) {
  const program = getProgramById(programId)

  return program && program.originId
    ? getProgramOriginId(program.originId)
    : programId
}
