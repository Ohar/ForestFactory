import { CLEAR_GAME_DATA } from '@/store/editor/action-types'

export default function actionClearGameData () {
  return dispatch => {
    dispatch({type: CLEAR_GAME_DATA})
  }
}
