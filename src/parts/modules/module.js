import { DEFAULT_MODULE_TYPE } from '@/constants/moduleTypes'
import { BIND_MODULE_TO_ROBOT, MODULE_ADD } from '@/store/editor/action-types'
import store from '@/store/index'
import getRobotById from '@/utils/getRobotById'
import uid from 'uid'

export default class Module {
  constructor ({id = uid(20), moduleType = DEFAULT_MODULE_TYPE, owner, ownerId}) {
    this.id         = id
    this.moduleType = moduleType

    const combinedOwnerId = owner && owner.ffId || ownerId || null

    this.setOwner(combinedOwnerId)

    store.dispatch({
      type   : MODULE_ADD,
      ownerId: combinedOwnerId,
      module : this,
    })
  }

  reconnectToOwner () {
    this.setOwner(this.ownerId)
  }

  setOwner (ownerId) {
    if (ownerId) {
      store.dispatch({
        moduleId: this.id,
        ownerId,
        type    : BIND_MODULE_TO_ROBOT,
      })

      this.owner   = getRobotById(ownerId) // TODO: rename into getOwnerById here and in reducer
      this.ownerId = ownerId
    } else {
      this.owner   = null
      this.ownerId = null
    }
  }

  toJson () {
    const storageObjData = this._storage
                           ? {
        storage : this._storage,
        _storage: undefined,
      }
                           : {}
    const fovData = this.fov
                           ? { fov: undefined }
                           : {}
    const grabbedObjData = this.grabbedObj
                           ? { grabbedObj: undefined }
                           : {}
    const grabbedObjParentData = this.grabbedObjParent
                           ? { grabbedObjParent: undefined }
                           : {}
    const fullnessCbData = this.fullnessCb
                           ? { fullnessCb: undefined }
                           : {}
    const changeFullnessCbData = this.changeFullnessCb
                           ? { changeFullnessCb: undefined }
                           : {}
    const contentData = this.content
                           ? { content: undefined }
                           : {}
    const textData = this.text
                           ? { text: undefined }
                           : {}
    return {
      ...this,
      ...changeFullnessCbData,
      ...contentData,
      ...fovData,
      ...fullnessCbData,
      ...grabbedObjData,
      ...grabbedObjParentData,
      ...storageObjData,
      ...textData,
      owner: null,
    }
  }
}
