import * as actionTypes from '@/store/tab/action-types'
import DEFAULT_STATE from '@/store/tab/default_state'

export default function tabsReducer (state = DEFAULT_STATE, action) {
  switch (action.type) {
    case actionTypes.SET_ACTIVE_TAB: {
      return {
        ...state,
        activeTabId: action.activeTabId,
      }
    }

    case actionTypes.SET_TAB_LIST_VISIBILITY: {
      return {
        ...state,
        tabListVisibility: action.tabListVisibility,
      }
    }

    case actionTypes.SET_TAB_LIST_FULLSCREEN: {
      return {
        ...state,
        fullScreen: action.fullScreen,
      }
    }

    default:
      return state
  }
}
