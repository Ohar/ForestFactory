import {SET_TAB_LIST_FULLSCREEN} from '@/store/tab/action-types'

export default function signalTabListFullScreenOn () {
	return dispatch => {
		dispatch(
			{
				type      : SET_TAB_LIST_FULLSCREEN,
				fullScreen: true,
			}
		)
	}
}
